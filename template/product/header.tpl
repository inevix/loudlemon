﻿<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title><?php echo $title; if (isset($_GET['page'])) { echo " - ". ((int) $_GET['page'])." ".$text_page;} ?></title>
        <base href="<?php echo $base; ?>" />
        <?php if ($description) { ?>
        <meta name="description" content="<?php echo $description; if (isset($_GET['page'])) { echo " - ". ((int) $_GET['page'])." ".$text_page;} ?>" />
        <?php } ?>
        <?php if ($keywords) { ?>
        <meta name="keywords" content= "<?php echo $keywords; ?>" />
        <?php } ?>
        <meta property="og:title" content="<?php echo $title; if (isset($_GET['page'])) { echo " - ". ((int) $_GET['page'])." ".$text_page;} ?>" />
        <meta property="og:type" content="website" />
        <meta property="og:url" content="<?php echo $og_url; ?>" />
        <?php if ($og_image) { ?>
        <meta property="og:image" content="<?php echo $og_image; ?>" />
        <?php } else { ?>
        <meta property="og:image" content="<?php echo $logo; ?>" />
        <?php } ?>
        <meta property="og:site_name" content="<?php echo $name; ?>" />
        <!-- Bootstrap -->
        <link rel="stylesheet" href="/catalog/view/theme/loudlemon/stylesheet/bootstrap.min.css">
        <link rel="stylesheet" href="/catalog/view/theme/loudlemon/stylesheet/owl.carousel.min.css" />
        <link rel="stylesheet" href="/catalog/view/theme/loudlemon/stylesheet/menu.css">
        <link rel="stylesheet" href="/catalog/view/theme/loudlemon/stylesheet/loudlemon.css">
        <?php foreach ($styles as $style) { ?>
        <link href="/<?php echo $style['href']; ?>" type="text/css" rel="<?php echo $style['rel']; ?>" media="<?php echo $style['media']; ?>" />
        <?php } ?>
        <?php foreach ($analytics as $analytic) { ?>
        <?php echo $analytic; ?>
        <?php } ?>

        

        <link rel="stylesheet" type="text/css" href="/catalog/view/theme/loudlemon/stylesheet/thumbelina.css" />
        <style type="text/css">
            /* Some styles for the containers */
            #slider3 {
                position: relative;
                margin-top: 23px;
                padding-top: 7px;
                width: 100%;
                height: 803px;
                margin-bottom: 40px;
                margin-left: -13px;
                min-width: 80px;
            }

            @media (max-width: 991px) {
                #slider3 {
                    margin-left: 10px;
                }

            }

            @media (max-width: 360px) {
                #slider3 {
                    margin-left: 5px;
                }

            }

            @media (max-width: 772px) {
                .thumbelina-but.vert {
                    width: 88%;
                }
            }
            

            #slider3 div:nth-child(2) {
                margin-top: -27px;
            }
            <?php if($curLang == 'en') {echo "ul.nav {padding: 0 11px;} ul.nav:last-child {padding-right: 22px; padding-left: 21px;}"; } ?>  

            /* Ширина слайдера */
            #slider-price {
                width: 94%;
                margin: auto;
                margin-top: 10px;
            }
            /* Контейнер слайдера */
            .ui-slider {
                position: relative;
            }
            /* Ползунок */
            .ui-slider .ui-slider-handle {
                position: absolute;
                z-index: 2;
                width: 8px;   /* Задаем нужную ширину */
                height: 20px;  /* и высоту */
                background-color: #B38E67;
                border: 1px solid;
                cursor: pointer
            }

            .ui-slider-horizontal .ui-slider-handle {
                border-radius: 0 !important;
                border: 0;                
            }
            .ui-slider .ui-slider-range {
                position: absolute;
                z-index: 1;
                font-size: .7em;
                display: block;
                border: 0;
                overflow: hidden;
            }
            /* горизонтальный слайдер (сама полоса по которой бегает ползунок) */
            .ui-slider-horizontal {
                 height: 12px; /* задаем высоту согласно дизайна */
            }
            /* позиционируем ползунки */
            .ui-slider-horizontal .ui-slider-handle { 
                top: -5px;
                margin-left: -6px;
            }
            .ui-slider-horizontal .ui-slider-range {
                top: 0;
                height: 100%;
            }
            .ui-slider-horizontal .ui-slider-range-min { 
                left: 0;
            }
            .ui-slider-horizontal .ui-slider-range-max {
                right: 0;
            }
            /* оформление полосы по которой ходит ползунок */
            .ui-widget-content { 
                border: 1px solid #D4D4D4;
                background: #fff;
            }
            /* оформление активного участка (между двумя ползунками) */
            .ui-widget-header { 
                border: 1px solid #D4D4D4;
                background: #FFF;
            }
            /* скругление для полосы слайдера */
            .ui-corner-all {
                -moz-border-radius: 4px;
                -webkit-border-radius: 4px;
                border-radius: 4px;
}

</style>
        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="/catalog/view/theme/loudlemon/js/jquery-2.2.1.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
        <script src="/catalog/view/theme/loudlemon/js/owl.carousel.min.js"></script>
        <script src="/catalog/view/theme/loudlemon/js/bootstrap.min.js"></script>

        <script src="/catalog/view/theme/loudlemon/js/thumbelina.js"></script>
        <?php foreach ($scripts as $script) { ?>
        <script src="/<?php echo $script; ?>" type="text/javascript"></script>
        <?php } ?>
        <script src="/catalog/view/javascript/common.js"></script>
        <script src="/catalog/view/theme/loudlemon/js/common.js"></script>
        <!-- <link rel="icon" href="http://loud-lemon.com/image/catalog/favicon.ico" type="image/x-icon"> -->

        <link rel="apple-touch-icon" sizes="180x180" href="/catalog/view/theme/loudlemon/images/layout/favicon/apple-touch-icon.png">
        <link rel="icon" type="image/png" href="/catalog/view/theme/loudlemon/images/layout/favicon/favicon-32x32.png" sizes="32x32">
        <link rel="icon" type="image/png" href="/catalog/view/theme/loudlemon/images/layout/favicon/favicon-16x16.png" sizes="16x16">
        <link rel="manifest" href="/catalog/view/theme/loudlemon/images/layout/favicon/manifest.json">
        <link rel="mask-icon" href="/catalog/view/theme/loudlemon/images/layout/favicon/safari-pinned-tab.svg" color="#5bbad5">
        <link rel="shortcut icon" href="/catalog/view/theme/loudlemon/images/layout/favicon/favicon.ico">
        <meta name="msapplication-config" content="/catalog/view/theme/loudlemon/images/layout/favicon/browserconfig.xml">
        <meta name="theme-color" content="#ffffff">
        
    </head>
    <body>
        <header>
            <div class="toppest">
                <div class="container">
                    <div class="row toolbar">                                                
                        <div class="col-md-4 col-sm-4 col-xs-12 langs">
                            <?php echo $language; ?>
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-12 search-container">
                            <div id="search" class="search">
                                <input type="text" name="search" placeholder="<?PHP echo $text_search; ?>">
                                <button type="button" class="btn btn-default" name="s">
                                    <span class="glyphicon glyphicon-search" aria-hidden="true"></span></button>
                                    <div class="search-line"></div>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-12" id="user-links">
                            <div class="row" style="margin: 0;">
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                            <?php echo $cart; ?></div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" id='account-drop'>
                                    <ul class="list-inline">
                                        <li class="dropdown">
                                            <a  id="garaj" href="<?php echo $account; ?>" title="<?php echo $text_account; ?>" class="dropdown-toggle" data-toggle="dropdown">
                                                
                                                <?php echo $text_account; ?>
                                                <span class="caret hidden-xs"></span>
                                            </a>
                                            <ul class="dropdown-menu dropdown-menu-right">
                                                <?php if ($logged) { ?>
                                                <li><a href="<?php echo $account; ?>"><?php echo $text_account; ?></a></li>
                                                <li><a href="<?php echo $order; ?>"><?php echo $text_order; ?></a></li>
                                                <li><a href="<?php echo $logout; ?>"><?php echo $text_logout; ?></a></li>
                                                <?php } else { ?>
                                                <li><a href="<?php echo $register; ?>"><?php echo $text_register; ?></a></li>
                                                 <li><a href="<?php echo $login; ?>"><?php echo $text_login; ?></a></li>
                                                 <?php } ?>
                                            </ul>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container" id="middle-header">
                <div class="row">
                    <div class="col-md-4 col-sm-4 col-xs-12">
                        <a href=""><img src="/catalog/view/theme/loudlemon/images/layout/main_logo.svg" alt="Loud-Lemon" id="logo" class="logo-desktop img-responsive"><img src="/catalog/view/theme/loudlemon/images/layout/main_logo_mobile.svg" alt="Loud-Lemon" class="img-responsive logo-mobile"></a>

                    </div>
                    <div class="col-md-4 col-sm-4 hidden-xs">
                        <a href="tel:+74952334442">
                            <img src="/catalog/view/theme/loudlemon/images/layout/<?PHP echo $text_mascot; ?>" id="mascot" class="img-responsive">
                        </a>
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-12">
                        <div id="contacts">
                            <span class="phone hidden-lg hidden-md hidden-sm"><a href="tel:<?php echo str_replace(array("-", " "), "", $telephone); ?>"><?php echo $telephone; ?></a></span>
                            <span class="work-hours"><?php  if($curLang == 'ru'){echo 'Часы работы: <span class="brln"></span>';}else {echo 'Working time: <span class="brln"></span>';}?><?php  if($curLang == 'ru'){echo htmlspecialchars_decode($open[0]);} else {echo htmlspecialchars_decode($open[1]);} ?>
                            </span>
                            <span class="adress"><?php  if($curLang == 'ru'){echo htmlspecialchars_decode($address[0]);} else {echo htmlspecialchars_decode($address[1]);} ?></span>
                            <span class="mob-addr-open"><?php  if($curLang == 'ru'){echo htmlspecialchars_decode($comment[0]);} else {echo htmlspecialchars_decode($comment[1]);} ?></span>
                        </div>
                        <div class="SNbar">
                            <a href="https://www.instagram.com/loud_lemon/"><img id="cnpic1" src="/catalog/view/theme/loudlemon/images/layout/social_insta.svg" alt="Loud-Lemon В Instagram"></a>
                            <a href="https://vk.com/loud_lemon"><img id="cnpic2" src="/catalog/view/theme/loudlemon/images/layout/social_vk.svg" alt="Loud-Lemon на VK"></a>
                            <a href=""><img id="cnpic3" src="/catalog/view/theme/loudlemon/images/layout/social_fb.svg" alt="Loud-Lemon на Facebook"></a>
                        </div>                    
                    </div>
                </div>
            </div>

            <nav class="navbar navbar-default">
                <div class="container" id="mycon">
                    <div class="navbar-header">
                        <div class="animated-button-container hidden-sm hidden-md hidden-lg">
                            <div class="animated-button-container__background"></div>  
                            <div id="nav-icon3" data-toggle="collapse" data-target=".js-navbar-collapse" class="nav-icon">
                              <span></span>
                              <span></span>
                              <span></span>
                              <span></span>
                            </div>                            
                            <span class="menu-text">МЕНЮ</span>
                        </div>
                        <!-- <button class="navbar-toggle desk-nav-button" type="button" data-toggle="collapse" data-target=".js-navbar-collapse">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button> -->
                        <form action="/search/">
                            <input class="search-mobile-input" type="text">
                            <button class="glyphicon glyphicon-search search-mob-button" aria-hidden="true"></button>
                        </form>
                        <div class="mob-cart-container">
                            <?php echo $cart; ?>
                        </div>                        
                        <div class="row tablet-nav-row">
                            <div class="col-xs-2">
                                <div class="animated-button-container animated-button-container__tablet hidden-xs hidden-md hidden-lg">
                                    <div class="animated-button-container__background"></div>  
                                    <div id="nav-icon3" data-toggle="collapse" data-target=".js-navbar-collapse" class="nav-icon">
                                      <span></span>
                                      <span></span>
                                      <span></span>
                                      <span></span>
                                    </div>
                                </div>
                                <span class="menu-text">МЕНЮ</span>
                                <!-- <button class="navbar-toggle" type="button" data-toggle="collapse" data-target=".js-navbar-collapse">
                                    <span class="sr-only">Toggle navigation</span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button> -->
                            </div>
                            <div class="col-xs-6">
                                <div id="search" class="search search-tablet">
                                    <input type="text" name="search" placeholder="<?PHP echo $text_search; ?>">         
                                    <div class="search-line">
                                        <button parentName="search-tablet" type="button" class="btn btn-default" name="s">
                                            <span class="glyphicon glyphicon-search" aria-hidden="true"></span>
                                        </button>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-2 tablet-cart-container">
                                <?php echo $cart; ?>
                            </div>
                            <div class="col-xs-2 tablet-login-container">
                                <a href="<?php echo $account; ?>" title="<?php echo $text_account; ?>" class="login-tablet">                                    
                                    <?php echo $text_account; ?> <img src="/catalog/view/theme/loudlemon/images/layout/login_door_white.svg" alt="Корзина" class="img-responsive login-tablet-img">
                                </a>
                            </div>
                        </div>                        
                    </div>


                    <div class="collapse navbar-collapse js-navbar-collapse">
                        <div class="row mob-nav">
                            <div class="col-xs-6 col-sm-4">
                                <a href="/guitars" class="category-header"><?PHP echo $text_guitars; ?></a>
                                <ul class="category-items">
                                    <?php 
                                    foreach ($brands as $brand) {
                                        echo "<li class=\"category-item\"><a href=\"/guitars/?filter=".$brand['id']."\">".$brand['name']."</a></li>";
                                    } ?>                                    
                                </ul>

                                <a href="/amps" class="category-header"><?PHP echo $text_amps; ?></a>
                                <ul class="category-items">
                                    <?php
                                    if($amps_watts){
                                    foreach ($amps_watts as $amps_watt) {
                                        echo "<li class=\"category-item\"><a href=\"/amps/?filter=".$amps_watt['id']."\">".$amps_watt['name']."</a></li>";
                                    }} 
                                    ?>                                    
                                </ul>

                                <a href="/bass" class="category-header"><?PHP echo $text_вass; ?></a>
                                <ul class="category-items">
                                    <?php
                                    if($bass_types){
                                    foreach ($bass_types as $bass_type) {
                                        echo "<li class=\"category-item\"><a href=\"/bass/?filter=".$bass_type['id']."\">".$bass_type['name']."</a></li>";
                                    }}
                                    ?>                                    
                                </ul>

                                <a href="/effects" class="category-header"><?PHP echo $text_effects; ?></a>
                                <ul class="category-items">
                                    <li class="category-item">
                                        <a href="/effects"><?php echo $curLang == 'ru' ? 'Все' : 'All'; ?></a>
                                    </li>              
                                </ul>
  <!--                               <div class="hidden-sm">
                                    <a class="mob-nav-item" href="/repair"><?PHP echo $text_repair; ?></a>
                                    <a class="mob-nav-item" href="/delivery"><?PHP echo $text_delivery; ?></a>
                                    <a class="mob-nav-item" href="/contacts"><?PHP echo $text_contacts; ?></a>
                                    <a class="mob-nav-item" href="/shop"> <?PHP echo $text_shop; ?> <span class="glyphicon glyphicon-home"></span></a>
                                </div>      -->                           

                            </div>
                            <div class="col-xs-6 col-sm-4">
                                <a href="/accessories" class="category-header"><?PHP echo $text_accessories; ?></a>
                                <ul class="category-items"> 
                                    <li class="category-item"><a href="/strings/">
                                         <?php if($curLang == 'en') {echo "Strings";}else {
                                            echo "Струны";
                                         }?>
                                    </a></li>
                                    <li class="category-item"><a href="/straps/">
                                    <?php if($curLang == 'en') {echo "Straps";}else {
                                            echo "Ремни";
                                         }?>                                                
                                    </a></li>
                                    <li class="category-item"><a href="/cabel/">
                                    <?php if($curLang == 'en') {echo "Cabel";}else {
                                            echo "Кабель";
                                         }?>                                                        
                                    </a></li>
                                    <li class="category-item"><a href="/parts/">
                                    <?php if($curLang == 'en') {echo "Parts";}else {
                                            echo "Запчасти";
                                         }?>                                                
                                    </a></li>
                                    <li class="category-item"><a href="/cases/">
                                    <?php if($curLang == 'en') {echo "Cases";}else {
                                            echo "Чехлы";
                                         }?>                                                
                                    </a></li>
                                    <li class="category-item"><a href="/clothes/">
                                    <?php if($curLang == 'en') {echo "Clothes";}else {
                                            echo "Одежда";
                                         }?>                                           
                                    </a></li>
                                    <li class="category-item"><a href="/staff/">
                                    <?php if($curLang == 'en') {echo "Stands";}else {
                                            echo "Стойки";
                                         }?>                                                
                                    </a></li>
                                    <li class="category-item"><a href="/tuners/">
                                    <?php if($curLang == 'en') {echo "Tuners";}else {
                                            echo "Тюнеры";
                                         }?>                                                
                                    </a></li>
                                    <li class="category-item"><a href="/slides-capo/">
                                    <?php if($curLang == 'en') {echo "Slides/Capo";}else {
                                            echo "Слайды/Каподастры";
                                         }?>                                                
                                    </a></li>                                    
                                    <li class="category-item"><a href="/mediators/">
                                    <?php if($curLang == 'en') {echo "Mediators";}else {
                                            echo "Медиаторы";
                                         }?>                                                
                                    </a></li>

                                    <li class="category-item"><a href="/pickups/">
                                    <?php if($curLang == 'en') {echo "Pickups";}else {
                                            echo "Звукосниматели";
                                         }?>                                                
                                    </a></li>

                                    <li class="category-item"><a href="/care/">
                                    <?php if($curLang == 'en') {echo "Care products";}else {
                                            echo "Средства по уходу";
                                         }?>                                                
                                    </a></li>

                                    <li class="category-item gifts-li"><a href="/gifts/"><img src="/catalog/view/theme/loudlemon/images/layout/gifts.svg" class="img-responsive gift-ico"></img>
                                    <?php if($curLang == 'en') {echo "Gifts";}else {
                                            echo "Подарки";
                                         }?>                                                
                                    </a></li>
                                    <div class="langs langs-mobile">
                                        <?php echo $language; ?>
                                    </div>
                                </ul>
                            </div>
                            <div class="col-xs-12 col-sm-3 mob-sub-menu tablet-third-col-first-sub">
                                <div class="col-xs-6 col-sm-12">   
                                    <a class="mob-nav-item" href="/shop"><?PHP echo $text_shop; ?> <span class="glyphicon glyphicon-home"></span></a>
                                    <a class="mob-nav-item" href="/repair"><?PHP echo $text_repair; ?></a>
                                    <a class="mob-nav-item" href="/delivery"><?PHP echo $text_delivery; ?></a>
                                    
                                </div>
                                <div class="col-xs-6 col-sm-12 tablet-third-col-second-sub">
                                    <a class="mob-nav-item" href="/contacts"><?PHP echo $text_contacts; ?></a>
                                    <a class="mob-nav-item login-mobile" href="<?php echo $account; ?>" title="<?php echo $text_account; ?>">   
                                        <?php echo $text_account; ?> <img src="/catalog/view/theme/loudlemon/images/layout/login_door.svg" alt="" class="login-mobile-icon">
                                    </a>
                                    <div class="langs langs-tablet">
                                        <?php echo $language; ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <ul class="nav navbar-nav <?PHP if($_SERVER['REQUEST_URI'] == '/guitars') echo 'active' ?>">
                            <li class="dropdown mega-dropdown">
                                <a href="/guitars" class="dropdown-toggle" data-toggle="dropdown"><?PHP echo $text_guitars; ?></a>

                                <ul class="dropdown-menu mega-dropdown-menu row">
                                <div class="container">
                                    <li class="col-sm-2">
                                        <ul>
                                            <li>
                                                <a href="/guitars"><p class="category-all-link"><?php echo $curLang == 'ru' ? 'Все' : 'All'; ?></p><img id="nav-pic-guitars" src="/catalog/view/theme/loudlemon/images/layout/navbar/guitar.png" class="img-responsive navpic"></a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li class="col-sm-2" style="min-width: 130px;">
                                        <ul>
                                            <li class="dropdown-header"><?PHP echo $text_brand; ?></li>
                                            <?php 
                                            foreach ($brands as $brand) {
                                                echo "<li><a href=\"/guitars/?filter=".$brand['id']."\">".$brand['name']."</a></li>";
                                            } ?>
                                        </ul>
                                    </li>
                                    <li class="col-sm-2" style="min-width: 130px;">
                                        <ul>
                                            <li class="dropdown-header"><?PHP echo $text_type; ?></li>
                                            <?php 
                                            foreach ($types as $type) {
                                                echo "<li><a href=\"/guitars/?filter=".$type['id']."\">".$type['name']."</a></li>";
                                            } ?>
                                        </ul>
                                    </li>
                                    <li class="col-sm-2" style="min-width: 130px;">
                                    <ul>
                                        <li class="dropdown-header"><?PHP echo $text_body_shape; ?></li>
                                        <?php 
                                        foreach ($bodyes as $body) {
                                                echo "<li><a href=\"/guitars/?filter=".$body['id']."\">".$body['name']."</a></li>";
                                        } ?>
                                    </ul>
                                    </li>
                                    <li class="col-sm-2"style="min-width: 130px;">
                                        <ul>
                                            <li class="dropdown-header"><?PHP echo $text_condition; ?></li>
                                            <?php 
                                            foreach ($conditions as $condition) {
                                                echo "<li><a href=\"/guitars/?filter=".$condition['id']."\">".$condition['name']."</a></li>";
                                            } ?>
                                        </ul>
                                    </li>
                                </div>
                                </ul>

                            </li>
                        </ul>
                        <ul class="nav navbar-nav">
                            <li class="dropdown mega-dropdown">
                                <a href="/bass" class="dropdown-toggle" data-toggle="dropdown"><?PHP echo $text_вass; ?></a>
                                <ul class="dropdown-menu mega-dropdown-menu row">
                                <div class="container">
                                    <li class="col-sm-2">
                                        <ul>
                                            <a href="/bass"><p class="category-all-link"><?php echo $curLang == 'ru' ? 'Все' : 'All'; ?></p><img id="nav-pic-bass" src="/catalog/view/theme/loudlemon/images/layout/navbar/bass-guitar.png" class="img-responsive navpic"></a>
                                        </ul>
                                    </li>

                                    <?php 
                                        if(!$bass_brands){
                                        ?>
                                    <li class="col-sm-2">
                                        <ul>
                                            <li class="dropdown-header"><?PHP echo $text_вass; ?></li>
                                            <li><a href="#"><?PHP echo $text_na; ?></a></li>
                                        </ul>
                                    </li>
                                        <?php
                                        }else{
                                    ?>

                                    <li class="col-sm-2">
                                        <ul>
                                            <li class="dropdown-header"><?PHP echo $text_brand; ?></li>
                                            <?php 
                                            if($bass_brands){
                                            foreach ($bass_brands as $bass_brand) {
                                                echo "<li><a href=\"/bass/?filter=".$bass_brand['id']."\">".$bass_brand['name']."</a></li>";
                                            }}else{
                                            ?>
                                            <li><a href="#">Нет товаров</a></li>
                                            <?php } ?>
                                        </ul>
                                    </li>
                                    <li class="col-sm-2">
                                        <ul>
                                            <li class="dropdown-header"><?PHP echo $text_type; ?></li>
                                            <?php 
                                            if($bass_types){
                                            foreach ($bass_types as $bass_type) {
                                                echo "<li><a href=\"/bass/?filter=".$bass_type['id']."\">".$bass_type['name']."</a></li>";
                                            }}else{
                                            ?>
                                            <li><a href="#">Нет товаров</a></li>
                                            <?php } ?>
                                        </ul>
                                    </li>
                                    <li class="col-sm-2">
                                        <ul>
                                            <li class="dropdown-header"><?PHP echo $text_body_shape; ?></li>
                                            <?php 
                                            if($bass_bodyes){
                                            foreach ($bass_bodyes as $bass_body) {
                                                echo "<li><a href=\"/bass/?filter=".$bass_body['id']."\">".$bass_body['name']."</a></li>";
                                            }}else{
                                            ?>
                                            <li><a href="#">Нет товаров</a></li>
                                            <?php } ?>
                                        </ul>
                                    </li>
                                    <li class="col-sm-2">
                                        <ul>
                                            <li class="dropdown-header"><?PHP echo $text_condition; ?></li>
                                            <?php 
                                            if($bass_conditions){
                                            foreach ($bass_conditions as $bass_condition) {
                                                echo "<li><a href=\"/bass/?filter=".$bass_condition['id']."\">".$bass_condition['name']."</a></li>";
                                            }}else{
                                            ?>
                                            <li><a href="#">Нет товаров</a></li>
                                            <?php } ?>
                                        </ul>
                                    </li>
                                    <?php } ?>
                                </div>
                                </ul>

                            </li>
                        </ul>
                        <ul class="nav navbar-nav">
                            <li class="dropdown mega-dropdown">
                                <a href="/effects" class="dropdown-toggle" data-toggle="dropdown"><?PHP echo $text_effects; ?></a>

                                <ul class="dropdown-menu mega-dropdown-menu row">
                                <div class="container">
                                    <li class="col-sm-2">
                                        <ul>
                                            <a href="/effects"><p class="category-all-link"><?php echo $curLang == 'ru' ? 'Все' : 'All'; ?></p><img id="nav-pic-effects" src="/catalog/view/theme/loudlemon/images/layout/navbar/effects.png" class="img-responsive navpic"></a>
                                        </ul>
                                    </li>
                                    <li class="col-sm-2">
                                        <ul>
                                            <li class="dropdown-header"><?PHP echo $text_brand; ?></li>
                                            <?php 
                                            if($effects_brands){
                                            foreach ($effects_brands as $effects_brand) {
                                                echo "<li><a href=\"/effects/?filter=".$effects_brand['id']."\">".$effects_brand['name']."</a></li>";
                                            }}else{
                                            ?>
                                            <li><a href="#">Нет товаров</a></li>
                                            <?php } ?>                                        </ul>
                                    </li>
                                    <li class="col-sm-2">
                                        <ul>
                                            <li class="dropdown-header"><?PHP echo $text_type; ?></li>
                                            <?php 
                                            if($effects_types){
                                            foreach ($effects_types as $effects_type) {
                                                echo "<li><a href=\"/effects/?filter=".$effects_type['id']."\">".$effects_type['name']."</a></li>";
                                            }}else{
                                            ?>
                                            <li><a href="#">Нет товаров</a></li>
                                            <?php } ?>       
                                        </ul>
                                    </li>
                                </div>
                                </ul>

                            </li>
                        </ul>
                        <ul class="nav navbar-nav">
                            <li class="dropdown mega-dropdown">
                                <a href="/amps" class="dropdown-toggle" data-toggle="dropdown"><?PHP echo $text_amps; ?></a>

                                <ul class="dropdown-menu mega-dropdown-menu row">
                                <div class="container">
                                    <li class="col-sm-2">
                                        <ul>
                                                <a href="/amps"><p class="category-all-link"><?php echo $curLang == 'ru' ? 'Все' : 'All'; ?></p><img id="nav-pic-amps" src="/catalog/view/theme/loudlemon/images/layout/navbar/intensifier.png" class="img-responsive navpic"></a>
                                        </ul>
                                    </li>

                                    <?php 
                                        if(!$amps_brands){
                                        ?>
                                    <li class="col-sm-2">
                                        <ul>
                                            <li class="dropdown-header"><?PHP echo $text_amps; ?></li>
                                            <li><a href="#"><?PHP echo $text_na; ?></a></li>
                                        </ul>
                                    </li>
                                        <?php
                                        }else{
                                    ?>

                                    <li class="col-sm-2">
                                        <ul>
                                            <li class="dropdown-header"><?PHP echo $text_brand; ?></li>
                                            <?php 
                                            if($amps_brands){
                                            foreach ($amps_brands as $amps_brand) {
                                                echo "<li><a href=\"/amps/?filter=".$amps_brand['id']."\">".$amps_brand['name']."</a></li>";
                                            }}else{
                                            ?>
                                            <li><a href="#">Нет товаров</a></li>
                                            <?php } ?>
                                        </ul>
                                    </li>
                                    <li class="col-sm-2">
                                        <ul>
                                            <li class="dropdown-header"><?PHP echo $text_condition; ?></li>
                                            <?php 
                                            if($amps_conditions){
                                            foreach ($amps_conditions as $amps_condition) {
                                                echo "<li><a href=\"/amps/?filter=".$amps_condition['id']."\">".$amps_condition['name']."</a></li>";
                                            }}else{
                                            ?>
                                            <li><a href="#">Нет товаров</a></li>
                                            <?php } ?>
                                        </ul>
                                    </li>
                                    <li class="col-sm-2">
                                        <ul>
                                            <li class="dropdown-header"><?PHP echo $text_watts; ?></li>
                                            <?php 
                                            if($amps_watts){
                                            foreach ($amps_watts as $amps_watt) {
                                                echo "<li><a href=\"/amps/?filter=".$amps_watt['id']."\">".$amps_watt['name']."</a></li>";
                                            }}else{
                                            ?>
                                            <li><a href="#">Нет товаров</a></li>
                                            <?php } ?>
                                        </ul>
                                    </li>
                                    
                                            <?php } ?>     
                                </div>
                                </ul>

                            </li>
                        </ul>
                        <ul class="nav navbar-nav">
                            <li class="dropdown mega-dropdown">
                                <a href="/accessories" class="dropdown-toggle" data-toggle="dropdown"><?PHP echo $text_accessories; ?></a>

                                <ul class="dropdown-menu mega-dropdown-menu row">
                                <div class="container">
                                    <li class="col-sm-2">
                                        <ul>
                                            <a href="/strings">
                                                <p class="category-all-link"><?php echo $curLang == 'ru' ? 'Все' : 'All'; ?></p>
                                                <img id="nav-pic-accessories" src="/catalog/view/theme/loudlemon/images/layout/navbar/accessory.png" class="img-responsive navpic">
                                            </a>
                                        </ul>
                                    </li>
                                    <li class="col-sm-2">
                                        <ul>
                                            <li class="dropdown-header"><?PHP echo $text_accessories; ?></li>
                                            <?php 
//                                            if($accessories){
 //                                           foreach ($accessories as $accessorie) {
 //<a href=\"/accessories/?filter=".$accessorie['id']."\"></a>
  //                                              echo "<li>".$accessorie['name']."</li>";
   //                                         }}else{
                                            ?>
                                            <li><a href="/strings/">
                                                 <?php if($curLang == 'en') {echo "Strings";}else {
                                                    echo "Струны";
                                                 }?>
                                            </a></li>
                                            <li><a href="/straps/">
                                            <?php if($curLang == 'en') {echo "Straps";}else {
                                                    echo "Ремни";
                                                 }?>                                                
                                            </a></li>
                                            <li><a href="/cabel/">
                                            <?php if($curLang == 'en') {echo "Cabel";}else {
                                                    echo "Кабель";
                                                 }?>                                                        
                                            </a></li>
                                            <li><a href="/parts/">
                                            <?php if($curLang == 'en') {echo "Parts";}else {
                                                    echo "Запчасти";
                                                 }?>                                                
                                            </a></li>
                                            <li><a href="/cases/">
                                            <?php if($curLang == 'en') {echo "Cases";}else {
                                                    echo "Чехлы";
                                                 }?>                                                
                                            </a></li>
                                            <li><a href="/clothes/">
                                            <?php if($curLang == 'en') {echo "Clothes";}else {
                                                    echo "Одежда";
                                                 }?>                                           
                                            </a></li>
                                            <li><a href="/staff/">
                                            <?php if($curLang == 'en') {echo "Stands";}else {
                                                    echo "Стойки";
                                                 }?>                                                
                                            </a></li>
                                            <li><a href="/tuners/">
                                            <?php if($curLang == 'en') {echo "Tuners";}else {
                                                    echo "Тюнеры";
                                                 }?>                                                
                                            </a></li>
                                            <li><a href="/slides-capo/">
                                            <?php if($curLang == 'en') {echo "Slides/Capo";}else {
                                                    echo "Слайды/Каподастры";
                                                 }?>                                                
                                            </a></li>
                                            <?php //} ?>
                                        </ul>
                                    </li>
                                    <li class="col-sm-2">
                                        <ul>
                                            <li class="dropdown-header" style="opacity: .01;"><?PHP echo $text_accessories; ?></li>
                                            <li><a href="/mediators/">
                                            <?php if($curLang == 'en') {echo "Mediators";}else {
                                                    echo "Медиаторы";
                                                 }?>                                                
                                            </a></li>

                                            <li><a href="/pickups/">
                                            <?php if($curLang == 'en') {echo "Pickups";}else {
                                                    echo "Звукосниматели";
                                                 }?>                                                
                                            </a></li>

                                            <li><a href="/care/">
                                            <?php if($curLang == 'en') {echo "Care products";}else {
                                                    echo "Средства по уходу";
                                                 }?>                                                
                                            </a></li>

                                            <li class="gifts-li"><a href="/gifts/"><img src="/catalog/view/theme/loudlemon/images/layout/gifts.svg" class="img-responsive gift-ico"></img>
                                            <?php if($curLang == 'en') {echo "Gifts";}else {
                                                    echo "Подарки";
                                                 }?>                                                
                                            </a></li>
                                            <?php //} ?>
                                        </ul>
                                    </li>
                                </div>
                                </ul>

                            </li>
                        </ul>
                        <ul class="nav navbar-nav <?PHP if($_SERVER['REQUEST_URI'] == '/shop') echo 'active' ?>">
                            <li>
                                <a href="/shop"><span class="glyphicon glyphicon-home"></span> <?PHP echo $text_shop; ?></a>
                            </li>
                        </ul>
                        <ul class="nav navbar-nav <?PHP if($_SERVER['REQUEST_URI'] == '/repair') echo 'active' ?>">        
                            <li>
                                <a href="/repair"><?PHP echo $text_repair; ?></a>
                            </li>
                        </ul>
                        <ul class="nav navbar-nav <?PHP if($_SERVER['REQUEST_URI'] == '/delivery') echo 'active' ?>">
                            <li>
                                <a href="/delivery"><?PHP echo $text_delivery; ?></a>
                            </li>
                        </ul>
                        <ul class="nav navbar-nav <?PHP if($_SERVER['REQUEST_URI'] == '/contacts') echo 'active' ?>">
                            <li>
                                <a href="/contacts"><?PHP echo $text_contacts; ?></a>
                            </li>
                        </ul>
                    </div><!-- /.nav-collapse -->
                </div>
            </nav>
        </header>