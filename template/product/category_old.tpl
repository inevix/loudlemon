<?php //phpinfo();?>
<?php echo $header; ?>
<img src="catalog/view/theme/loudlemon/images/layout/banners/catalog.jpg" class="img-responsive visible-md-block visible-lg-block">
<div class="container main catalog">
            <?php if (!$categories && !$products) { ?>
            <?php
            $defaultCat = false;
                switch ($_SERVER['REQUEST_URI']) {
    case '/bass/':
            $catMsg = $text_empty_bass?$text_empty_bass:$text_empty;
    break;
    case '/amps/':
            $catMsg = $text_empty_amps?$text_empty_amps:$text_empty;
    break;
    case '/effects/':
            $catMsg = $text_empty_effects?$text_empty_effects:$text_empty;
    break;
    case '/accessories/':
            $catMsg = $text_empty_accessories?$text_empty_accessories:$text_empty;
    break;
        default:
            $defaultCat = true;    
        break;
}
            ?>
<?php 
if($defaultCat !== true){?>
<h2>
<?php echo $catMsg; ?>
</h2>
</div>
<?php
    echo $footer;
    die();
}
            } ?>
<?php
if(isset($_GET['order'])){
    $order = $_GET['order']=='ASC'?'DESC':'ASC';
}else{
    $order = 'DESC';
}
if(isset($_GET['filter'])){
    $filter = $_GET['filter']?'&filter='.$_GET['filter']:'';
}else {
    $filter = '';
}

if(isset($_GET['pmin'])){
    $pminQ = $_GET['pmin']?'&pmin='.$_GET['pmin']:'';
}else {
    $pminQ = '';
}

if(isset($_GET['pmax'])){
    $pmaxQ = $_GET['pmax']?'&pmax='.$_GET['pmax']:'';
}else {
    $pmaxQ = '';
}
?>
    <br>
    <div id="title-block">
        <div class="row">
            <div class="col-xs-5 col-sm-6 col-md-3 col-lg-3"><span class="title"><?php echo $heading_title; ?></span></div>
            <div class="col-xs-7 col-sm-6 col-md-9 col-lg-9 text-right" style="color: #666; padding-top: 8px;">
                <?php echo $text_sort; ?> 
                <a href="<?php echo $_SERVER['REDIRECT_URL']; ?>?sort=pd.name&order=<?php echo $order.$filter.$pminQ.$pmaxQ; ?>" class="sort-link"><?php echo $text_sortName;?></a>
                <a href="<?php echo $_SERVER['REDIRECT_URL']; ?>?sort=p.price&order=<?php echo $order.$filter.$pminQ.$pmaxQ; ?>" class="sort-link"><?php echo $text_sortPrice;?></a>            
            </div>
        </div>
    </div>
    <p>
        <?php if (!empty($description)) { ?>
        <?php echo $description;
        } ?>
    </p>
    <p style="margin-top: 14px;">
    </p><div></div>
    <div class="row">
        <?php $bootsrapClasses[0] = 'col-xs-12 col-sm-12 col-md-12 col-lg-12'; ?>
        <?php $bootsrapClasses[1] = 'col-xs-6 col-sm-4 col-md-3 col-lg-3'; ?>
        <?php if($category !== 65){ ?>
        <?php $bootsrapClasses[0] = 'col-xs-12 col-sm-8 col-md-9 col-lg-9'; ?>
        <?php $bootsrapClasses[1] = 'col-xs-6 col-sm-6 col-md-4 col-lg-4'; ?>
        <div class="col-xs-12 col-sm-4 col-md-3 col-lg-3">
            <?php echo $column_left; ?>            
        </div>
        <?php } ?>
        <?php $class = 'col-xs-12 col-sm-12 col-md-9 col-lg-9'; ?>
        <div id="content" class="<?php echo $bootsrapClasses[0]; ?>">
            <?php if ($products) { ?>
            <div class="row catalog-items">
                <?php foreach ($products as $product) { ?>
                <div class="<?php echo $bootsrapClasses[1]; ?>">
                    <div id="product-in-cat">
                        <a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" class="img-responsive"></a>
                        <div class="description" style="height: 41px;">
                        <div class="productName">
                            <a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></div>
                        </div>
                        <div style="width: 95%;">
                           <?php if ($product['price']) { ?>
                            <p class="price" style="margin: 0; height: 55px;">
                                <?php if (!$product['special']) { ?>
                                <?php echo '<span class="price-title" style="display: -webkit-inline-box; display: -moz-inline-box; margin-top: 7px;">'.$product['price'].'</span>'; ?>
                                <?php } else { ?>
                                <span class="price-title" style="font-size: 14px; text-decoration: line-through; display: -webkit-inline-box;  display: -moz-inline-box;"><?php echo $product['price']; ?></span>
                                <br>
                                <span class="price-title"><?php echo $product['special']; ?></span>
                                <?php } ?>
                            </p>
                            <?php } ?>
                        </div>
                    </div>
                </div>                
                <?php } ?>
            </div>
            <div class="row">
                <div class="col-sm-6 col-xs-6 text-left"><?php echo $pagination; ?></div>
                <div class="col-sm-6 col-xs-6 text-right">
                    <div style="padding-top: 25px;">
                <?php echo $results; ?>
                    </div>
                </div>
            </div>
            <?php } ?>
            <?php if (!$categories && !$products) { ?>
            <p><?php echo $text_empty; ?></p>
            <div class="buttons">
                <div class="pull-right"><a href="<?php echo $continue; ?>" class="btn btn-primary"><?php echo $button_continue; ?></a></div>
            </div>
            <?php } ?>
            <?php echo $content_bottom; ?></div>
        <?php echo $column_right; ?>
    </div>
            <br><br>
            <div class="up">
                <br>
                <a href="<?PHP echo $_SERVER['REQUEST_URI'] ?>#"><span class="glyphicon glyphicon-arrow-up"></span> Наверх</a>
                <br><br>
            </div>
</div>
<script type="text/javascript">
                var resized;

              if($( window ).width() < 767 && resized != true){
                $('.glyphicon-triangle-bottom').addClass('glyphicon-triangle-right');
                $('.glyphicon-triangle-bottom').removeClass('glyphicon-triangle-bottom');
            }

            $( window ).resize(function() {
              if($( window ).width() < 767 && resized != true){
                if($('.glyphicon').hasClass('glyphicon-triangle-bottom')){
                $('.glyphicon-triangle-bottom').addClass('glyphicon-triangle-right');
                $('.glyphicon-triangle-bottom').removeClass('glyphicon-triangle-bottom');
                resized = true;
                }
              }
            });    
</script>
<?php echo $footer; ?>
