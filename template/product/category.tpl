<?php echo $header; //catalog/view/theme/loudlemon/template/product/
?>
<?php if ($banner) {
    echo '<ul id="scene"><div bannerUrl="' . $banner . '" class="fixedbackground layer" data-depth="0.20" style="background-image: url(' . $banner . ')">';
    if ($banner_text) {
        echo '<img class="img-responsive front-image layer" data-depth="0.1" src="/' . $banner_text . '">';
    }
    echo '</div></ul>';
} ?>

<div class="container-main-wrapper">
    <div class="container large-wider-container main catalog">
        <?php if (!$defaultCattegories && !$products) {

        $defaultCat = false;
        switch ($_SERVER['REQUEST_URI']) {
            case '/bass/':
                $catMsg = $text_empty_bass ? $text_empty_bass : $text_empty;
                break;
            case '/amps/':
                $catMsg = $text_empty_amps ? $text_empty_amps : $text_empty;
                break;
            case '/effects/':
                $catMsg = $text_empty_effects ? $text_empty_effects : $text_empty;
                break;
            case '/accessories/':
                $catMsg = $text_empty_accessories ? $text_empty_accessories : $text_empty;
                break;
            default:
                $defaultCat = true;
                break;
        }
        ?>
        <?php
        if ($defaultCat !== true){
        ?>
        <h2 id="empty-cat">
            <?php echo $catMsg; ?>
        </h2>
    </div>
</div>
<?php
echo $footer;
die();
}
} ?>
<?php
if (isset($_GET['order'])) {
    $order = $_GET['order'] == 'ASC' ? 'ASC' : 'DESC';
}
else {
    $order = 'ASC';
}
if (isset($_GET['filter'])) {
    $filter = $_GET['filter'] ? '&filter=' . $_GET['filter'] : '';
}
else {
    $filter = '';
}

if (isset($_GET['pmin'])) {
    $pminQ = $_GET['pmin'] ? '&pmin=' . $_GET['pmin'] : '';
}
else {
    $pminQ = '';
}

if (isset($_GET['pmax'])) {
    $pmaxQ = $_GET['pmax'] ? '&pmax=' . $_GET['pmax'] : '';
}
else {
    $pmaxQ = '';
}
?>
<div id="title-block">
    <div class="row">
        <div class="col-xs-12 col-sm-7">
            <ul class="breadcrumb">
                <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
                <?php } ?>
            </ul>
        </div>
        <div class="hidden-xs col-xs-12 col-sm-5 text-right sort-container" style="color: #666;">
           <?php $url = substr($_SERVER['REQUEST_URI'],0,strpos($_SERVER['REQUEST_URI'],'?')); ?>
            <?php echo $text_sort; ?>
            <?php foreach ($sorts as $sorts) { ?>
              <?php if ($sorts['value'] == $sort . '-' . $order) { ?>
                  <?php if ($sort=='pd.name'){ ?>
                    <?php if ($order=='ASC'){ ?>
                        <a href="<?php echo $name_desc; ?>" class="sort-link sort-active sort-ASC"><?php echo $text_sortName; ?></a>
                        <a href="<?php echo $price_asc; ?>" class="sort-link"><?php echo $text_sortPrice; ?></a>
                    <?php }else{ ?>
                        <a href="<?php echo $name_asc; ?>" class="sort-link sort-active sort-DESC"><?php echo $text_sortName; ?></a>
                        <a href="<?php echo $price_asc; ?>" class="sort-link"><?php echo $text_sortPrice; ?></a>
                    <?php } ?>
                  <?php }elseif($sort=='p.price'){ ?>
                    <?php if ($order=='ASC'){ ?>
                        <a href="<?php echo $name_asc; ?>" class="sort-link"><?php echo $text_sortName; ?></a>
                        <a href="<?php echo $price_desc; ?>" class="sort-link sort-active sort-ASC"><?php echo $text_sortPrice; ?></a>
                    <?php }else{ ?>
                        <a href="<?php echo $name_asc; ?>" class="sort-link"><?php echo $text_sortName; ?></a>
                        <a href="<?php echo $price_asc; ?>" class="sort-link sort-active sort-DESC"><?php echo $text_sortPrice; ?></a>
                    <?php } ?>
                  <?php }else{ ?>
                    <a href="<?php echo $name_asc; ?>" class="sort-link"><?php echo $text_sortName; ?></a>
                    <a href="<?php echo $price_asc; ?>" class="sort-link"><?php echo $text_sortPrice; ?></a>
                  <?php } ?>
               <?php } ?>
            <?php } ?>
        </div>
        
        <div class="fader hidden"></div>
    </div>
</div>
<div class="row mobile-filter-buttons visible-xs-block">
	<div class="mobile-filter-button col-xs-6">Фильтры</div>
	<div class="mobile-sort-button col-xs-6">Сортировка</div>
</div>
<div class="row">
    <?php
    $bootsrapClasses[0] = 'col-xs-12 col-sm-12 col-md-12 col-lg-12';


    $bootsrapClasses[0] = 'col-xs-12 col-sm-8 col-md-9 col-lg-9 col-xlg-10';
    $urlArray = explode('?', $_SERVER['REQUEST_URI']);

    if (strlen($column_left) < 30) {
        $bootsrapClasses[0] = 'col-xs-12 col-sm-12 col-md-12 col-lg-12';
    }
    else { ?>
        <div class="hidden-xs col-xs-12 col-sm-4 col-md-3 col-lg-3 col-xlg-2 filter-col sticky-filter">
            <?php echo $column_left; ?>
        </div>
        <?php /* ?>
        <div class="apply-button col-xs-12 hidden-xs hidden-sm hidden-md hidden-lg hidden-xlg" sfdsdfdsonclick="$('.filter-col').toggleClass('hidden-xs')">
	        	<div class="lemon-button"><?php echo $button_apply; ?></div>
		 </div>
         <?php */ ?>
        <?php } ?>

    <?php //$class = 'col-xs-12 col-sm-12 col-md-9 col-lg-9'; ?>
    <div id="content" class="<?php echo $bootsrapClasses[0]; ?>"> 
		<h1><?php echo $heading_title;?></h1>
		<?php if ($description && $description != '<p><br></p>') { ?>
			<div class="description">
				 <?php echo $description; ?>
			</div>
		<?php } ?>
        <?php /* if ($products) { ?>
            <div class="row catalog-items">
                <?php foreach ($products as $product) { ?>
                    <div class="<?php echo $bootsrapClasses[1]; ?>">
                        <div id="product-in-cat">
                            <a href="<?php echo $product['href']; ?>">
                                <img src="<?php
                                $urlArray = explode('?', $_SERVER['REQUEST_URI']);
                                if ($urlArray[0] == "/amps/") {
                                    echo $product['imgAmps'];
                                }
                                else {
                                    echo $product['thumb'];
                                } ?>" class="img-responsive">
                            </a>
                            <div class="description" style="height: 41px;">
                                <div class="productName">
                                    <a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a>
                                </div>
                            </div>
                            <div style="width: 95%;">
                                <?php if ($product['price']) { ?>
                                    <p class="price" style="margin: 0; height: 55px;">
                                        <?php if (!$product['special']) { ?>
                                            <?php echo '<span class="price-title" style="display: inline; margin-top: 7px;">' . $product['price'] . '</span>'; ?>
                                        <?php }
                                        else { ?>
                                            <span class="price-title"
                                                  style="font-size: 14px; text-decoration: line-through; display: -webkit-inline-box;  display: -moz-inline-box;"><?php echo $product['price']; ?></span>
                                            <br>
                                            <span
                                                class="price-title"><?php echo $product['special']; ?></span>
                                        <?php } ?>
                                    </p>
                                <?php } ?>
                            </div>

                            <?php
                            $label = null;
                            $src = "";
                            foreach ($product as $key => $values) {

                                if ($key === 'label') {

                                    foreach ($values as $value => $mean) {
                                        if ($mean)
                                            $label = $mean;
                                    }
                                }
                            }
                            if ($label) {
                                $src = "/catalog/view/theme/loudlemon/images/layout/stikers/sticker_" . $label . "_preview.svg";
                            }

                            ?>
                            <img src="<?= $src ?>" alt="" class="stiker"
                                 style="margin-top: 4px;">
                        </div>
                    </div>
                <?php } ?>
            </div>
        <?php } ?>
        <?php if (!$categories && !$products) { ?>
            <p><?php echo $text_empty; ?></p>
            <div class="buttons">
                <div class="pull-right"><a href="<?php echo $continue; ?>"
                                           class="btn btn-primary"><?php echo $button_continue; ?></a>
                </div>
            </div>
        <?php } ?>
        <?php echo $content_bottom; ?><?php */ ?>
        <?=$list //ajax_filter_list.tpl?>

<div class="row pagin-container">
    <?php echo $pagination; ?>
    <!-- <div class="col-xs-12 text-center" style="margin-top:-15px;"><?php echo $pagination; ?></div>
        <div class="col-xs-12 text-center">
            <div style="padding-top: 10px; margin-bottom: -25px;">
                <?php //echo $results; ?>
            </div>
        </div> -->
</div>
    <?php echo $column_right; ?>
     </div>
</div>
</div>
<script type="text/javascript">

    // var resized;

    // if ($(window).width() < 767 && resized != true) {
    //     $('.glyphicon-triangle-bottom').addClass('glyphicon-triangle-right');
    //     $('.glyphicon-triangle-bottom').removeClass('glyphicon-triangle-bottom');
    // }

    // $(window).resize(function () {
    //     if ($(window).width() < 767 && resized != true) {
    //         if ($('.glyphicon').hasClass('glyphicon-triangle-bottom')) {
    //             $('.glyphicon-triangle-bottom').addClass('glyphicon-triangle-right');
    //             $('.glyphicon-triangle-bottom').removeClass('glyphicon-triangle-bottom');
    //             resized = true;
    //         }
    //     }
    // });
    <?php
    if (!$opened_titles) { ?>
    $('.ft1').trigger('click');
    $('.ft2').trigger('click');
    $('.ft4').trigger('click');
    <?php } ?>

    // $('.filter-title').each(function(){
    //     if(!$(this).hasClass('ft1') && !$(this).hasClass('ft2') && !$(this).hasClass('ft4')) {
    //         // console.log('a');
    //         var inputs = $(this).next().find('input:checked');
    //         inputs.each(function(){
    //             console.log($(this).attr('checked'));
    //         });

    //         // $(this).next().children("input").each(function(){
    //         //     console.log('asdfs');
    //         // });
    //     }
    // });

    $(document).ready(function () {
        $(window).bind('popstate', function () {
            $.ajax({
                url: location.href + '&for_ajax=true',
                success: function (json) {

                    $('#content').html(json['list']);
                    $('.pagin-container').html(json['pagination']);
                    $('.filter').parent().html(json['filter']);
                }
            });
            		        // $(window).trigger('popstate');
 //       	<div class="mobile-filter-button col-xs-6">Фильтры</div>
	// <div class="mobile-sort-button col-xs-6">Сортировка</div>

        });
        
	

    });

</script>
<?php echo $footer; ?>
