<?php echo $header; ?>
<div class="container">
  <div class="row"><?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
      <div style="background-color: #e6e6e6; padding: 10px; margin: 20px 0;">
        <h3 style="margin-top: 0;"><?php echo $heading_title; ?></h3>
        <label class="control-label" for="input-search"><?php echo $entry_search; ?></label>
        <div class="row">
          <div class="col-sm-4">
            <input type="text" name="search" value="<?php echo $search; ?>" placeholder="<?php echo $text_keyword; ?>" id="input-search" class="form-control" />
          </div>
          <div class="col-sm-3">
            <select name="category_id" class="form-control">
              <option value="0"><?php echo $text_category; ?></option>
              <?php foreach ($categories as $category_1) { ?>
              <?php if ($category_1['category_id'] == $category_id) { ?>
              <option value="<?php echo $category_1['category_id']; ?>" selected="selected"><?php echo $category_1['name']; ?></option>
              <?php } else { ?>
              <option value="<?php echo $category_1['category_id']; ?>"><?php echo $category_1['name']; ?></option>
              <?php } ?>
              <?php foreach ($category_1['children'] as $category_2) { ?>
              <?php if ($category_2['category_id'] == $category_id) { ?>
              <option value="<?php echo $category_2['category_id']; ?>" selected="selected">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $category_2['name']; ?></option>
              <?php } else { ?>
              <option value="<?php echo $category_2['category_id']; ?>">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $category_2['name']; ?></option>
              <?php } ?>
              <?php foreach ($category_2['children'] as $category_3) { ?>
              <?php if ($category_3['category_id'] == $category_id) { ?>
              <option value="<?php echo $category_3['category_id']; ?>" selected="selected">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $category_3['name']; ?></option>
              <?php } else { ?>
              <option value="<?php echo $category_3['category_id']; ?>">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $category_3['name']; ?></option>
              <?php } ?>
              <?php } ?>
              <?php } ?>
              <?php } ?>
            </select>
          </div>
        </div>
        <p style="padding: 0;">
          <label class="checkbox-inline" style="margin-top: 9px;">
            <?php if ($description) { ?>
            <input type="checkbox" name="description" value="1" id="description" checked="checked" />
            <?php } else { ?>
            <input type="checkbox" name="description" value="1" id="description" />
            <?php } ?>
            <?php echo $entry_description; ?></label>
        </p>

 
  <?php
  if(isset($_GET['order'])){
  $order = $_GET['order']?$_GET['order']:false;
  }
  if(isset($_GET['search'])){
  $search_query = 'search='.$_GET['search'].'&';
  }else{
    $search_query = '';
  }
  if(isset($_GET['description'])){
  $descriptionOp = 'description=true&';
  }else{
  $descriptionOp = '';
  }
  ?>
      <br>
      <div id="title-block" style="margin-top: -10px; margin-bottom: 10px;">
          <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
            <div class="buttons" style="margin-top: 1px;">
              <input type="button" value="<?php echo $button_search; ?>" id="button-search" class="btn btn-primary" style="border-radius: 0;" />
            </div>
            </div>
              <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 text-right" style="color: #666; padding-top: 8px;">
                  <?php echo $text_sort; ?> 
                  <a href="<?php echo $_SERVER['REDIRECT_URL']; ?>?<?php echo $search_query; echo $descriptionOp; ?>sort=pd.name&order=<?php if($order === 'ASC') {echo 'DESC';} else { echo 'ASC'; } ?>" class="sort-link<?php echo $current_sort == 'pd.name' ? ' sort-active sort-' . $order : '';?>"><?php echo $text_sortName;?></a>
                  <a href="<?php echo $_SERVER['REDIRECT_URL']; ?>?<?php echo $search_query; echo $descriptionOp; ?>sort=p.price&order=<?php if($order === 'ASC') {echo 'DESC';} else { echo 'ASC'; } ?>" class="sort-link<?php echo $current_sort == 'p.price' ? ' sort-active sort-' . $order : '';?>"><?php echo $text_sortPrice;?></a>            
              </div>
          </div>
      </div>
    </div>
    
    <?php echo $list; ?>
    
      <?php echo $content_bottom; ?></div>
    <?php echo $column_right; ?></div>
</div>
<div class="row pagin-container">
    <?php echo $pagination; ?>
</div>
<script type="text/javascript"><!--
$('#button-search').bind('click', function() {
	url = 'index.php?route=product/search';

	var search = $('#content input[name=\'search\']').prop('value');

	if (search) {
		url += '&search=' + encodeURIComponent(search);
	}

	var category_id = $('#content select[name=\'category_id\']').prop('value');

	if (category_id > 0) {
		url += '&category_id=' + encodeURIComponent(category_id);
	}

	var sub_category = $('#content input[name=\'sub_category\']:checked').prop('value');

	if (sub_category) {
		url += '&sub_category=true';
	}

	var filter_description = $('#content input[name=\'description\']:checked').prop('value');

	if (filter_description) {
		url += '&description=true';
	}

	location = url;
});

$('#content input[name=\'search\']').bind('keydown', function(e) {
	if (e.keyCode == 13) {
		$('#button-search').trigger('click');
	}
});

$('select[name=\'category_id\']').on('change', function() {
	if (this.value == '0') {
		$('input[name=\'sub_category\']').prop('disabled', true);
	} else {
		$('input[name=\'sub_category\']').prop('disabled', false);
	}
});

$('select[name=\'category_id\']').trigger('change');
--></script>
<?php echo $footer; ?>
