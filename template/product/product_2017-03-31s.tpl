<?php echo $header; //catalog/view/theme/loudlemon/template/product/
  function label($labels){
    $mark = NULL;
    $src = "";
    foreach ($labels as $label){ 
        if ($label){  
            $mark = $label;
        }
    } 
    if ($mark)
    {
        return $src = "/catalog/view/theme/loudlemon/images/layout/stikers/sticker_" . $mark . "_preview.svg";
    }
    return '';
}
?>
<script>
    $(document).ready(function(){
        var owl = $('.owl-carousel');
        owl.owlCarousel({
            margin:10,
            nav: false,
            loop: true,
            navContainer: '',
            responsive:{
                0:{
                    items:4
                },
                600:{
                    items:5
                },
                992:{
                    items:4
                },
                1200:{
                    items:5
                }
            }
        });
        var mytimer;
        owl.on('mousewheel', '.owl-stage', function (e) {
            if(mytimer !== ($.now()/100).toFixed(0)){
                if (e.originalEvent.deltaY < 0) {
                    owl.trigger('next.owl');
                } else {
                    owl.trigger('prev.owl');
                }
                mytimer = ($.now()/100).toFixed(0);
            }
            e.preventDefault();
        });
        $(".gal-prev").click(function(){
            owl.trigger('prev.owl.carousel');
        });
        $(".gal-next").click(function(){
            owl.trigger('next.owl.carousel');
        })

    });
</script>
<br>
            <div class="container">
              <ul class="breadcrumb">
              <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                  <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
              <?php } ?>
              </ul>
               <div class="row product-row">
                   <div class="col-xs-12 col-sm-12 col-md-5 col-lg-6 prod-col-1">
                   <!-- <a id="product_main_link" target="_blank" href="/image/<?php echo $realSizeImageDef;?>"> -->  
               <?php
                    $src = label($labels);
                ?>    
               <div data-zoom="/image/<?php echo $realSizeImageDef;?>" class="zoom">
    					   <img id="product_main_img" src="<?php
    					   if($category_id == '63'){
    					  	 echo $amp_image;
    					   }else {
    					  	 echo $popup;
    					   }
    			
                           ?>" class="img-responsive">
                           <img src="<?= $src?>" alt="" class="stiker" width="20%" style="margin-top: 10px;">
               </div>
  
                   <!-- </a> -->
                       <?php if ($images) { ?>
                         <div class="gallery-wrapper">
                         <div class="gal-prev"></div>
                         <div class="gal-next"></div>
                         <div class="gallery">
                             <div class="owl-carousel">
                                 <?php foreach ($images as $image) { ?>
                                 <div class="gallery-item">
                                     <img src="<?php echo $image['thumb']; ?>" class="img-responsive thumb_click" data-big="<?php
               if(strpos($_SERVER['REQUEST_URI'],'amps')){
                 echo $image['amp_image_real'];
               }else {
                 echo $image['popup'];
               }
               ?>" data-big-ori="<?php echo $image['realSizeImage']; ?>">
                                 </div>
                                 <?php } ?>
                             </div>
                         </div>
                       </div>
                       <?php } ?>
                   </div>
                   <!--
                   <div class="col-xs-3 col-sm-3 col-md-1 col-lg-1" id="slider-row">
                       <?php if ($images) { ?>
             <div  id="slider3">
            <div class="thumbelina-but vert top">&#708;</div>
            <ul>
                <?php foreach ($images as $image) { ?>
                <li>
                  <img src="<?php echo $image['thumb']; ?>" class="img-responsive thumb_click" data-big="<?php echo $image['popup']; ?>" data-big-ori="<?php echo $image['realSizeImage']; ?>"></li>
            <?php } ?>
            </ul>
            <div class="thumbelina-but vert bottom">&#709;</div>
        </div>
            <?php } ?>
                   </div>
                   -->
                   <div class="col-xs-12 col-sm-12 col-md-7 col-lg-6 prod-col-2">
                   <div id="product_char">
                        <h3><?php echo $heading_title; ?></h3>
                        <p>
                            <?php echo $description; ?>
                        </p>
                           <table id="product-attrs">

                <?php foreach ($attribute_groups as $attribute_group) { ?>

                    <?php foreach ($attribute_group['attribute'] as $attribute) { ?>
                     <tr>
                               <td><?php echo $attribute['name']; ?>:</td>
                               <td><?php echo $attribute['text']; ?></td>
                      </tr>
                   <?php } ?>
                <?php } ?>
                           </table>
                        <span style="text-align: center; display: block;"><?php echo $text_stock; ?> <?php echo $stock; ?></span><br>
                        <style>
                        	
                        	
                        	.trigger-etc-list img {
                        		max-width: 50px;
                        		 margin: auto;
                        	}
                        	
                        	.trigger-etc-list > div {
                        		padding-left: 0;
                        		
                        		padding-bottom: 15px;
                        		
                        	}
                        	
                        	@media (min-width: 768px) {
                        		.trigger-etc-list > div {
                        			width: 20%;
                        		}
                        		
                        		.trigger-etc-list > div:nth-of-type(5) {
                        			padding-right: 0;
                        		}
                        	}
                        	
                        	
                        	
                        	
                        </style>
                        <div class="row trigger-etc-list">
                            <?php 
                            $src = '';
                            foreach ($triggers as $trigger)
                            {
                                foreach ($trigger as $key => $value)
                                {
                                    if (($value) and ($key !== 'made_in'))
                                    {
                                        $src = "/catalog/view/theme/loudlemon/images/layout/triggers/trigger_" . $key . ".svg";
                                      
                            ?>
                            <div class="col-xs-4 col-md-2">
                            	<img src="<?= $src?>" alt="" class="img-responsive">
                            </div>
                            <?php

                                    }
                                    if (($value) and ($key === 'made_in'))
                                    {
                                        $src = "/catalog/view/theme/loudlemon/images/layout/triggers/trigger_" . $value . ".svg";
                                        
                            ?>
                            <div class="col-xs-4 col-md-2">
                            	<img src="<?= $src?>" alt="" class="img-responsive">
                            </div>
                            <?php
                                    }
                                }
                            }?>
                        </div>
                        <?php if ($price) { ?>
            <?php if (!$special) { ?>
            <span class="price-title"><?php echo $price; ?></span>
              <?php } else { ?>
            <span class="price-title" style="text-decoration: line-through; font-size: 14px;">
              <?php echo $price; ?>
            </span>
            <span class="price-title">
            <?php echo $special; ?></span>
            <?php } ?>
            <?php if ($discounts) { ?>
              <hr>
            <?php foreach ($discounts as $discount) { ?>
            <h3><?php echo $discount['quantity']; ?><?php echo $text_discount; ?><?php echo $discount['price']; ?></h3>
            <?php } ?>
            <?php } ?>
          <?php } ?>
          <div id="product">
          <div class="form-group" style="display: flex; margin-top: 10px;">
              <input type="hidden" name="quantity" value="<?php echo $minimum; ?>" id="input-quantity" class="form-control" />
              <input type="hidden" name="product_id" value="<?php echo $product_id; ?>" />
              <br />
              <button type="button" id="button-cart" data-loading-text="<?php echo $text_loading; ?>" class="btn-product"><?php echo $button_cart; ?></button>
              <button type="button" id="button-test" data-loading-text="<?php echo $text_loading; ?>" class="btn-product"><?php echo "Запись на тест"; ?></button>
            </div>
          </div>
        </div>
        <div class="product-payments">
          <div class="row payments-row">
            <div class="col-xs-2 vcenter"><img src="/catalog/view/theme/loudlemon/images/layout/payment/pay_yandex.svg" alt="" class="img-responsive"></div>
            <div class="col-xs-2 vcenter"><img src="/catalog/view/theme/loudlemon/images/layout/payment/pay_mastercard.svg" alt="" class="img-responsive"></div>
            <div class="col-xs-2 vcenter"><img src="/catalog/view/theme/loudlemon/images/layout/payment/pay_maestro.svg" alt="" class="img-responsive"></div>
            <div class="col-xs-2 vcenter"><img src="/catalog/view/theme/loudlemon/images/layout/payment/pay_visa.svg" alt="" class="img-responsive"></div>
            <div class="col-xs-2 vcenter"><img src="/catalog/view/theme/loudlemon/images/layout/payment/pay_mir.png" alt="" class="img-responsive"></div>
            <div class="col-xs-2 vcenter"><img src="/catalog/view/theme/loudlemon/images/layout/payment/pay_qiwi.svg" alt="" class="img-responsive"></div>
          </div>
        </div>

      </div>
               </div>
                <br>
                <?php if($youtube){ ?>
                 <iframe id="youtubeProd" width="101.8%" height="315" src="<?php echo $youtube; ?>" frameborder="0" allowfullscreen></iframe>

                <?php }?>

                      <?php if ($accessories) { ?>
                      <h4 style="border-bottom: 1px solid #cccccc;"><div id="rel_prod_text"><?php echo $text_access_related; ?></div></h4>
                      <br>
                      <div class="row">
                        <?php $i = 0; ?>
                        <?php foreach ($accessories as $product) { ?>

                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6" id="rel-prod">
                          <div class="product-thumb transition description">
                            <div class="image">
                            <a href="<?php echo $product['href']; ?>">
                            <img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-responsive" /></a></div>
                            <div class="caption">
                              <a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a>
                            </div>
                                          <?php if ($product['price']) { ?>
                <p class="price" style="margin: 0; height: 55px;">
                                                <?php if (!$product['special']) { ?>
                                                <?php echo '<span class="price-title" style="display: -webkit-inline-box; display: -moz-inline-box; margin-top: 7px;">'.$product['price'].'</span>'; ?>
                                                <?php } else { ?>
                                                <span class="price-title" style="font-size: 14px; text-decoration: line-through; display: -webkit-inline-box;  display: -moz-inline-box;"><?php echo $product['price']; ?></span>
                                                <br>
                                                <span class="price-title"><?php echo $product['special']; ?></span>
                                                <?php } ?>
                                            </p>
                              <?php } ?>


                          </div>
                        </div>
                        <?php $i++; ?>
                        <?php } ?>
                      </div>
                      <?php } ?>


      <?php if ($products) { ?>
      <h4 style="border-bottom: 1px solid #cccccc;"><div id="rel_prod_text"><?php echo $text_related; ?></div></h4>
      <br>
      <div class="row">
        <?php $i = 0; ?>
        <?php foreach ($products as $product) { ?>

        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6" id="rel-prod">
          <div class="product-thumb transition description">
            <div class="image">
            <a href="<?php echo $product['href']; ?>">
            <?php $src2 = label($product['labels']);?> 
            <img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-responsive" />
            <img src="<?= $src2?>" alt="" class="stiker" style="margin-left: 15px;" >
            </a></div>
            <div class="caption">
              <a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a>
            </div>
                          <?php if ($product['price']) { ?>
<p class="price" style="margin: 0; height: 55px;">
                                <?php if (!$product['special']) { ?>
                                <?php echo '<span class="price-title" style="display: -webkit-inline-box; display: -moz-inline-box; margin-top: 7px;">'.$product['price'].'</span>'; ?>
                                <?php } else { ?>
                                <span class="price-title" style="font-size: 14px; text-decoration: line-through; display: -webkit-inline-box;  display: -moz-inline-box;"><?php echo $product['price']; ?></span>
                                <br>
                                <span class="price-title"><?php echo $product['special']; ?></span>
                                <?php } ?>
                            </p>
              <?php } ?>
          </div>
        </div>
        <?php $i++; ?>
        <?php } ?>
      </div>
      <?php } ?>
      <br>
           </div>
           <div id="cartModal" class="modal fade" tabindex="-1" role="dialog">
             <div class="modal-dialog modal-md" role="document">
               <div class="modal-content">
                 <div class="modal-header">
                   <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                   <h4 class="modal-title"><?php echo $text_product_added; ?></h4>
                 </div>
                 <div class="modal-footer">
                   <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo $text_continue; ?></button>
                   <a href="/checkout/" type="button" class="btn btn-primary"><?php echo $text_checkout; ?></a>
                 </div>
               </div><!-- /.modal-content -->
             </div><!-- /.modal-dialog -->
           </div><!-- /.modal -->
           <div id="testModal" class="modal fade" tabindex="-1" role="dialog">
             <div class="modal-dialog modal-lg" role="document">
               <img src="/catalog/view/theme/loudlemon/images/layout/icon_cross.svg" id="close-test-modal">
                 <div class="modal-content">
                   <div class="row">
                     <div class="hidden-xs hidden-sm col-md-6">
                       <img src="/catalog/view/theme/loudlemon/images/layout/test.jpg" alt="Записаться на тест" class="img-responsive">
                     </div>
                     <div class="col-xs-12 col-sm-12 col-md-6">
                         <h4 id="test-modal-title"><?php echo "Хотите попробовать перед покупкой?"; ?></h4>
                         <p>Просто сообщите через форму обратной связи, когда вам будет удобно это сделать, и как с вами связаться - и мы подготовим всё к вашему приходу!</p>
                         <div id="test-modal-form">
                           <div class="test-form-first-line">
                             <div>
                               <label for  = "phone" id="phone-label">Телефон *</label>
                               <input type = "text"  id="phone" name="phone" required="required">
                             </div>
                             <div>
                               <label for  = "email"  id="email-label">E-mail *</label>
                               <input type = "email"  id="email" name="email" required="required">
                             </div>
                             <div>
                               <label for  = "date"  id="date-label">Дата *</label>
                               <input type = "text"  id="date" class="datetime" name="date" required="required">
                               <img src="/catalog/view/theme/loudlemon/images/layout/calendar.svg" id="datetimepicker-img">
                             </div>
                           </div>
                           <div class="test-form-second-line">
                             <div>
                               <label for="name">Имя</label>
                               <input type="text" id="name" name="name">
                             </div>
                           </div>
                           <div class="test-form-third-line">
                             <div>
                               <label for="name">Комментарий</label>
                               <textarea id="comment" name="comment" rows="8"></textarea>
                             </div>
                           </div>
                           <div class="form-group" id="test-submit">
                             <button>
                                Записаться на тест
                             </button>
                           </div>
                         </div>
                     </div>
                   </div>
                 </div><!-- /.modal-content -->
             </div><!-- /.modal-dialog -->
           </div><!-- /.modal -->


           <script src="catalog/view/javascript/jquery.imagezoom.js"></script>
           <script>
           $('.zoom').zoom();
           </script>
        <script>
            $(window).ready(function () {


                $('img.thumb_click').click(function(){
                    $('#product_main_img').attr('src', $( this ).attr('data-big'));
                    $('.zoom').attr('data-zoom', '/image/' + $( this ).attr('data-big-ori'));
                    $('.jsZoomMouse').css({
                      'background-image': 'url(/image/' + $( this ).attr('data-big-ori')+')'
                    });
                });
                var dataCar = [];
                $('.thumb_click').each(function(){
                  dataCar[dataCar.length] = $(this).attr('data-big');
                });
                var countDataCar = dataCar.indexOf($('#product_main_img').attr('src'));
                countDataCar++;
                $('#product_main_img').click(function(e){
                    e.preventDefault();
                    if (dataCar.length == countDataCar) {
                      countDataCar = 0;
                    }
                    $('#product_main_img').attr('src', dataCar[countDataCar]);
                    countDataCar++;
                });
            })
        </script>
<script type="text/javascript"><!--
$('select[name=\'recurring_id\'], input[name="quantity"]').change(function(){
	$.ajax({
		url: 'index.php?route=product/product/getRecurringDescription',
		type: 'post',
		data: $('input[name=\'product_id\'], input[name=\'quantity\'], select[name=\'recurring_id\']'),
		dataType: 'json',
		beforeSend: function() {
			$('#recurring-description').html('');
		},
		success: function(json) {
			$('.alert, .text-danger').remove();

			if (json['success']) {
				$('#recurring-description').html(json['success']);
			}
		}
	});
});
//--></script>
<script type="text/javascript"><!--
$('#button-cart').on('click', function() {
	$.ajax({
		url: 'index.php?route=checkout/cart/add',
		type: 'post',
		data: $('#product input[type=\'text\'], #product input[type=\'hidden\'], #product input[type=\'radio\']:checked, #product input[type=\'checkbox\']:checked, #product select, #product textarea'),
		dataType: 'json',
		beforeSend: function() {
			$('#button-cart').button('loading');
		},
		complete: function() {
			$('#button-cart').button('reset');
		},
		success: function(json) {
			$('.alert, .text-danger').remove();
			$('.form-group').removeClass('has-error');

			if (json['error']) {
				if (json['error']['option']) {
					for (i in json['error']['option']) {
						var element = $('#input-option' + i.replace('_', '-'));

						if (element.parent().hasClass('input-group')) {
							element.parent().after('<div class="text-danger">' + json['error']['option'][i] + '</div>');
						} else {
							element.after('<div class="text-danger">' + json['error']['option'][i] + '</div>');
						}
					}
				}

				if (json['error']['recurring']) {
					$('select[name=\'recurring_id\']').after('<div class="text-danger">' + json['error']['recurring'] + '</div>');
				}

				// Highlight any found errors
				$('.text-danger').parent().addClass('has-error');
			}

			if (json['success']) {
				$('.breadcrumb').after('<div class="alert alert-success">' + json['success'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');

				$('.col-lg-6.col-md-6.col-sm-6.col-xs-6 #cart > button').html('<i class="fa fa-shopping-cart"></i> ' + json['total']);
        console.log(json['tablet_cart_total']);
        $('.tablet-cart-container #cart_link span').html(json['tablet_cart_total']);
        $('.mob-cart-container #cart_link span').html(json['tablet_cart_total']);

				// $('html, body').animate({ scrollTop: 0 }, 'slow');

				$('#cart > ul').load('index.php?route=common/cart/info ul li');

				$('#cartModal').modal('show');
			}
		},
        error: function(xhr, ajaxOptions, thrownError) {
            alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
	});
});
//--></script>

<script type="text/javascript"><!--

$('#close-test-modal').hover(
  function() {
    $( this ).attr( 'src', '/catalog/view/theme/loudlemon/images/layout/icon_cross_hover.svg' );
  }, function() {
    $( this ).attr( 'src', '/catalog/view/theme/loudlemon/images/layout/icon_cross.svg' );
  }
);

$('#close-test-modal').click(function(){
  $('#testModal').modal('hide');
});

$('#test-submit button').on('click', function() {
  $("#email").removeClass('error');
  $("#email-label").removeClass('error');
  $("#phone").removeClass('error');
  $("#phone-label").removeClass('error');
  $("#date").removeClass('error');
  $("#date-label").removeClass('error');
  var error = false;

  if($("#email").val().length === 0) {
     $("#email").addClass('error');
     $("#email-label").addClass('error');
     error = true;
  }
  if ($("#phone").val().length === 0) {
      $("#phone").addClass('error');
      $("#phone-label").addClass('error');
      error = true;
  }
  if ($("#date").val().length === 0) {
      $("#date").addClass('error');
      $("#date-label").addClass('error');
      error = true;
  }
  if(!error) {
    	$.ajax({
    		url: 'index.php?route=services/mail/send',
    		type: 'post',
    		data: $('#test-modal-form #name, #test-modal-form #phone, #test-modal-form #email, #test-modal-form #date, #test-modal-form #comment'),
    		dataType: 'json',
    		beforeSend: function() {
    			$('#button-test').button('loading');
    		},
    		complete: function() {
    			$('#button-test').button('reset');
    		},
    		success: function(json) {
        if(json['success'] == true){
          $('#testModal').modal('hide');
        } else {
          console.error("Произошла ошибка, смотри контроллер отправки e-mail.");
          // if (json['success']==false) {
          //            alert('Провал');
          // }else{
          //   alert('Произошла ошибка');
          // }
        }
    		},
            error: function(xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
    	});
    }
});
//--></script>

<script type="text/javascript"><!--
$('.date').datetimepicker({
	pickTime: false
});

$('.datetime').datetimepicker({
	pickDate: true,
	pickTime: true
});

$('.time').datetimepicker({
	pickDate: false
});

$('button[id^=\'button-upload\']').on('click', function() {
	var node = this;

	$('#form-upload').remove();

	$('body').prepend('<form enctype="multipart/form-data" id="form-upload" style="display: none;"><input type="file" name="file" /></form>');

	$('#form-upload input[name=\'file\']').trigger('click');

	if (typeof timer != 'undefined') {
    	clearInterval(timer);
	}

	timer = setInterval(function() {
		if ($('#form-upload input[name=\'file\']').val() != '') {
			clearInterval(timer);

			$.ajax({
				url: 'index.php?route=tool/upload',
				type: 'post',
				dataType: 'json',
				data: new FormData($('#form-upload')[0]),
				cache: false,
				contentType: false,
				processData: false,
				beforeSend: function() {
					$(node).button('loading');
				},
				complete: function() {
					$(node).button('reset');
				},
				success: function(json) {
					$('.text-danger').remove();

					if (json['error']) {
						$(node).parent().find('input').after('<div class="text-danger">' + json['error'] + '</div>');
					}

					if (json['success']) {
						alert(json['success']);

						$(node).parent().find('input').attr('value', json['code']);
					}
				},
				error: function(xhr, ajaxOptions, thrownError) {
					alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
				}
			});
		}
	}, 500);
});
//--></script>
<script type="text/javascript"><!--
$('#review').delegate('.pagination a', 'click', function(e) {
    e.preventDefault();

    $('#review').fadeOut('slow');

    $('#review').load(this.href);

    $('#review').fadeIn('slow');
});

$('#review').load('index.php?route=product/product/review&product_id=<?php echo $product_id; ?>');

$('#button-review').on('click', function() {
	$.ajax({
		url: 'index.php?route=product/product/write&product_id=<?php echo $product_id; ?>',
		type: 'post',
		dataType: 'json',
		data: $("#form-review").serialize(),
		beforeSend: function() {
			$('#button-review').button('loading');
		},
		complete: function() {
			$('#button-review').button('reset');
		},
		success: function(json) {
			$('.alert-success, .alert-danger').remove();

			if (json['error']) {
				$('#review').after('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + '</div>');
			}

			if (json['success']) {
				$('#review').after('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + '</div>');

				$('input[name=\'name\']').val('');
				$('textarea[name=\'text\']').val('');
				$('input[name=\'rating\']:checked').prop('checked', false);
			}
		}
	});
});

$(document).ready(function() {
	$('.thumbnails').magnificPopup({
		type:'image',
		delegate: 'a',
		gallery: {
			enabled:true
		}
	});
});
//--></script>
<script>
jQuery(document).ready(function($){
  var ress = function() {
    if ($(window).width() < 1400) {
      $('.prod-col-1').removeClass('col-lg-6');
      $('.prod-col-1').addClass('col-lg-5');
      $('.prod-col-2').removeClass('col-lg-6');
      $('.prod-col-2').addClass('col-lg-7');
    } else {
      $('.prod-col-1').removeClass('col-lg-5');
      $('.prod-col-1').addClass('col-lg-6');
      $('.prod-col-2').removeClass('col-lg-7');
      $('.prod-col-2').addClass('col-lg-6');
    }

    if ($(window).width() < 500) {
      $('.payments-row>div').removeClass('col-xs-2').addClass('col-xs-4');
      $('.payments-row').addClass('payments-row-min');
    } else {
      $('.payments-row>div').removeClass('col-xs-4').addClass('col-xs-2');
      $('.payments-row').removeClass('payments-row-min');
    }
  }
  ress();
  $(window).resize(function(){
    ress();
  });
});
</script>
<script>
$(document).ready(function() {
    $('#button-test').click(function(){
       $('#testModal').modal('show');
       $('#scroll').hide();
    });
});

$(function() {
    $('img, .jsZoom').bind('contextmenu', function() {
        return false;
    });
});
</script>
<?php echo $footer; ?>
