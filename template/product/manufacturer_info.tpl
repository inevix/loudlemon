<?php echo $header; //catalog/view/theme/loudlemon/template/product/
?>
<div class="container">
  <div class="row">
<?php
if (isset($_GET['order'])) {
    $order = $_GET['order'] == 'ASC' ? 'ASC' : 'DESC';
}
else {
    $order = 'ASC';
}
if (isset($_GET['filter'])) {
    $filter = $_GET['filter'] ? '&filter=' . $_GET['filter'] : '';
}
else {
    $filter = '';
}

if (isset($_GET['pmin'])) {
    $pminQ = $_GET['pmin'] ? '&pmin=' . $_GET['pmin'] : '';
}
else {
    $pminQ = '';
}

if (isset($_GET['pmax'])) {
    $pmaxQ = $_GET['pmax'] ? '&pmax=' . $_GET['pmax'] : '';
}
else {
    $pmaxQ = '';
}
?>
<div id="title-block">
    <div class="row">
        <div class="col-xs-12 col-sm-7">
            <ul class="breadcrumb">
                <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
                <?php } ?>
            </ul>
        </div>
        <div class="hidden-xs col-xs-12 col-sm-5 text-right sort-container" style="color: #666;">
           <?php $url = substr($_SERVER['REQUEST_URI'],0,strpos($_SERVER['REQUEST_URI'],'?')); ?>
            <?php echo $text_sort; ?>
            <?php foreach ($sorts as $sorts) { ?>
              <?php if ($sorts['value'] == $sort . '-' . $order) { ?>
                  <?php if ($sort=='pd.name'){ ?>
                    <?php if ($order=='ASC'){ ?>
                        <a href="<?php echo $name_desc; ?>" class="sort-link sort-active sort-ASC"><?php echo $text_sortName; ?></a>
                        <a href="<?php echo $price_asc; ?>" class="sort-link"><?php echo $text_sortPrice; ?></a>
                    <?php }else{ ?>
                        <a href="<?php echo $name_asc; ?>" class="sort-link sort-active sort-DESC"><?php echo $text_sortName; ?></a>
                        <a href="<?php echo $price_asc; ?>" class="sort-link"><?php echo $text_sortPrice; ?></a>
                    <?php } ?>
                  <?php }elseif($sort=='p.price'){ ?>
                    <?php if ($order=='ASC'){ ?>
                        <a href="<?php echo $name_asc; ?>" class="sort-link"><?php echo $text_sortName; ?></a>
                        <a href="<?php echo $price_desc; ?>" class="sort-link sort-active sort-ASC"><?php echo $text_sortPrice; ?></a>
                    <?php }else{ ?>
                        <a href="<?php echo $name_asc; ?>" class="sort-link"><?php echo $text_sortName; ?></a>
                        <a href="<?php echo $price_asc; ?>" class="sort-link sort-active sort-DESC"><?php echo $text_sortPrice; ?></a>
                    <?php } ?>
                  <?php }else{ ?>
                    <a href="<?php echo $name_asc; ?>" class="sort-link"><?php echo $text_sortName; ?></a>
                    <a href="<?php echo $price_asc; ?>" class="sort-link"><?php echo $text_sortPrice; ?></a>
                  <?php } ?>
               <?php } ?>
            <?php } ?>
        </div>
        
        <div class="fader hidden"></div>
    </div>
</div>
<div class="row mobile-filter-buttons visible-xs-block">
	<div class="mobile-filter-button col-xs-6">Фильтры</div>
	<div class="mobile-sort-button col-xs-6">Сортировка</div>
</div>
<div class="row">
    <?php
    $bootsrapClasses[0] = 'col-xs-12 col-sm-12 col-md-12 col-lg-12';


    $bootsrapClasses[0] = 'col-xs-12 col-sm-8 col-md-9 col-lg-9 col-xlg-10';
    $urlArray = explode('?', $_SERVER['REQUEST_URI']);

    if (strlen($column_left) < 30) {
        $bootsrapClasses[0] = 'col-xs-12 col-sm-12 col-md-12 col-lg-12';
    }
    else { ?>
        <div class="hidden-xs col-xs-12 col-sm-4 col-md-3 col-lg-3 col-xlg-2 filter-col sticky-filter">
            <?php echo $column_left; ?>
        </div>
        <?php } ?>
        
		<div id="content" class="<?php echo $bootsrapClasses[0]; ?>"> 
		<h1><?php echo $heading_title;?></h1>
		<?php if ($description && $description != '<p><br></p>') { ?>
			<div class="description">
				 <?php echo $description; ?>
			</div>
		<?php } ?>
      
       <?php echo $list ?>
        </div>

      <?php echo $content_bottom; ?></div>
    <?php echo $column_right; ?></div>
    
    
    <div class="row pagin-container">
		<?php echo $pagination; ?>
	</div>
    
</div>
<?php echo $footer; ?>
