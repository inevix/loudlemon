<?php if ($products) { ?>
<div class="row catalog-items">
    <?php foreach ($products as $product) { ?>
    <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
        <div id="product-in-cat">
            <a href="<?php echo $product['href']; ?>"><img src="<?php
            $urlArray = explode('?', $_SERVER['REQUEST_URI']);
            if($urlArray[0] == "/amps/"){
                echo $product['imgAmps'];
                }else{
                echo$product['thumb'];
                } ?>" class="img-responsive"></a>
            <div class="description" style="height: 41px;">
            <div class="productName">
                <a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></div>
            </div>
            <div style="width: 95%;">
               <?php if ($product['price']) { ?>
                <p class="price" style="margin: 0; height: 55px;">
                    <?php if (!$product['special']) { ?>
                    <?php echo '<span class="price-title" style="display: inline; margin-top: 7px;">'.$product['price'].'</span>'; ?>
                    <?php } else { ?>
                    <span class="price-title" style="font-size: 14px; text-decoration: line-through; display: -webkit-inline-box;  display: -moz-inline-box;"><?php echo $product['price']; ?></span>
                    <br>
                    <span class="price-title"><?php echo $product['special']; ?></span>
                    <?php } ?>
                </p>
                <?php } ?>
            </div>
        </div>
    </div>                
    <?php } ?>
</div>            
<?php } ?>
<?php if (!$categories && !$products) { ?>
<p><?php echo $text_empty; ?></p>
<?php } ?>