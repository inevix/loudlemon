        <?php
        if($_SERVER['REQUEST_URI'] == '/accessories/'){
          ?>
          <div class="container">
          	<style>
				#access div > a {
					color: #2b2a29;
					font-weight: bold;
					font-size: 160%;
				}
				#access div > a:hover {
					color: #a88d69;
					text-decoration: none;
				}
          	</style>
            <div class="row" id="access">
        	 <div class="col-md-3 col-sm-4 col-xs-12">
	                <a href="/straps/">
	                <img src="/catalog/view/theme/loudlemon/images/layout/accessories/accessories_straps.svg" alt="Ремни" class="img-responsive">
	                <div>Ремни</div>
	              </a>
            </div>
            
            <div class="col-md-3 col-sm-4 col-xs-12">
	                <a href="/strings/">
	                <img src="/catalog/view/theme/loudlemon/images/layout/accessories/accessories_strings.svg" alt="Струны" class="img-responsive">
	                <div>Струны</div>
	              </a>
            </div>
	          <div class="col-md-3 col-sm-4 col-xs-12">
		            <a href="/cable/">
		              <img src="/catalog/view/theme/loudlemon/images/layout/accessories/accessories_cable.svg" alt="Кабель" class="img-responsive">
		              <div>Кабели</div>
		            </a>
	          </div>
	          <div class="col-md-3 col-sm-4 col-xs-12">
	                <a href="/parts/">
	                <img src="/catalog/view/theme/loudlemon/images/layout/accessories/accessories_parts.svg" alt="Запчасти" class="img-responsive">
	                <div>Запчасти</div>
        			   </a>
        			   </div>
          <div class="col-md-3 col-sm-4 col-xs-12">
                  <a href="/apparel/">
                    <img src="/catalog/view/theme/loudlemon/images/layout/accessories/accessories_clothes.svg" alt="Одежда" class="img-responsive">
                    <div>Одежда</div>
                  </a>
              </div>
           <div class="col-md-3 col-sm-4 col-xs-12">
                <a href="/staff/">
                <img src="/catalog/view/theme/loudlemon/images/layout/accessories/accessories_stands.svg" alt="Стойки" class="img-responsive">
                <div>Стойки</div>
              </a>
              </div>
             <div class="col-md-3 col-sm-4 col-xs-12">
                <a href="/tuners/">
                <img src="/catalog/view/theme/loudlemon/images/layout/accessories/accessories_tuners.svg" alt="Тюнеры" class="img-responsive">
                <div>Тюнеры</div>
              </a>
              </div>
              <div class="col-md-3 col-sm-4 col-xs-12">
                <a href="/slides-capos/">
                <img src="/catalog/view/theme/loudlemon/images/layout/accessories/accessories_slides-capo.svg" alt="Слайды/Каподасты" class="img-responsive">
                <div>Слайды / Каподастры</div>
              </a>
              </div>
              <div class="col-md-3 col-sm-4 col-xs-12">
                <a href="/picks/">
                <img src="/catalog/view/theme/loudlemon/images/layout/accessories/accessories_picks.svg" alt="Медиаторы" class="img-responsive">
                <div>Медиаторы</div>
              </a>
              </div>
            <div class="col-md-3 col-sm-4 col-xs-12">
                <a href="/pickups/">
                <img src="/catalog/view/theme/loudlemon/images/layout/accessories/accessories_pickups.svg" alt="Звукосниматели" class="img-responsive">
                <div>Звукосниматели</div>
              </a>
              </div>
           <div class="col-md-3 col-sm-4 col-xs-12">
                <a href="/care-products/">
                  <img src="/catalog/view/theme/loudlemon/images/layout/accessories/accessories_care.svg" alt="Средства по уходу" class="img-responsive">
                  <div>Средства по уходу</div>
                </a>
              </div>
             <div class="col-md-3 col-sm-4 col-xs-12">
                <a href="/gifts/">
                  <img src="/catalog/view/theme/loudlemon/images/layout/accessories/accessories_gifts.svg" alt="Подарки" class="img-responsive">
                  <div>Подарки</div>
               </a>
              </div>
               <div class="col-md-3 col-sm-4 col-xs-12">
                <a href="/cases-gig-bags/">
                  <img src="/catalog/view/theme/loudlemon/images/layout/accessories/accessories_cases.svg" alt="Чехлы / кейсы" class="img-responsive">
                  <div>Чехлы / кейсы</div>
               </a>
              </div>
          </div>
          <script>
            var source = "";
            $('#access div > a').hover(function(){
             source = $( this ).children('img').attr('src');
               $( this ).children('img').attr('src', source.substr(0, source.length - 4 ) + '_hover.svg');
            }, function(){
               $( this ).children('img').attr('src', source);
            });
          </script>
          </div>
          <?php
        }else{
            if($_SERVER['REQUEST_URI'] == '/contacts'){
        ?>
<?php  if($curLang == 'ru'){$fax = $fax[0];} else {$fax = $fax[1];} ?>
        <?php } ?>
 <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 contact-col" itemscope itemtype="http://schema.org/LocalBusiness">
				<div class="hidden">
					<span itemprop="name"><?php echo $store; ?></span>
					<?php if ($logo) { ?>
						<img itemprop="image" src="<?php echo $logo; ?>" alt="<?php echo $store; ?>" />
					<?php } ?>
					<?php if ($streetAddress || $postalCode || $addressLocality) { ?>
						<span itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
							<?php if ($streetAddress) { ?>
								<span itemprop="streetAddress"><?php echo $streetAddress; ?></span>
							<?php } ?>
							<?php if ($postalCode) { ?>
								<span itemprop="postalCode"><?php echo $postalCode; ?></span>
							<?php } ?>
							<?php if ($addressLocality) { ?>
								<span itemprop="addressLocality"><?php echo $addressLocality; ?></span>
							<?php } ?>
						</span>
					<?php } ?>
					<?php if ($priceRange) { ?>
						<span itemprop="priceRange"><?php echo $priceRange; ?></span>	
					<?php } ?>
					<?php if ($openingHours) { ?>
						<time itemprop="openingHours" datetime="<?php echo $openingHours; ?>"></time>
					<?php } ?>
				</div>
                <ul id="shop_rek">
                    <li><?php  if($curLang == 'ru'){echo 'Часы работы: ' . htmlspecialchars_decode($open[0]);}else {echo 'Work time: '. htmlspecialchars_decode($open[1]);} ?></li>
                    <li style="padding-right: 20px"><a href="https://yandex.ru/maps/-/C6QbYIiu"><?php  if($curLang == 'ru'){echo htmlspecialchars_decode($address[0]);}else {echo htmlspecialchars_decode($address[1]);} ?></a></li>
                    <li><a href="mailto:<?php echo $email; ?>"><span itemprop="email"><?php echo $email; ?></span></a></li>
                    <?php /* <li><a href="tel:<?php echo str_replace(array("-", " "), "", $telephone); ?>"><span itemprop="telephone"><?php echo $telephone; ?></span></a></li> */ ?>
                    <li><span class="ya-phone" itemprop="telephone"><?php echo $telephone; ?></span></li>
                </ul>
<!--                 <div class="addition-map">
                  <img class="img-responsive" src="/catalog/view/theme/loudlemon/images/layout/map.svg" alt="" />
                </div>     -->
                <div class="row">

            </div>
        </div></div>
        <?php /*
<div class="map-container">
<div style="
    position: absolute;
    width: 100%;
    height: 50px;
    background-color: #fff;
"></div>
</div>
*/ ?>

                   <div class="container">
                   <div class="rekv">
                       ИП Амелин Дмитрий Дмитриевич<br />
                       119634, г. Москва, Лукинская улица 11, 405<br />
                       ИНН 772985992305<br />
                       р/c: 40802810802730000907<br />
                       АО "Альфа-Банк"<br />
                       БИК 044525593<br />
                       к/сч.: 30101810200000000593<br />
                       ОГРНИП: 314774629500080<br />
                   </div>

<script>
if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
  console.log('mob');
  $('.map-container>div').after('<iframe src="https://www.google.com/maps/d/embed?mid=1U7JNqJHNB7orxGPJAaCjvINdF8o" width="100%" height="800"></iframe>');
} else {
  $('.map-container>div').after('<iframe src="https://www.google.com/maps/d/embed?mid=1U7JNqJHNB7orxGPJAaCjvINdF8o" width="100%" height="800"></iframe>');
  console.log('desk');
}
	$('.map-container')
		.click(function(){
				$(this).find('iframe').addClass('clicked')})
		.mouseleave(function(){
				$(this).find('iframe').removeClass('clicked')});
	$(document).ready(function(){
		var shop_rek = $('#shop_rek').html();
		$('#shop_rek').remove();
		$('#contact-page').prepend('<ul id="shop_rek">'+shop_rek+'</ul>');
	});
</script>
<?php
}
?>
