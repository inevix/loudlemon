<div class="row featured">
    <?php foreach ($products as $product) { ?>
  <div class="col-xs-6 col-sm-3 col-md-3 featured-item" style="margin-bottom: 15px">
    <div style="background-color: #e6e6e6;">
          <div class="image">
            <a href="<?php echo $product['href']; ?>">
              <div class="discount-square">-<?php echo 100 - round ((str_replace(' ', '', $product['special']) / (int)str_replace(' ', '', $product['price']))*100); ?>%</div>
              <img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-responsive" />
            </a>
          </div>
          <div id="recom-block">
            <h4><a href="<?php echo $product['href']; ?>"><?php echo substr($product['name'], 0, 25).'...'; ?></a></h4>
            <?php if ($product['price']) { ?>
            <p class="price">
              <?php if (!$product['special']) { ?>
              <?php echo $product['price']; ?>
              <?php } else { ?>
              <span class="price-old"><?php echo $product['price']; ?></span>
              <span class="price-new"><?php echo $product['special']; ?></span>
              <?php } ?>
            </p>
            <?php } ?>
          </div>
    </div>
  </div>
    <?php } ?>
</div>


