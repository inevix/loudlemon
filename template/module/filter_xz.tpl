<div class='filter'>
     <?php 
     $i = 0;
     $bad_filters = array(6, 7, 8, 9, 11);
     foreach ($filter_groups as $filter_group) { 
         
         if (!in_array($filter_group['filter_group_id'], $bad_filters) && isset($filter_group['filter'])) { ?>
     <div group_id="<?php echo $filter_group['filter_group_id']; ?>" class="filter-title ft<?php echo $filter_group['filter_group_id']; ?>">
                           <a href="#" style="pointer-events: none;"><span class="glyphicon glyphicon-triangle-right"> </span> <?php echo $filter_group['name']; ?></a>
    </div>
    <ul class="filter-list fl<?php echo $filter_group['filter_group_id']; ?>">
        <?php         
        foreach ($filter_group['filter'] as $filter) { 
        $i++;
        ?>
          <label>
                <?php if (in_array($filter['filter_id'], $filter_category)) { ?>
                                <li> <input <?php echo $filter['disabled']; ?> class="filter-checkbox <?php echo $filter['disabled'] ? 'filter-disabled' : ''; ?>" id="ccb<?php echo $i; ?>" type="checkbox" name="filter[]" value="<?php echo $filter['filter_id']; ?>" checked="checked" /> 
                                <label for="ccb<?php echo $i; ?>">
                                <span class="input_title"><?php echo $filter['name']; ?></span></label></li>
                <?php } else { ?>
                <li><input <?php echo $filter['disabled']; ?> class="filter-checkbox <?php echo $filter['disabled'] ? 'filter-disabled' : ''; ?>" id="ccb<?php echo $i; ?>" type="checkbox" name="filter[]" value="<?php echo $filter['filter_id']; ?>" />
                <label for="ccb<?php echo $i; ?>">
                <span class="input_title"><?php echo $filter['name']; ?></label></span>
                </li>
                <?php } ?>
              </label><br>
            <?php } ?>
                            </ul>
        <?php } 
        }?>
    <div group_id="12" class="filter-title ft12">
                           <a href="#" style="pointer-events: none;"><span class="glyphicon glyphicon-triangle-right"></span> <?php echo $filter_price ?></a>
    </div>
<?php

  if(isset($_GET['pmin'])){
    $pmin = $_GET['pmin'];
  }else{
    $pmin = 0;
  }

  if(isset($_GET['pmax'])){
    $pmax = $_GET['pmax'];
  }else{
    $pmax = $get_pmax;
  }

?>
    <ul class="filter-list fl12">
                  <label style="color: #666;">
                        <li>                          
                         <?php echo $filter_from ?> <input name="pmin" id="pmin" value="<?php echo $pmin; ?>" type="text" size="5" style="    width: 69px;
    margin-left: 4px;
    margin-right: 5px;">
                         <?php echo $filter_to ?> <input name="pmax" id="pmax" value="<?php echo $pmax; ?>" type="text" size="5" style="
    width: 69px;
    margin-left: 4px;">
                        <?php echo $curlang == 'ru' ? 'руб.' : '$'; ?>
                        </li>
                      </label>
    <div id="slider-price"></div>
                      <br>
                                </ul>

                        
                    </div>

<script>
        $('.filter-title').css('cursor','pointer');
        $( document ).on('click touch', '.filter-title', function() {
          $( this ).next().toggle();
          if($( this ).next().css('display') == 'none'){
            $( this ).children().children().removeClass('glyphicon-triangle-bottom');
            $( this ).children().children().addClass('glyphicon-triangle-right');
          }else{
            $( this ).children().children().removeClass('glyphicon-triangle-right');
            $( this ).children().children().addClass('glyphicon-triangle-bottom');            
          }
          
        });

        var opened_titles = [];
        $('.filter-title').each(function(){
              if ($(this).hasClass('filter-title-opened')) {
                opened_titles[$(this).attr('group_id')] = true;
              } else {
                opened_titles[$(this).attr('group_id')] = false;
              }         
            });
        <?php if(isset($opened_titles)) { ?>
          var get_opened_titles = '<?php echo $opened_titles; ?>';
          get_opened_titles = get_opened_titles.split(',');          
          get_opened_titles.forEach(function(item, i, arr){
            if (item != "") {
              opened_titles[i] = item;
            }
          });
          opened_titles.forEach(function(item, i, arr){
            
            if (item == 'true') {
              $('.ft' + i).addClass('filter-title-opened');
              // $('.ft' + i).trigger('click');
              $('.ft' + i).trigger('touch');
            }
          });
          

        <?php } ?>

        $(document).on('click touch', '.filter-title', function(){
            if ($(this).hasClass('filter-title-opened')) {
                $(this).removeClass('filter-title-opened');               
            } else {
                $(this).addClass('filter-title-opened');               
            }
            opened_titles = [];
            $('.filter-title').each(function(){
              if ($(this).hasClass('filter-title-opened')) {
                opened_titles[$(this).attr('group_id')] = true;
              } else {
                opened_titles[$(this).attr('group_id')] = false;
              }         
            });
            // console.log(opened_titles);
        });
    
        // $("input[name='filter[]']:checked").parent().parent().parent().css('display', 'block');
        // $("input[name='filter[]']:checked").parent().parent().parent().prev().addClass('filter-title-opened');
        // $("input[name='filter[]']:checked").parent().parent().parent().prev().children().children().removeClass('glyphicon-triangle-right');
        // $("input[name='filter[]']:checked").parent().parent().parent().prev().children().children().addClass('glyphicon-triangle-bottom');
        // if( $("#pmin").val() > 0 || $("#pmax").val() < 200000 ){
        //   $('.fl12').css('display', 'block');
        //   $('.ft12').children().children().removeClass('glyphicon-triangle-right');
        //   $('.ft12').children().children().addClass('glyphicon-triangle-bottom');
        // }



    $("#slider-price").slider({
      min: 0,
      max: <?php echo $get_pmax; ?>,
      values: [<?PHP echo $pmin; ?>, <?PHP echo $pmax; ?>],
      range: true,
      stop: function(event, ui) {  
        jQuery("input#pmin").val(jQuery("#slider-price").slider("values",0));
        jQuery("input#pmax").val(jQuery("#slider-price").slider("values",1));
        refreshList();
        },
        slide: function(event, ui){
        jQuery("input#pmin").val(jQuery("#slider-price").slider("values",0));
        jQuery("input#pmax").val(jQuery("#slider-price").slider("values",1));
        }
    });
    var refreshList = function(page = false) {
      console.log('a');
        
        if ($('.sort-active').attr('href')) {
          var sort = $('.sort-active').attr('href');
        } else {
          var sort = 'sort=p.sort_order&order=ASC';
        }

        filter = [];
        var pmin = '';
        var pmax = '';
        $('input[name^=\'filter\']:checked').each(function(element) {
          filter.push(this.value);
        });

        if($('#pmin').val()){
            pmin = '&pmin=' + $('#pmin').val();    
        }

        if($('#pmax').val()){
            pmax = '&pmax=' + $('#pmax').val();    
        }
        pageUrl = '';
        if (page) {
          pageUrl = '&page=' + page;
        }

        var url = '<?php echo $action; ?>&filter=' + filter.join(',') + pmin + pmax + '&' + sort + '&opened_titles=' + opened_titles.join(',') + pageUrl;
        window.history.pushState(null, null, url);

          $.ajax({
              url: url + '&for_ajax=true',
              type: 'post',
              beforeSend: function() {
                  $('#content').html('Загрузка...');
                  $('.pagin-container').html('');
              },
              complete: function() {

              },
              success: function(json) {
                  $('#content').html(json['list']);
                  $('.pagin-container').html(json['pagination']);
                  $('.filter').parent().html(json['filter']);
              }
          });
    }
    
    
$('input[name^=\'filter\']').on('click touch', function() {
  refreshList();
});
var oldPmax = $('#pmax').val();
var oldPmin = $('#pmin').val();
// setInterval(function() { 
//             if ($('#pmax').val() != oldPmax || $('#pmin').val() != oldPmin) {
//                 oldPmax = $('#pmax').val();
//                 oldPmin = $('#pmin').val();
//                 refreshList();
//             }
//         },
//     1000);




// $("#pmin").on("change paste", function() {
//    refreshList();
// });
$("#pmax, #pmin").on("change paste keyup", function() {
   var pmaxTimer = setTimeout(function() {
      refreshList();
   }, 1000);
   $("#pmax, #pmin").on("change paste keyup", function() {
      clearTimeout(pmaxTimer);
   });
});


// $('body').on('click touch', '.pagination li a', function(e){
//   e.preventDefault();
//   var link = $(this).attr('href');  
//   if (link.indexOf("page=") == -1) {
//     refreshList();
//   }  else {
//     refreshList(link.substring(link.indexOf("page=")+5));    
//   }
// });

// $(document).ready(function(){
//   $('.pagination li a').on('click touch', function(e){
//     e.preventDefault();
//     var link = $(this).attr('href');  
//     if (link.indexOf("page=") == -1) {
//       refreshList();
//     }  else {
//       refreshList(link.substring(link.indexOf("page=")+5));    
//     }
//   });
// });

</script>

