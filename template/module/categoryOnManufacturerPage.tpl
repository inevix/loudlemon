<div class="categories-list box">
  <div class="box-heading"><?php echo $heading_title; ?></div>
  <div class="box-content">
	  <ul class="box-category">
		<?php foreach ($categories as $category) { ?>
		  <li><a href="<?php echo $category['href']; ?>"><?php if (isset($category['image'])) { ?><img src="<?php echo $category['image']; ?>" /><?php } ?> <?php echo $category['name']; ?></a></li>
		<?php } ?>
	  </ul>
  </div>
</div>
