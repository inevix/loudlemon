<?php 
$id = rand(0, 5000)*rand(0, 5000); 

if($position == 'footer_bottom' || $position == 'footer' || $position == 'footer_top' || $position == 'footer_left' || $position == 'footer_right' || $position == 'customfooter_top' || $position == 'customfooter_bottom' || $position == 'customfooter') {
	echo '<h4>'.$module['content']['title'].'</h4>';
	echo '<div class="strip-line"></div>';
	
	echo '<div class="clearfix" style="clear: both"><div class="advanced-grid-products">';
          foreach($module['content']['products'] as $product) {
               echo '<div class="product clearfix">';
                    echo '<div class="image"><a href="' . $product['href'] . '">';
                         if($theme_options->get( 'lazy_loading_images' ) != '0' || $config->get('speedup_image_lazyload') != 0) {
                         	echo '<img src="image/catalog/blank.gif" data-echo="' . $product['thumb'] . '" alt="' . $product['name'] . '" '. ($product['img_width'] ? 'style="max-width: '. $product['img_width'] .'px"' : '') .' />';
                         } else {
                         	echo '<img src="' . $product['thumb'] . '" alt="' .$product['name'] . '" />';
                         }
                    echo '</a></div>';
                    echo '<div class="right">';
                         echo '<div class="name"><a href="' . $product['href'] . '">' . $product['name'] . '</a></div>';
                         if ($product['rating'] && $theme_options->get( 'display_rating' ) != '0') { 
                          echo '<div class="rating">';
                            echo '<i class="fa fa-star' . ($product['rating'] >= 1 ? ' active' : '' ) .'"></i>';
                            echo '<i class="fa fa-star' . ($product['rating'] >= 2 ? ' active' : '' ) .'"></i>';
                            echo '<i class="fa fa-star' . ($product['rating'] >= 3 ? ' active' : '' ) .'"></i>';
                            echo '<i class="fa fa-star' . ($product['rating'] >= 4 ? ' active' : '' ) .'"></i>';
                            echo '<i class="fa fa-star' . ($product['rating'] >= 5 ? ' active' : '' ) .'"></i>';
                          echo '</div>';
                         }
                         echo '<div class="price">';
                         	if (!$product['special']) {
                         	     echo $product['price'];
                         	} else {
                         	     echo '<span class="price-old">' . $product['price'] . '</span> <span class="price-new">' . $product['special'] . '</span>';
                         	}
                         echo '</div>';
                    echo '</div>';
               echo '</div>';
          }
     echo '</div></div>';
} else {
	echo '<div class="box">';
    if ($module['content']['title']!=''){
  		echo '<div class="box-heading">';
  			echo $module['content']['title'];
  		echo '</div>';
    }
		echo '<div class="strip-line"></div>';
		echo '<div class="products">';
               echo '<div class="clearfix" style="clear: both"><div class="advanced-grid-products">'; ?>
                <div class="box-product">
                  <div id="myCarousel<?php echo $id; ?>" class="carousel slide">
                    <!-- Carousel items -->
                    <div class="carousel-inner">
                    <?php foreach($module['content']['products'] as $product) {
                         echo '<div class="box-content product clearfix">';
                              echo '<div class="image"><a href="' . $product['href'] . '">';
                                   if($theme_options->get( 'lazy_loading_images' ) != '0' || $config->get('speedup_image_lazyload') != 0) {
                                   	echo '<img src="image/catalog/blank.gif" data-echo="' . $product['thumb'] . '" alt="' . $product['name'] . '" />';
                                   } else {
                                   	echo '<img src="' . $product['thumb'] . '" alt="' .$product['name'] . '" />';
                                   }
                              echo '</a></div>';
                              echo '<div class="right">';
                                echo '<div class="name"><a href="' . $product['href'] . '">' . $product['name'] . '</a></div>'; ?>
                                <?php if($product['special']) { 
                                  $countdown = rand(0, 5000)*rand(0, 5000); 
                                  $product_detail = $theme_options->getDataProduct( $product['product_id'] );
                                  $date_end = $product_detail['date_end'];
                                  if($date_end != '0000-00-00' && $date_end) { ?>
                                      <script>
                                      $(function () {
                                        var austDay = new Date();
                                        austDay = new Date(<?php echo date("Y", strtotime($date_end)); ?>, <?php echo date("m", strtotime($date_end)); ?> - 1, <?php echo date("d", strtotime($date_end)); ?>);

                                        $('#countdown<?php echo $countdown; ?>').countdown({until: austDay});
                                      });
                                      </script>
                                      <div class="timeout"><?php echo $text_special_end; ?></div>
                                      <div id="countdown<?php echo $countdown; ?>" class="clearfix"></div>
                                 <?php }else{ ?>
                                   
                                    <?php   if ($product['rating'] && $theme_options->get( 'display_rating' ) != '0') { 
                                           echo '<div class="rating">';
                                               echo '<i class="fa fa-star' . ($product['rating'] >= 1 ? ' active' : '' ) .'"></i>';
                                               echo '<i class="fa fa-star' . ($product['rating'] >= 2 ? ' active' : '' ) .'"></i>';
                                               echo '<i class="fa fa-star' . ($product['rating'] >= 3 ? ' active' : '' ) .'"></i>';
                                               echo '<i class="fa fa-star' . ($product['rating'] >= 4 ? ' active' : '' ) .'"></i>';
                                               echo '<i class="fa fa-star' . ($product['rating'] >= 5 ? ' active' : '' ) .'"></i>';
                                           echo '</div>';
                                      }
                                     
                                     echo '<div class="price">';
                                      if (!$product['special']) {
                                           echo $product['price'];
                                      } else {
                                           echo '<div class="price-old">' .$text_old_price. $product['price'] . '</div> <div class="price-new">' .$text_special. $product['special'] . '</div>';
                                      }
                                      echo '</div>';
                                     echo '<a class="btn button" href="' . $product['href'] . '">'.$text_more.'</a>';
                                  }
                                }else{
                                    if ($product['rating'] && $theme_options->get( 'display_rating' ) != '0') { 
                                       echo '<div class="rating">';
                                           echo '<i class="fa fa-star' . ($product['rating'] >= 1 ? ' active' : '' ) .'"></i>';
                                           echo '<i class="fa fa-star' . ($product['rating'] >= 2 ? ' active' : '' ) .'"></i>';
                                           echo '<i class="fa fa-star' . ($product['rating'] >= 3 ? ' active' : '' ) .'"></i>';
                                           echo '<i class="fa fa-star' . ($product['rating'] >= 4 ? ' active' : '' ) .'"></i>';
                                           echo '<i class="fa fa-star' . ($product['rating'] >= 5 ? ' active' : '' ) .'"></i>';
                                       echo '</div>';
                                      }
                                     
                                     echo '<div class="price">';
                                      if (!$product['special']) {
                                           echo $product['price'];
                                      } else {
                                           echo '<div class="price-old">' .$text_old_price. $product['price'] . '</div> <div class="price-new">' .$text_special. $product['special'] . '</div>';
                                      }
                                      echo '</div>';
                                     echo '<a class="btn button" href="' . $product['href'] . '">'.$text_more.'</a>';
                                }
                              echo '</div>';
                         echo '</div>';
                    }
               echo '</div></div></div></div></div>';
		echo '</div>';
	echo '</div>';	
} ?>
<script type="text/javascript">
  $(document).ready(function() {
    var owl<?php echo $id; ?> = $(".box #myCarousel<?php echo $id; ?> .carousel-inner");
          
    owl<?php echo $id; ?>.owlCarousel({
      nav:false,
    navText: ['', ''],  
    dots: true,
    margin:0,
    responsiveClass:true,                    
    items: 1,
    responsive : {
      768 : {
          items : 3,
          dots: false,
          margin:16,
      }
    }
    });
  });
  </script>
