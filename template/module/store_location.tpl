
<div class="box store-locations">
	<?php if ($heading_title) { ?>
		<div class="box-heading"><span><?php echo $heading_title; ?></span></div>
	<?php } ?> 
	<?php if ($subtitle) { ?>
		<div class="box-subtitle subtitle"><?php echo $subtitle; ?></div>
	<?php } ?> 
		
	<div class="map">
		<div id="map" style="width: 100%; height: 450px;"></div>
		<div class="caption">
			<?php foreach ($locations as $location) { ?>
				<p class="phone-number"><?php echo $location['telephone']; ?></p>
				<p><?php echo $location['address']; ?> (<?php echo $location['open']; ?>)<br><?php echo $location['comment']; ?></p>
			<?php } ?> 
		</div>
	</div>
		
	<script src="https://maps.google.com/maps/api/js?language=ru&key=AIzaSyCKfCU3W2k9zl0XjfNNDGwVs1c_WHi3HdA"></script>
	<script type="text/javascript"> 
		<?php foreach ($locations as $location) { ?>
			<?php $geocodes = explode(',',$map_center); ?>
			$(document).ready(function() {
				var lat = {lat: <?php echo $geocodes[0]; ?>, lng: <?php echo $geocodes[1]; ?>}; 
				var point = '/image/catalog/design/icon-map.png';   
                var myOptions = {  
					zoom: <?php echo $zoom; ?>,
					center: lat,
					mapTypeId: google.maps.MapTypeId.ROADMAP,
					disableDefaultUI: true,
					zoomControl: true,
					scrollwheel: false,
					styles: [
						{elementType: 'geometry', stylers: [{color: '#303030'}]},
						{elementType: 'labels.text.stroke', stylers: [{color: '#1c1c1c'}]},
						{elementType: 'labels.text.fill', stylers: [{color: '#746855'}]},
						{
							featureType: 'administrative.locality',
							elementType: 'labels.text.fill',
							stylers: [{color: '#6b6b6b'}]
						},
						{
							featureType: 'poi',
							elementType: 'labels.text.fill',
							stylers: [{color: '#6b6b6b'}]
						},
						{
							featureType: 'poi.park',
							elementType: 'geometry',
							stylers: [{color: '#2f2f2f'}]
						},
						{
							featureType: 'poi.park',
							elementType: 'labels.text.fill',
							stylers: [{color: '#6b6b6b'}]
						},
						{
							featureType: 'road',
							elementType: 'geometry',
							stylers: [{color: '#252525'}]
						},
						{
							featureType: 'road',
							elementType: 'geometry.stroke',
							stylers: [{color: '#252525'}]
						},
						{
							featureType: 'road',
							elementType: 'labels.text.fill',
							stylers: [{color: '#6b6b6b'}]
						},
						{
							featureType: 'road.highway',
							elementType: 'geometry',
							stylers: [{color: '#252525'}]
						},
						{
							featureType: 'road.highway',
							elementType: 'geometry.stroke',
							stylers: [{color: '#252525'}]
						},
						{
							featureType: 'road.highway',
							elementType: 'labels.text.fill',
							stylers: [{color: '#6b6b6b'}]
						},
						{
							featureType: 'transit',
							elementType: 'geometry',
							stylers: [{color: '#2f3948'}]
						},
						{
							featureType: 'transit.station',
							elementType: 'labels.text.fill',
							stylers: [{color: '#6b6b6b'}]
						},
						{
							featureType: 'water',
							elementType: 'geometry',
							stylers: [{color: '#17263c'}]
						},
						{
							featureType: 'water',
							elementType: 'labels.text.fill',
							stylers: [{color: '#6b6b6b'}]
						},
						{
							featureType: 'water',
							elementType: 'labels.text.stroke',
							stylers: [{color: '#17263c'}]
						}
					]
				}; 
				
				var map = new google.maps.Map(document.getElementById("map"), myOptions);   
                var marker = new google.maps.Marker({
					map: map,
					icon: point,
					// Define the place with a location, and a query string.
					place: {
						location: lat,
						query: 'subway_station'
					},
					// Attributions help users find your site again.
					attribution: {
						source: 'Google Maps JavaScript API',
						webUrl: 'https://developers.google.com/maps/'
					}
				});
			});   
		<?php } ?>    
	</script>   
</div>



