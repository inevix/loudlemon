<section class="view-guitars">
    <div class="view-text">
        <div class="flex-box flex-center box-title">
            <div class="box-title__lines box-title__lines--left wow fadeInLeft" data-wow-delay=".2s" data-wow-duration="1s"></div>
            <h1 class="wow fadeInUp" data-wow-delay=".3s" data-wow-duration="1s">
                <?php echo $setting_module['name']; ?>
            </h1>
            <div class="box-title__lines box-title__lines--right wow fadeInRight" data-wow-delay=".2s" data-wow-duration="1s"></div>
        </div>
    </div>
    <?php if ($products) { ?>
    <div class="fb-container">

        <?php if ($products) { //catalog/view/theme/loudlemon/template/module/ ?>

            <ul class="flex-box flex-wrap flex-start flex-align-stretch products-list">
                <?php $limit = 1; foreach ($products as $product) { ?>

                    <?php if ($limit > 8) break; ?>

                    <li class="flex-col-4 products-list__item">
                        <a href="<?php echo $product['href']; ?>" class="products-list__item__img">
                            <img src="<?php
                            if ($product['thumb'] !== null)
                                echo $product['thumb'];
                            else
                                echo '/catalog/view/theme/loudlemon/images/homepage/guitars/7.jpg';
                            ?>" alt="<?php echo $product['name']; ?>" />
                        </a>
                        <a href="#" class="products-list__item__brand">
                            <?php echo $product['manufacturer']; ?>
                        </a>
                        <a href="<?php echo $product['href']; ?>" class="products-list__item__title">
                            <?php echo $product['name']; ?>
                        </a>
                        <div class="products-list__item__cost">
                            <?php if ($product['special'] !== null) { ?>
                                <div class="products-list__item__cost__old">
                                    <?php echo $product['special'] ?>
                                </div>
                            <?php } ?>
                            <?php echo $product['price']; ?>
                        </div>
                    </li>

                    <?php $limit++; } ?>
            </ul>

        <?php } ?>
        <?php if (!$categories && !$products) { ?>
            <p><?php echo $text_empty; ?></p>
        <?php } ?>

        <?php// echo $list //ajax_filter_list.tpl ?>
        <!--<ul class="flex-box flex-wrap flex-start flex-align-stretch products-list">
            <li class="flex-col-4 products-list__item">
                <a href="#" class="products-list__item__img">
                    <img src="/catalog/view/theme/loudlemon/images/homepage/guitars/7.jpg" alt="Guitar preview"/>
                </a>
                <a href="#" class="products-list__item__brand">
                    Fender
                </a>
                <a href="#" class="products-list__item__title">
                    American Pro Jazzmaster Mystic Seafoam Green
                </a>
                <div class="products-list__item__cost">
                    99 800 руб.
                </div>
            </li>
            <li class="flex-col-4 products-list__item">
                <a href="#" class="products-list__item__img">
                    <img src="/catalog/view/theme/loudlemon/images/homepage/guitars/1.jpg" alt="Guitar preview"/>
                </a>
                <a href="#" class="products-list__item__brand">
                    GIBSON
                </a>
                <a href="#" class="products-list__item__title">
                    Explorer 2005 Ebony
                </a>
                <div class="products-list__item__cost">
                    55 000 руб.
                </div>
            </li>
        </ul>-->
    </div>
    <?php } ?>
</section>


