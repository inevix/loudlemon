<div class="banner-module">
	<div id="banner<?php echo $module; ?>"<?php if ($carousel) { ?> class="owl-carousel"<?php } ?>>
	  <?php foreach ($banners as $banner) { ?>
	  <div class="item">
		<?php if ($banner['link']) { ?>
		<a href="<?php echo $banner['link']; ?>"><img src="<?php echo $banner['image']; ?>" alt="<?php echo $banner['title']; ?>" class="img-responsive" /></a>
		<?php } else { ?>
		<img src="<?php echo $banner['image']; ?>" alt="<?php echo $banner['title']; ?>" class="img-responsive" />
		<?php } ?>
	  </div>
	  <?php } ?>
	</div>
	<?php if ($carousel) { ?>
		<div class="slider-nav">
			<div class="container">
				
			</div>
		</div>
		<script>
			$(document).ready(function() {
				$('#banner<?php echo $module; ?>').owlCarousel({
					loop: true,
					center:true,
					autoPlay: <?php echo $autoplay_timeout; ?>,
					nav: <?php echo $arrows; ?>,
					navText: ['',''],
					navContainer: '.slider-nav .container',	
					dots: <?php echo $dots; ?>,
					responsive : {
						0 : {
							items: 1,
						},
						480 : {
							items: 2,
						},
						768 : {
							items: <?php echo $number_per_row - 1; ?>,
						},
						1024 : {
							items: <?php echo $number_per_row; ?>,
						}
					}
					
				});
			});
		</script>
	<?php } ?>
</div>
