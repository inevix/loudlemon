<div class='filter'>
     <?php 
     $i = 0;
     $bad_filters = array(6, 7, 8, 9, 11);
     foreach ($filter_groups as $filter_group) { 
         
         if (!in_array($filter_group['filter_group_id'], $bad_filters) && isset($filter_group['filter'])) { ?>
     <div group_id="<?php echo $filter_group['filter_group_id']; ?>" class="filter-title ft<?php echo $filter_group['filter_group_id']; ?>">
                           <a href="#" style="pointer-events: none;"><span class="glyphicon glyphicon-triangle-bottom"> </span> <?php echo $filter_group['name']; ?></a>
    </div>
    <ul class="filter-list fl<?php echo $filter_group['filter_group_id']; ?>">
        <?php         
        foreach ($filter_group['filter'] as $filter) { 
        $i++;
        ?>
          <label>
                <?php if (in_array($filter['filter_id'], $filter_category)) { ?>
                                <li> <input <?php echo $filter['disabled']; ?> class="filter-checkbox <?php echo $filter['disabled'] ? 'filter-disabled' : ''; ?>" id="ccb<?php echo $i; ?>" type="checkbox" name="filter[]" value="<?php echo $filter['filter_id']; ?>" checked="checked" /> 
                                <label for="ccb<?php echo $i; ?>">
                                <span class="input_title"><?php echo $filter['name']; ?></span></label></li>
                <?php } else { ?>
                <li><input <?php echo $filter['disabled']; ?> class="filter-checkbox <?php echo $filter['disabled'] ? 'filter-disabled' : ''; ?>" id="ccb<?php echo $i; ?>" type="checkbox" name="filter[]" value="<?php echo $filter['filter_id']; ?>" />
                <label for="ccb<?php echo $i; ?>">
                <span class="input_title"><?php echo $filter['name']; ?></label></span>
                </li>
                <?php } ?>
              </label><br>
            <?php } ?>
                            </ul>
        <?php } 
        }?>
    <div group_id="12" class="filter-title ft12">
      <a href="#" style="pointer-events: none;"><span class="glyphicon glyphicon-triangle-bottom"></span> <?php echo $filter_price ?></a>
    </div>
<?php

  if(isset($_GET['pmin'])){
    $pmin = $_GET['pmin'];
  }else{
    $pmin = 0;
  }
  
  //overwrite controller data
  $get_pmax = 500000;

  if(isset($_GET['pmax'])){
    if ($_GET['pmax'] > $get_pmax) {
      $pmax = $get_pmax;
    } else {
      $pmax = $_GET['pmax'];
    }    
  }else{
    $pmax = $get_pmax;
  }

?>
    <ul class="filter-list fl12">
                  <label style="color: #666;">
                        <li>                          
                         <?php echo $filter_from ?> <input name="pmin" id="pmin" value="<?php echo $pmin; ?>" type="text" size="5" style="    width: 69px;
    margin-left: 4px;
    margin-right: 5px;">
                         <?php echo $filter_to ?> <input name="pmax" id="pmax" value="<?php echo $pmax+1; ?>" type="text" size="5" style="
    width: 69px;
    margin-left: 4px;">
                        <?php echo $curlang == 'ru' ? 'руб.' : '$'; ?>
                        </li>
                      </label>
    <div id="slider-price"></div>
                      <br>
                                </ul>


                    </div>

                        




<script>
		

		
        
        $( ".filter-title" ).on('click touch', function() {
          $( this ).next().toggle();
          if($( this ).next().css('display') == 'none'){
            $( this ).children().children().removeClass('glyphicon-triangle-top');
            $( this ).children().children().addClass('glyphicon-triangle-bottom');
          }else{
            $( this ).children().children().removeClass('glyphicon-triangle-bottom');
            $( this ).children().children().addClass('glyphicon-triangle-top');            
          }
          
        });

        var opened_titles = [];
        $('.filter-title').each(function(){
              if ($(this).hasClass('filter-title-opened')) {
                opened_titles[$(this).attr('group_id')] = true;
              } else {
                opened_titles[$(this).attr('group_id')] = false;
              }         
            });
        <?php if(isset($opened_titles)) { ?>
          var get_opened_titles = '<?php echo $opened_titles; ?>';
          get_opened_titles = get_opened_titles.split(',');          
          get_opened_titles.forEach(function(item, i, arr){
            if (item != "") {
              opened_titles[i] = item;
            }
          });
          opened_titles.forEach(function(item, i, arr){
            
            if (item == 'true') {
              $('.ft' + i).addClass('filter-title-opened');
              if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
                $('.ft' + i).trigger('touch');
              } else {
                $('.ft' + i).trigger('click');
              }
            }
          });
          

        <?php } ?>

        $('.filter-title').on('click touch', function(){
            if ($(this).hasClass('filter-title-opened')) {
                $(this).removeClass('filter-title-opened');               
            } else {
                $(this).addClass('filter-title-opened');               
            }
            opened_titles = [];
            $('.filter-title').each(function(){
              if ($(this).hasClass('filter-title-opened')) {
                opened_titles[$(this).attr('group_id')] = true;
              } else {
                opened_titles[$(this).attr('group_id')] = false;
              }         
            });
        });



    $("#slider-price").slider({
      min: 0,
      max: <?php echo $get_pmax; ?>,
      values: [<?PHP echo $pmin; ?>, <?PHP echo $pmax; ?>],
      range: true,
      stop: function(event, ui) {  
        jQuery("input#pmin").val(jQuery("#slider-price").slider("values",0));
        jQuery("input#pmax").val(jQuery("#slider-price").slider("values",1)+1);
        $('#pmin').unmask();
        $('#pmax').unmask();
        $('#pmin').mask("### ### ###", {reverse: true});
        $('#pmax').mask("### ### ###", {reverse: true});
        refreshList();
        },
        slide: function(event, ui){
        jQuery("input#pmin").val(jQuery("#slider-price").slider("values",0));
        jQuery("input#pmax").val(jQuery("#slider-price").slider("values",1)+1);
        $('#pmin').unmask();
        $('#pmax').unmask();
        $('#pmin').mask("### ### ###", {reverse: true});
        $('#pmax').mask("### ### ###", {reverse: true});
        }
    });
    $('#slider-price').draggable();

    var refreshList = function(page) {
      if ($('.sort-active').attr('href')) {
        var sort = $('.sort-active').attr('href');
      } else {
        var sort = 'sort=p.sort_order&order=ASC';
      }

      filter = [];
      var pmin = '';
      var pmax = '';
      $('input[name^=\'filter\']:checked').each(function(element) {
        filter.push(this.value);
      });

      if($('#pmin').val()){
          pmin = '&pmin=' + $('#pmin').cleanVal();    
      }

      if($('#pmax').val()){
          pmax = '&pmax=' + $('#pmax').cleanVal();    
      }
      var priceString = $('#pmax').val() >= <?php echo $get_pmax ?> ? '' : pmin + pmax;
      pageUrl = '';
      if (page) {
        pageUrl = '&page=' + page;
      }

      var url = '<?php echo $action; ?>&filter=' + filter.join(',') + priceString + '&' + sort + '&opened_titles=' + opened_titles.join(',') + pageUrl;
      window.history.pushState(null, null, url);
      var curContentHeight = $(window).height();
        $.ajax({
            url: url + '&for_ajax=true',
            type: 'post',
            beforeSend: function() {                  
                $('#content').html('<div style="height: '+curContentHeight+'px;" class="list-loading"><div class="sk-fading-circle"><div class="sk-circle1 sk-circle"></div><div class="sk-circle2 sk-circle"></div><div class="sk-circle3 sk-circle"></div><div class="sk-circle4 sk-circle"></div><div class="sk-circle5 sk-circle"></div><div class="sk-circle6 sk-circle"></div><div class="sk-circle7 sk-circle"></div><div class="sk-circle8 sk-circle"></div><div class="sk-circle9 sk-circle"></div><div class="sk-circle10 sk-circle"></div><div class="sk-circle11 sk-circle"></div><div class="sk-circle12 sk-circle"></div></div></p>');
                $('.pagin-container').html('');
                $('html, body').scrollTop($("#content").offset().top-175);
            },
            complete: function() {

            },
            success: function(json) {
                $('#content').html(json['list']);
                $('.pagin-container').html(json['pagination']);
                $('.filter').parent().html(json['filter']);
                $('#pmin').unmask();
                $('#pmax').unmask();
                $('#pmin').mask("### ### ###", {reverse: true});
                $('#pmax').mask("### ### ###", {reverse: true});
            }
        });
    }

    
$('input[name^=\'filter\']').on('click go', function() {  
  var refreshTimer = setTimeout(function() {
    refreshList();
  }, 1000);
  $('input[name^=\'filter\']').on('click', function() {  
    clearTimeout(refreshTimer);
  });
});

// $(window).on('load', function() {
// 	$('input[name^=\'filter\']').trigger("go");
// });

var oldPmax = $('#pmax').val();
var oldPmin = $('#pmin').val();

$("#pmax, #pmin").on("change paste keyup", function() {
   var pmaxTimer = setTimeout(function() {
      refreshList();
   }, 1000);
   $("#pmax, #pmin").on("change paste keyup", function() {
      clearTimeout(pmaxTimer);
   });
});


</script>

