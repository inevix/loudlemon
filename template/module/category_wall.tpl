<div class="box">
	<?php if($block_heading!=''){ ?>
		<div class="box-heading"><span><?php echo $block_heading; ?></span></div>
	<?php } ?>
	<div class="strip-line"></div>
	<div class="category-wall">		
		<?php  $class = 3; 
		if($category_number == 6) $class = 4;
		if($category_number == 5) $class = 25;
		if($category_number == 3) $class = 4;
		if($category_number == 2) $class = 6;
		
		$i = 0;
		echo '<div class="row">';
			foreach($categories as $category) {
				if($i%$category_number == 0 && $i != 0) echo '</div><div class="row">';
				echo '<div class="col-md-' . $class . ' col-sm-6 col-xs-6"><div class="box-category-wall">';
					echo '<a href="' . $category['href'] . '">';
						echo '<div class="common-info">';
						if($category['image']) {
							echo '<div class="image">';
								echo '<img src="' . $category['image'] .'" alt="' . $category['name'] . '">';
							echo '</div>';
						} else {
	     					echo '<ul><li><a href="' . $category['href'] . '">' . $category['name'] . '</a>';
						}
						echo '<div class="name">' . $category['name'] . '</div>';

						if(count($category['children']) > 0) {
							echo '<div class="categories">';
	     					echo '<ul>';
	     					foreach($category['children'] as $child) {
	     						echo '<li><a href="' . $child['href'] . '">' . $child['name'] . '</a></li>';
	     					}
	     					echo '</ul>';		
	     					echo '<a href="' . $category['href'] . '" class="button">' . $more_text . '</a>';			
							echo '</div>';
						}
						echo '</li></ul>';	
						echo '</div>';
					echo '</a>';	
					echo '<div class="typo-actions clearfix">';
					echo '<a href="' . $category['href'] . '" class="btn button">' . $more_text . '</a>';
					echo '</div>';
				echo '</div></div>';
				$i++;
			}
		echo '</div>'; ?>
	</div>
</div>
