<?php if ($products) { //catalog/view/theme/loudlemon/template/module/
    ?>
    
        <?php
    $bootsrapClasses[1] = 'col-xs-6 col-sm-4 col-md-3 col-lg-3 clear-xlg-3';
    $bootsrapClasses[0] = 'clear-xs-6 clear-sm-4 clear-md-3 clear-lg-3 clear-xlg-3';

    if ($category !== 65) { //лучшие (предложения)
        $urlArray = explode('?', $_SERVER['REQUEST_URI']);

        if ($urlArray[0] == "/amps/") {
            $bootsrapClasses[1] = 'col-xs-6 col-sm-6 col-md-6 col-lg-6 col-xlg-4';
            $bootsrapClasses[0] = 'clear-xs-6 clear-sm-6 clear-md-6 clear-lg-6 clear-xlg-4';
        }
        else {
            $bootsrapClasses[1] = 'col-xs-6 col-sm-6 col-md-4 col-lg-4 col-xlg-3';
            $bootsrapClasses[0] = 'clear-xs-6 clear-sm-6 clear-md-4 clear-lg-4 clear-xlg-3';
        }
        //var_dump($data);
        //die();
        if($module=="featured" || $module=="product_accessories" || $module=="product_details") {
        	$bootsrapClasses[1] = 'col-xs-6 col-sm-3 col-md-3 col-lg-3 col-xlg-3';
            $bootsrapClasses[0] = 'clear-xs-6 clear-sm-3 clear-md-3 clear-lg-3 clear-xlg-3';
        }
        else {
        	// var_dump($data);
        	// die();
        	$module=="default-module";
        }
        
        
		?>
    <?php } ?>
    
<div class="row catalog-items <?=$module?> <?=$bootsrapClasses[0]?>">
    <?php foreach ($products as $product) { 
    // var_dump($product);die();
    ?>
    
    <div class="<?=$bootsrapClasses[1]?> catalog-item">
        <div id="product-in-cat">
            <a href="<?php echo $product['href']; ?>"><img src="<?php
            $urlArray = explode('?', $_SERVER['REQUEST_URI']);
            if($urlArray[0] == "/amps/"){
                echo $product['imgAmps'];
                }else{
                echo $product['thumb'];
                } ?>" alt="<?php echo (isset($product['alt']) ? $product['alt'] : $product['name'] ); ?>" class="img-responsive"></a>
            <div class="description">
	            <div class="manufacturer" data="<?=$product['manufacturer']; ?>"><?=$product['manufacturer']; ?></div>
	            <div class="productName">
	                <a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a>
	            </div>
            	<div style="width: 95%;">
               <?php if ($product['price']) { ?>
                <p class="price" style="margin: 0; height: 55px;">
                    <?php if (!$product['special']) { ?>
                    <?php echo '<span class="price-title">'.$product['price'].'</span>'; ?>
                    <?php } else { ?>
                    <span class="price-title price-old-title"><?php echo $product['price']; ?></span>
                    <br>
                    <span class="price-title"><?php echo $product['special']; ?></span>
                    <?php } ?>
                </p>
                <?php } ?>
        		</div>
        	</div>
            
                        <?php
                        $label = NULL;
                        $src = "";
                        foreach ($product as $key => $values)
                        {

                            if ($key === 'label')
                            {

                                foreach ($values as $value => $mean)
                                {
                                    if ($mean)
                                    $label = $mean;
                                }
                            }
                        }
                        // die($label); 
                        if ($label)
                        {
                            $src = "/catalog/view/theme/loudlemon/images/layout/stikers/sticker_" . $label . "_preview.svg";
                        }
                        
                        ?>
                        <img src="<?= $src?>" alt="" class="stiker">
                        
        </div>
    </div>                
    <?php } ?>
</div>            
<?php } ?>
<?php if (!$categories && !$products) { ?>
<p><?php echo $text_empty; ?></p>
<?php } ?>
