<?php echo $header; ?>
<div class="container" id="not-found">
  <div id="not-found-images">
  	<img src="/catalog/view/theme/loudlemon/images/layout/404_lemon.svg"  class="hidden-sm"  alt="Страница не найдена">
  	<img src="/catalog/view/theme/loudlemon/images/layout/404_letters.svg" alt="Страница не найдена">
  </div>
  <div id="not-found-text">
  	Страница не найдена. Похоже, вы попали куда-то не туда.
  </div>
  <div id="not-found-link">
  	<a href="/">
  		Продолжить
  	</a>
  </div>
</div>
<?php echo $footer; ?>