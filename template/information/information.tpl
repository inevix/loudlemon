<?php echo $header; ?>
<div class="container breadcrumbs-container inf-breadcrumbs">
    <ul class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
    <?php } ?>
    </ul>
</div>
<?php if ($_SERVER['REQUEST_URI'] == '/repair'){ ?>
<div style="text-align: center; margin-top: 50px; padding-bottom: 105px;">
    <span class="underConstruction">PAGE UNDER CONSTRUCTION</span>
</div>
<?php
}else {
?>
<?php echo $content_top;
?>

<div class="container main">
    <h1><?php echo $heading_title;?></h1>
    <div id="contact-page">
        <?php echo $description; ?>
    </div>
    <?php echo $content_bottom; ?>
</div>
<?php } ?>
<script type="text/javascript">
    $('#contact-page img').addClass('img-responsive');
    var deliveryImg = $('#deliveryImg').html();
    $('#deliveryImg').remove();
    $('header').after(deliveryImg);

</script>
<?php echo $footer; ?>

