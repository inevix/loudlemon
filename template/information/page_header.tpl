<?php 
$config = $this->registry->get('config');
require_once( DIR_TEMPLATE.$config->get('config_template')."/lib/module.php" );
$modules = new Modules($this->registry);
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!--<meta name="viewport" content="width=device-width, initial-scale=1">-->
        <meta name="viewport" content="initial-scale=1, maximum-scale=1, user-scalable=no">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title><?php echo $title; if (isset($_GET['page'])) { echo " - ". ((int) $_GET['page'])." ".$text_page;} ?></title>
        <base href="<?php echo $base; ?>" />
        <?php if ($description) { ?>
        <meta name="description" content="<?php echo $description; if (isset($_GET['page'])) { echo " - ". ((int) $_GET['page'])." ".$text_page;} ?>" />
        <?php } ?>
        <?php if ($keywords) { ?>
        <meta name="keywords" content= "<?php echo $keywords; ?>" />
        <?php } ?>
        
        <?php foreach ($links as $link) { ?>
			<link href="<?php echo $link['href']; ?>" rel="<?php echo $link['rel']; ?>" />
		<?php } ?>
        
        <meta property="og:title" content="<?php echo $title; if (isset($_GET['page'])) { echo " - ". ((int) $_GET['page'])." ".$text_page;} ?>" />
        <meta property="og:type" content="website" />
        <meta property="og:url" content="<?php echo $og_url; ?>" />
        <meta property="og:site_name" content="<?php echo $name; ?>" />
        <!-- Bootstrap -->
        <link rel="stylesheet" href="/catalog/view/theme/loudlemon/stylesheet/bootstrap.min.css" />
        <link rel="stylesheet" href="/catalog/view/theme/loudlemon/stylesheet/font-awesome.min.css" />
        <link rel="stylesheet" href="/catalog/view/theme/loudlemon/stylesheet/owl.carousel.min.css" />
        <link rel="stylesheet" href="/catalog/view/theme/loudlemon/stylesheet/hamburgers.css" />
        <link rel="stylesheet" href="/catalog/view/theme/loudlemon/stylesheet/menu.css" />
        <link rel="stylesheet" href="/catalog/view/theme/loudlemon/stylesheet/megamenu.css" />
        <link rel="stylesheet" href="/catalog/view/theme/loudlemon/stylesheet/loudlemon.css" />
        <link rel="stylesheet" href="/catalog/view/theme/loudlemon/stylesheet/new.css" />
        <link rel="stylesheet" href="/catalog/view/javascript/jquery/swiper/css/swiper.css" />
        <link rel="stylesheet" href="/catalog/view/theme/loudlemon/stylesheet/filter_product.css" />
        <link rel="stylesheet" href="/catalog/view/theme/loudlemon/stylesheet/page.css" />
          
        <?php foreach ($styles as $style) { ?>
        <link href="/<?php echo $style['href']; ?>" type="text/css" rel="<?php echo $style['rel']; ?>" media="<?php echo $style['media']; ?>" />
        <?php } ?>
        <?php foreach ($analytics as $analytic) { ?>
        <?php echo $analytic; ?>
        <?php } ?>

		

        <link rel="stylesheet" type="text/css" href="/catalog/view/theme/loudlemon/stylesheet/thumbelina.css" />
        <style type="text/css">
            <?php if($curLang == 'en') {echo "ul.nav {padding: 0 11px;} ul.nav:last-child {padding-right: 22px; padding-left: 21px;}"; } ?>
		</style>
		
        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="/catalog/view/theme/loudlemon/js/jquery-2.2.1.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="//code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
        <script src="/catalog/view/javascript/jquery.ui.touch-punch.min.js"></script>
        <script src="/catalog/view/theme/loudlemon/js/owl.carousel.min.js"></script>
        <script src="/catalog/view/theme/loudlemon/js/bootstrap.min.js"></script>
        <script src="/catalog/view/javascript/jquery/swiper/js/swiper.min.js"></script>
        
       
        <script type="text/javascript">
			var responsive_design = 'yes';
		</script>

        <script src="/catalog/view/theme/loudlemon/js/thumbelina.js"></script>
        <?php foreach ($scripts as $script) { ?>
        <script src="/<?php echo $script; ?>" type="text/javascript"></script>
        <?php } ?>
        <script src="/catalog/view/javascript/common.js"></script>
        <script src="/catalog/view/theme/loudlemon/js/common.js"></script>
        <!-- <link rel="icon" href="http://loud-lemon.com/image/catalog/favicon.ico" type="image/x-icon"> -->

        <link rel="apple-touch-icon" sizes="180x180" href="/catalog/view/theme/loudlemon/images/layout/favicon/apple-touch-icon.png">
        <link rel="icon" type="image/png" href="/catalog/view/theme/loudlemon/images/layout/favicon/favicon-32x32.png" sizes="32x32">
        <link rel="icon" type="image/png" href="/catalog/view/theme/loudlemon/images/layout/favicon/favicon-16x16.png" sizes="16x16">
        <link rel="manifest" href="/catalog/view/theme/loudlemon/images/layout/favicon/manifest.json">
        <link rel="mask-icon" href="/catalog/view/theme/loudlemon/images/layout/favicon/safari-pinned-tab.svg" color="#5bbad5">
        <link rel="shortcut icon" href="/catalog/view/theme/loudlemon/images/layout/favicon/favicon.ico">
        <meta name="msapplication-config" content="/catalog/view/theme/loudlemon/images/layout/favicon/browserconfig.xml">
        <meta name="theme-color" content="#ffffff">

    </head>
    <body>  
