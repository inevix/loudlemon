 <div class="page-grid advanced-grid-<?php echo $id; ?> <?php echo $custom_class; ?> <?php if($disable_on_mobile == 1) echo 'hidden-xs hidden-sm'; ?>">
          <?php if($background_image_type == 2) { ?><div class="parallax-window" style="<?php if($background_color != '') { echo 'background-color: ' . $background_color . ';'; } if($background_image != '') { echo 'background-image: url(image/' . $background_image . ');'; } ?>" data-velocity="-0.3"><?php } ?>
          <?php if($background_image_type == 1) { ?><div style="<?php if($background_color != '') { echo 'background-color: ' . $background_color . ';'; } if($background_image != '') { echo 'background-image: url(image/' . $background_image . ');'; } echo 'background-position: ' . $background_image_position . ';background-repeat: ' . $background_image_repeat . ';background-attachment: ' . $background_image_attachment . ';'; ?>"><?php } ?>
          <?php if($background_image_type == 0) { ?><div style="<?php if($background_color != '') { echo 'background-color: ' . $background_color . ';'; } ?>"><?php } ?>
			<?php if($force_full_width != 1) { ?>
				<div class="container">
					<div class="row">
						<div class="col-sm-12">
			<?php } ?>
							<?php if($type == 'load_module') { ?>
								<?php echo $content['module']; ?>
							<?php } else if($type == 'html') { ?>
								<?php echo $content['html']; ?>
							<?php } else if($type == 'box') { 
								echo '<div class="box">';
								if ($content['title']!=''){
									echo '<div class="box-heading">';
									echo $content['title'];
									echo '</div>';
								}
								echo '<div class="box-content">';
								echo $content['text'];
								echo '</div>';
								echo '</div>';	
							} ?>	
			<?php if($force_full_width != 1) { ?>  
						</div>
					</div>
				</div>
			<?php } ?>
				
          <?php if($background_image_type == 2) { ?>
          <script type="text/javascript" src="catalog/view/theme/<?php echo $config->get( 'config_template' ); ?>/js/jquery.scrolly.js"></script>
          <script>
              $(document).ready(function(){
                 $('.advanced-grid-<?php echo $id; ?> .parallax-window').scrolly({bgParallax: true});
              });
          </script>
          <?php } ?>
     </div>
 </div>
