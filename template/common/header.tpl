<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<!--<meta name="viewport" content="width=device-width, initial-scale=1">-->
	<meta name="viewport" content="initial-scale=1, maximum-scale=1, user-scalable=no">
	<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
	<title><?php echo $title; if (isset($_GET['page'])) { echo " - ". ((int) $_GET['page'])." ".$text_page;} ?></title>
	<base href="<?php echo $base; ?>" />
	<?php if ($description) { ?>
	<meta name="description" content="<?php echo $description; if (isset($_GET['page'])) { echo " - ". ((int) $_GET['page'])." ".$text_page;} ?>" />
	<?php } ?>
	<?php if ($keywords) { ?>
	<meta name="keywords" content= "<?php echo $keywords; ?>" />
	<?php } ?>

	<?php foreach ($links as $link) { ?>
	<link href="<?php echo $link['href']; ?>" rel="<?php echo $link['rel']; ?>" />
	<?php } ?>

	<meta property="og:title" content="<?php echo $title; if (isset($_GET['page'])) { echo " - ". ((int) $_GET['page'])." ".$text_page;} ?>" />
	<meta property="og:type" content="website" />
	<meta property="og:url" content="<?php echo $og_url; ?>" />
	<?php if ($og_image) { ?>
	<meta property="og:image" content="<?php echo $og_image; ?>" />
	<?php } else { ?>
	<meta property="og:image" content="<?php echo $logo; ?>" />
	<?php } ?>
	<meta property="og:site_name" content="<?php echo $name; ?>" />
	<!-- Bootstrap -->
	<link rel="stylesheet" href="/catalog/view/theme/loudlemon/stylesheet/bootstrap.min.css">
	<link rel="stylesheet" href="/catalog/view/theme/loudlemon/stylesheet/owl.carousel.min.css" />
	<link rel="stylesheet" href="/catalog/view/theme/loudlemon/stylesheet/hamburgers.css" />
	<link rel="stylesheet" href="/catalog/view/theme/loudlemon/stylesheet/menu.css">
	<link rel="stylesheet" href="/catalog/view/theme/loudlemon/stylesheet/megamenu.css">
	<link rel="stylesheet" href="/catalog/view/theme/loudlemon/stylesheet/loudlemon.css">
	<link rel="stylesheet" href="/catalog/view/theme/loudlemon/stylesheet/new.css">

	<?php foreach ($styles as $style) { ?>
	<link href="/<?php echo $style['href']; ?>" type="text/css" rel="<?php echo $style['rel']; ?>" media="<?php echo $style['media']; ?>" />
	<?php } ?>
	<?php foreach ($analytics as $analytic) { ?>
	<?php echo $analytic; ?>
	<?php } ?>

	<link rel="stylesheet" type="text/css" href="/catalog/view/theme/loudlemon/stylesheet/thumbelina.css" />
	<style type="text/css">
		<?php if($curLang == 'en') {echo "ul.nav {padding: 0 11px;} ul.nav:last-child {padding-right: 22px; padding-left: 21px;}"; } ?>
	</style>


	<!-- Include all compiled plugins (below), or include individual files as needed -->
	<script src="/catalog/view/theme/loudlemon/js/jquery-2.2.1.min.js"></script>
	<script src="//code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
	<script src="/catalog/view/theme/loudlemon/js/owl.carousel.min.js"></script>
	<script src="/catalog/view/theme/loudlemon/js/bootstrap.min.js"></script>

	<script src="/catalog/view/theme/loudlemon/js/thumbelina.js"></script>
	<?php foreach ($scripts as $script) { ?>
	<script src="/<?php echo $script; ?>" type="text/javascript"></script>
	<?php } ?>
	<script src="/catalog/view/theme/loudlemon/js/common.js"></script>
	<!-- <link rel="icon" href="http://loud-lemon.com/image/catalog/favicon.ico" type="image/x-icon"> -->

	<script type="text/javascript">
        var responsive_design = 'yes';
	</script>

	<link rel="apple-touch-icon" sizes="180x180" href="/catalog/view/theme/loudlemon/images/layout/favicon/apple-touch-icon.png">
	<link rel="icon" type="image/png" href="/catalog/view/theme/loudlemon/images/layout/favicon/favicon-32x32.png" sizes="32x32">
	<link rel="icon" type="image/png" href="/catalog/view/theme/loudlemon/images/layout/favicon/favicon-16x16.png" sizes="16x16">
	<link rel="manifest" href="/catalog/view/theme/loudlemon/images/layout/favicon/manifest.json">
	<link rel="mask-icon" href="/catalog/view/theme/loudlemon/images/layout/favicon/safari-pinned-tab.svg" color="#5bbad5">
	<link rel="shortcut icon" href="/catalog/view/theme/loudlemon/images/layout/favicon/favicon.ico">
	<meta name="msapplication-config" content="/catalog/view/theme/loudlemon/images/layout/favicon/browserconfig.xml">
	<meta name="theme-color" content="#ffffff">

	<?php if ($_SERVER['REQUEST_URI'] == '/') { ?>

	<link rel="stylesheet" href="/catalog/view/theme/loudlemon/stylesheet/homepage-plugins.css"/>
	<link rel="stylesheet" href="/catalog/view/theme/loudlemon/stylesheet/homepage.css"/>

	<script src="/catalog/view/theme/loudlemon/js/wow.js"></script>
	<script src="/catalog/view/theme/loudlemon/js/scroll-lock.min.js"></script>
	<script src="/catalog/view/theme/loudlemon/js/homepage-plugins.js"></script>
	<script src="/catalog/view/theme/loudlemon/js/homepage.js"></script>

	<!-- Если не существует акций -->
	<script>
        window.onload = function() {
            var shareSection = $('.js-share'),
                hideSections = $('.js-content, .js-header-info, .js-header, .js-mobile-header, .js-mobile-header-nav-dropdown, .js-submenu, .js-search-panel, .js-fixed-subs');

            shareSection.addClass('is-hide is-user-closed');
            hideSections.removeClass('padding-top').addClass('not-share');
        }
	</script>
	<!-- // Если не существует акций -->

	<?php } ?>

</head>
<?php

	$config = $this->registry->get('config');
	require_once( DIR_TEMPLATE.$config->get('config_template')."/lib/module.php" );
	$modules = new Modules($this->registry);

	if ( $_SERVER['REQUEST_URI'] == '/' ) {
		require_once( DIR_TEMPLATE.$config->get('config_template').'/template/common/header_new.tpl' );
	} else {
		require_once( DIR_TEMPLATE.$config->get('config_template').'/template/common/header_old.tpl' );
	}

?>