<?php

	$config = $this->registry->get('config');
    require_once( DIR_TEMPLATE.$config->get('config_template')."/lib/module.php" );
    $modules = new Modules($this->registry);

    if ( $_SERVER['REQUEST_URI'] == '/' ) {
        require_once( DIR_TEMPLATE.$config->get('config_template').'/template/common/footer_new.tpl' );
    } else {
        require_once( DIR_TEMPLATE.$config->get('config_template').'/template/common/footer_old.tpl' );
    }

?>
