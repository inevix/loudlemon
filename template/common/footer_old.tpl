<div id="scroll">
    <img src="/catalog/view/theme/loudlemon/images/layout/up/up_button.svg">
</div>
<?php if (isset ($ya_metrika_active) && $ya_metrika_active){ ?>
<?php echo $yandex_metrika; ?>
<script type="text/javascript">
    var old_addCart = cart.add;
    cart.add = function (product_id, quantity)
    {
        var params_cart = new Array();
        params_cart['name'] = 'product id = '+product_id;
        params_cart['quantity'] = quantity;
        params_cart['price'] = 0;
        old_addCart(product_id, quantity);
        metrikaReach('metrikaCart', params_cart);
    }

    $('#button-cart').on('click', function() {
        var params_cart = new Array();
        params_cart['name'] = 'product id = '+ $('#product input[name="product_id"]').val();
        params_cart['quantity'] = $('#product input[name="quantity"]').val();
        params_cart['price'] = 0;
        metrikaReach('metrikaCart', params_cart);
    });

    function metrikaReach(goal_name, params) {
        for (var i in window) {
            if (/^yaCounter\d+/.test(i)) {
                window[i].reachGoal(goal_name, params);
            }
        }
    }
</script>
<?php } ?>

<!-- footer start -->
<footer>
    <div id="footer-menu-main">

        <div class="container">
            <div class="row">
                <div class="col-lg-2 hidden-md hidden-xs hidden-sm">
                    <img src="/catalog/view/theme/loudlemon/images/layout/main_logo_footer.svg" class="img-responsive footer-logo-img" />
                </div>
                <div class="col-lg-2 hidden-md hidden-xs hidden-sm" id="footer-category">
                    <ul class="footer-category-menu">
                        <li><a href="/guitars/"><?PHP echo $text_guitars; ?></a></li>
                        <li><a href="/bass/"><?PHP echo $text_вass; ?></a></li>
                        <li><a href="/acoustic/"><?PHP echo $text_acoustic; ?></a></li>
                    </ul>
                    <ul class="footer-category-menu">
                        <li><a href="/effects/"><?PHP echo $text_effects; ?></a></li>
                        <li><a href="/amps/"><?PHP echo $text_amps; ?></a></li>
                        <li><a href="/accessories/"><?PHP echo $text_accessories; ?></a></li>
                    </ul>
                </div>
                <div class="hidden-xs col-lg-3 col-md-4 col-sm-4 col-xs-4" id="footer-secondary-menu">
                    <ul class="footer-category-menu">
                        <li><a href="/repair/"><?PHP echo $text_repair; ?></a></li>
                        <li><a href="/warranty/"><?PHP echo "Гарантии"; ?></a></li>
                    </ul>
                    <ul class="footer-category-menu">
                        <li><a href="/delivery/"><?PHP echo $text_delivery; ?></a></li>
                        <li><a href="/contacts/"><?PHP echo $text_contacts; ?></a></li>
                        <!--<li class="langs"><?php echo $language; ?></li>-->
                    </ul>
                </div>
                <div class="col-lg-2 col-md-4 col-sm-5 col-xs-12">
                    <div class="footer-contacts">
                        <div id="footer-phone"><span class="ya-phone">+7 495 233-44-42</span></div>
                        <div id="footer-address">
                            Москва, Спартаковский пер. дом 2с1<br>
                            вход №7 (вт-вс: 12:00-20:00)<br>
                            Шоурум закрыт по понедельникам
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-4 col-sm-3 col-xs-12">
                    <div class="footer-sotials">
                        <a href=""><img id="cnpic1" src="/catalog/view/theme/loudlemon/images/layout/social_fb.svg" alt="Loud-Lemon на Facebook"></a>
                        <a href="https://www.instagram.com/loud_lemon/"><img id="cnpic2" src="/catalog/view/theme/loudlemon/images/layout/social_insta.svg" alt="Loud-Lemon В Instagram"></a>
                    </div>
                    <div class="footer-sotials">
                        <a href="https://vk.com/loud_lemon"><img id="cnpic3" src="/catalog/view/theme/loudlemon/images/layout/social_vk.svg" alt="Loud-Lemon на VK"></a>
                        <a href="https://www.youtube.com/channel/UC49el0FbI69uG8txZEhHq_w"><img id="cnpic2" src="/catalog/view/theme/loudlemon/images/layout/social_youtube.svg" alt="Loud-Lemon на YouTube"></a>
                    </div>
                </div>
            </div>
            <?php/*<div id="footer-menu" class="row">
            <div class="col-xs-6 col-sm-4 col-md-2">
                <ul>
                    <li><a href="<?=HTTPS_SERVER?>/guitars/"><?php if($curLang == 'en') {echo "Guitars"; }else { echo "Гитары"; } ?></a></li>
                    <li><a href="<?=HTTPS_SERVER?>/guitars/?filter=4"><?php if($curLang == 'en') {echo "Solid"; }else { echo "Цельнокорпусные"; } ?></a></li>
                    <li><a href="<?=HTTPS_SERVER?>/guitars/?filter=3"><?php if($curLang == 'en') {echo "Hollow-bodу"; }else { echo "Hollow-bodу"; } ?></a></li>
                    <li class="hidden-xs hidden-sm"><a href="<?=HTTPS_SERVER?>/guitars/?filter=106"><?php if($curLang == 'en') {echo "Left-handed"; }else { echo "Леворукие"; } ?></a></li>
                </ul>
            </div>
            <div class="col-xs-6 col-sm-4 col-md-2 hidden-sxs">
                <ul>
                    <li><a href="<?=HTTPS_SERVER?>/bass/"><?php if($curLang == 'en') {echo "Bass"; }else { echo "Бас-гитары"; } ?></a></li>
                    <li><a href="<?=HTTPS_SERVER?>/bass/?filter=127"><?php if($curLang == 'en') {echo "4-strings"; }else { echo "4 струны"; } ?></a></li>
                    <li style="visibility: hidden;"><a href=""><?php if($curLang == 'en') {echo "5-strings"; }else { echo "5 струн"; } ?></a></li>
                </ul>
            </div>
            <div class="col-xs-6 col-sm-4 col-md-2">
                <ul>
                    <li><a href="<?=HTTPS_SERVER?>/acoustic/"><?php if($curLang == 'en') {echo "Acoustic"; }else { echo "Акустика"; } ?></a></li>
                    <li><a href="/acoustic/?filter=144"><?php if($curLang == 'en') {echo "Guitars"; }else { echo "Гитары"; } ?></a></li>
                    <!--             <li><a href="/acoustic/?filter=145"><?php if($curLang == 'en') {echo "Ukuleles"; }else { echo "Укулеле"; } ?></a></li>
            <li><a href="/acoustic/?filter=146"><?php if($curLang == 'en') {echo "Mandolines"; }else { echo "Мандолины"; } ?></a></li>
            <li><a href="/acoustic/?filter=147"><?php if($curLang == 'en') {echo "Banjos"; }else { echo "Банджо"; } ?></a></li> -->
                </ul>
            </div>
            <div class="col-xs-6 col-sm-4 col-md-2" id="title-links">
                <ul>
                    <li><a href="<?=HTTPS_SERVER?>/amps/"><?php if($curLang == 'en') {echo "Amps"; }else { echo "Усиление"; } ?></a></li>
                    <?php if($curLang == 'ru') {?>
                    <li><a href="/amps/?filter=118">20 Вт и меньше</a></li><li><a href="/amps/?filter=119">20 Вт - 40 Вт</a></li><li><a href="/amps/?filter=120">40 Вт и больше</a></li>
                    <?php } else {?>
                    <li><a href="/amps/?filter=118">20 W or less</a></li><li><a href="/amps/?filter=119">20 W - 40 W</a></li><li><a href="/amps/?filter=120">40 W and more</a></li>
                    <?php } ?>
                </ul>
            </div>
            <div class="col-xs-6 col-sm-4 col-md-2" id="title-links">
                <ul>
                    <li><a href="<?=HTTPS_SERVER?>/effects/"><?php if($curLang == 'en') {echo "Effects"; }else { echo "Эффекты"; } ?></a></li>
                    <li><?php if($curLang == 'en') {echo "Coming soon :)"; }else { echo "Скоро будут :)"; } ?></li>
                </ul>
            </div>
            <div class="col-xs-6 col-sm-4 col-md-2">
                <ul>
                    <li><a href="<?=HTTPS_SERVER?>/strings/"><?php if($curLang == 'en') {echo "Acsessorise"; }else { echo "Аксессуары"; } ?></a></li>
                    <li><a href="/straps/"><?php if($curLang == 'en') {echo "Straps"; }else { echo "Ремни"; } ?></a></li>
                    <li><a href="/strings/"><?php if($curLang == 'en') {echo "Strings"; }else { echo "Струны"; } ?></a></li>
                    <li><a href="/cabel/"><?php if($curLang == 'en') {echo "Cables"; }else { echo "Кабели"; } ?></a></li>
                    <li><a href="/parts/"><?php if($curLang == 'en') {echo "Parts"; }else { echo "Запчасти"; } ?></a></li>
                    <li><a href="/cases/"><?php if($curLang == 'en') {echo "Cases"; }else { echo "Кейсы"; } ?></a></li>
                </ul>
                <!-- <ul class="hidden-xs hidden-sm hidden-md hidden-lg hidden-xl nothidden-sxs">
            <li><a href="<?=HTTPS_SERVER?>/bass/"><?php if($curLang == 'en') {echo "Bass"; }else { echo "Бас-гитары"; } ?></a></li>
            <li><a href=""><?php if($curLang == 'en') {echo "5-strings"; }else { echo "5 струн"; } ?></a></li>
            <li><a href="<?=HTTPS_SERVER?>/bass/?filter=127"><?php if($curLang == 'en') {echo "4-strings"; }else { echo "4 струны"; } ?></a></li>
          </ul> -->
            </div>
            <div class="col-xs-12 footer-last-ul" id="title-links">
                <ul class="footer-last-ul-ul">
                    <li><a href="<?=HTTPS_SERVER?>/repair"><?php if($curLang == 'en') {echo "Repair"; }else { echo "Ремонт"; } ?></a></li>
                    <li><a href="<?=HTTPS_SERVER?>/delivery"><?php if($curLang == 'en') {echo "Delivery"; }else { echo "Доставка"; } ?></a></li>
                    <li><a href="<?=HTTPS_SERVER?>/contacts"><?php if($curLang == 'en') {echo "Contacts"; }else { echo "Контакты"; } ?></a></li>
                    <li><a href="<?=HTTPS_SERVER?>/shop"><?php if($curLang == 'en') {echo "Shop"; }else { echo "Магазин"; } ?></a> <span class="glyphicon glyphicon-home"></span></li>
                </ul>
            </div>
            <!-- <div class="col-xs-6 col-sm-4 col-md-2">
              <div id="footer-payments1" class="footer-payments">
                <div class="row">
                  <div class="col-xs-6 vcenter"><img src="/catalog/view/theme/loudlemon/images/layout/payment/pay_yandex.svg" alt="" class="img-responsive"></div>
                  <div class="col-xs-6 vcenter"><img src="/catalog/view/theme/loudlemon/images/layout/payment/pay_mastercard.svg" alt="" class="img-responsive"></div>
                  <div class="col-xs-6 vcenter"><img src="/catalog/view/theme/loudlemon/images/layout/payment/pay_maestro.svg" alt="" class="img-responsive"></div>
                  <div class="col-xs-6 vcenter"><img src="/catalog/view/theme/loudlemon/images/layout/payment/pay_visa.svg" alt="" class="img-responsive"></div>
                  <div class="col-xs-6 vcenter"><img src="/catalog/view/theme/loudlemon/images/layout/payment/pay_mir.png" alt="" class="img-responsive"></div>
                  <div class="col-xs-6 vcenter"><img src="/catalog/view/theme/loudlemon/images/layout/payment/pay_qiwi.svg" alt="" class="img-responsive"></div>
                </div>
              </div>
            </div> -->
        </div>

        <div class="footer-payments">
            <div class="row">
                <div class="col-xs-2 vcenter"><img src="/catalog/view/theme/loudlemon/images/layout/payment/pay_yandex.svg" alt="" class="img-responsive"></div>
                <div class="col-xs-2 vcenter"><img src="/catalog/view/theme/loudlemon/images/layout/payment/pay_mastercard.svg" alt="" class="img-responsive"></div>
                <div class="col-xs-2 vcenter"><img src="/catalog/view/theme/loudlemon/images/layout/payment/pay_maestro.svg" alt="" class="img-responsive"></div>
                <div class="col-xs-2 vcenter"><img src="/catalog/view/theme/loudlemon/images/layout/payment/pay_visa.svg" alt="" class="img-responsive"></div>
                <div class="col-xs-2 vcenter"><img src="/catalog/view/theme/loudlemon/images/layout/payment/pay_mir.png" alt="" class="img-responsive"></div>
                <div class="col-xs-2 vcenter"><img src="/catalog/view/theme/loudlemon/images/layout/payment/pay_qiwi.svg" alt="" class="img-responsive"></div>
            </div>
        </div>
        */?>
    </div>

    </div>
    <div id="footer-secondary" class="hidden-xs">
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-sm-3 col-xs-3">
                    <div id="footer-name">
                        LoudLemon &copy; 2014-2018
                    </div>
                </div>
                <div class="col-md-6 col-sm-6 col-xs-6" id="footer-payments">
                    <div>
                        <img src="/catalog/view/theme/loudlemon/images/layout/payment_new/pay_yandex.svg" />
                        <img src="/catalog/view/theme/loudlemon/images/layout/payment_new/pay_maestro.svg" />
                        <img src="/catalog/view/theme/loudlemon/images/layout/payment_new/pay_mastercard.svg" />
                        <img src="/catalog/view/theme/loudlemon/images/layout/payment_new/pay_visa.svg" />
                        <img src="/catalog/view/theme/loudlemon/images/layout/payment_new/pay_mir.svg" />
                        <img src="/catalog/view/theme/loudlemon/images/layout/payment_new/pay_quiwi.svg" />
                    </div>
                </div>
                <div class="col-md-3 col-sm-3 col-xs-3">
                    <div class="our-logo"></div>
                </div>
            </div>
        </div>
    </div>

    <div id="footer-secondary" class="visible-xs">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-sm-6 col-xs-12" id="footer-payments">
                    <div>
                        <img src="/catalog/view/theme/loudlemon/images/layout/payment_new/pay_yandex.svg" />
                        <img src="/catalog/view/theme/loudlemon/images/layout/payment_new/pay_maestro.svg" />
                        <img src="/catalog/view/theme/loudlemon/images/layout/payment_new/pay_mastercard.svg" />
                        <img src="/catalog/view/theme/loudlemon/images/layout/payment_new/pay_visa.svg" />
                        <img src="/catalog/view/theme/loudlemon/images/layout/payment_new/pay_mir.svg" />
                        <img src="/catalog/view/theme/loudlemon/images/layout/payment_new/pay_quiwi.svg" />
                    </div>
                </div>
                <div class="col-xs-12">
                    <div id="footer-name" class="text-center">
                        LoudLemon &copy; 2014-2018
                    </div>
                </div>
            </div>
        </div>
    </div>

</footer>

<?php

  if(isset($_GET['pmin'])){
    $pmin = $_GET['pmin'];
  }else{
    $pmin = 0;
  }

  if(isset($_GET['pmax'])){
    $pmax = $_GET['pmax'];
  }else{
    $pmax = 200000;
  }

?>

<script src="/catalog/view/theme/loudlemon/js/svg-loader.php"></script>
<script src="/catalog/view/javascript/stickyfill.min.js"></script>
<script src="/catalog/view/javascript/jquery.mask.min.js"></script>
<script src="/catalog/view/javascript/jquery.parallax.js"></script>
<script src="/catalog/view/theme/loudlemon/js/footer.js"></script>
<script type="text/javascript" src="/catalog/view/theme/loudlemon/js/megamenu.js"></script>

</body>

</html>