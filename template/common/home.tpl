<?php echo $header;?>

<main class="view-section">
<div class="box-main-slider">
    <div class="js-main-slider main-slider">
        <?php foreach ($banners as $banner) {
        $options = explode('-', $banner['title']);
        $text = isset($options[0]) ? explode('|', $options[0]) : [];
        $position = isset($options[1]) ? trim($options[1]) : 'center';
        ?>
        <div>
            <div class="main-slider__title__img">
                <img src="<?php echo $banner['image']; ?>">
            </div>
            <div class="main-slider__title banner-<?php echo $position ?>">
                <?php if (count($text) > 1) { $count = 1; ?>
                    <?php foreach ($text as $value) { ?>
                        <?php if ($count == 2) { ?>
                            <h1 class="text-fade">
                                <span>
                                    <?php echo $value; ?>
                                </span>
                            </h1>
                        <?php } else { ?>
                            <h2 class="text-fade">
                                <span>
                                    <?php echo $value; ?>
                                </span>
                            </h2>
                        <?php } ?>
                    <?php $count++; } ?>
                <?php } else { ?>
                    <h1 class="text-fade">
                        <span>
                            <?php echo isset($text[0]) ? $text[0] : ''; ?>
                        </span>
                    </h1>
                <?php } ?>

                <div class="main-slider__title__button button-fade">
                    <a href="<?php echo $banner['link']; ?>" class="button button--dbl-border">
                        <span class="text-label">
                            Подробнее
                        </span>
                    </a>
                </div>
            </div>
        </div>
        <?php } ?>
    </div>
</div>

<?php echo $content_bottom; ?>

<!-- <section class="view-guitars">
    <div class="view-text">
        <div class="flex-box flex-center box-title">
            <div class="box-title__lines box-title__lines--left wow fadeInLeft" data-wow-delay=".2s" data-wow-duration="1s"></div>
            <h1 class="wow fadeInUp" data-wow-delay=".3s" data-wow-duration="1s">
                Свежие поступления гитар
            </h1>
            <div class="box-title__lines box-title__lines--right wow fadeInRight" data-wow-delay=".2s" data-wow-duration="1s"></div>
        </div>
    </div>
    <div class="fb-container">
        <ul class="flex-box flex-wrap flex-start flex-align-stretch products-list">
            <li class="flex-col-4 products-list__item">
                <a href="#" class="products-list__item__img">
                    <img src="/catalog/view/theme/loudlemon/images/homepage/guitars/7.jpg" alt="Guitar preview"/>
                </a>
                <a href="#" class="products-list__item__brand">
                    Fender
                </a>
                <a href="#" class="products-list__item__title">
                    American Pro Jazzmaster Mystic Seafoam Green
                </a>
                <div class="products-list__item__cost">
                    99 800 руб.
                </div>
            </li>
            <li class="flex-col-4 products-list__item">
                <a href="#" class="products-list__item__img">
                    <img src="/catalog/view/theme/loudlemon/images/homepage/guitars/1.jpg" alt="Guitar preview"/>
                </a>
                <a href="#" class="products-list__item__brand">
                    GIBSON
                </a>
                <a href="#" class="products-list__item__title">
                    Explorer 2005 Ebony
                </a>
                <div class="products-list__item__cost">
                    55 000 руб.
                </div>
            </li>
            <li class="flex-col-4 products-list__item">
                <a href="#" class="products-list__item__img">
                    <img src="/catalog/view/theme/loudlemon/images/homepage/guitars/2.jpg" alt="Guitar preview"/>
                </a>
                <a href="#" class="products-list__item__brand">
                    Rickenbacker
                </a>
                <a href="#" class="products-list__item__title">
                    330 1993 Midnight Blue
                </a>
                <div class="products-list__item__cost">
                    89 800 руб.
                </div>
            </li>
            <li class="flex-col-4 products-list__item">
                <a href="#" class="products-list__item__img">
                    <img src="/catalog/view/theme/loudlemon/images/homepage/guitars/4.jpg" alt="Guitar preview"/>
                </a>
                <a href="#" class="products-list__item__brand">
                    Fender
                </a>
                <a href="#" class="products-list__item__title">
                    American Stratocaster 1983 Olympic White
                </a>
                <div class="products-list__item__cost">
                    <div class="products-list__item__cost__old">
                        84 000 руб.
                    </div>
                    69 800 руб.
                </div>
            </li>
            <li class="flex-col-4 products-list__item">
                <a href="#" class="products-list__item__img">
                    <img src="/catalog/view/theme/loudlemon/images/homepage/guitars/5.jpg" alt="Guitar preview"/>
                </a>
                <a href="#" class="products-list__item__brand">
                    Yamaha
                </a>
                <a href="#" class="products-list__item__title">
                    SG-1000 1982 Black
                </a>
                <div class="products-list__item__cost">
                    55 000 руб.
                </div>
            </li>
            <li class="flex-col-4 products-list__item">
                <a href="#" class="products-list__item__img">
                    <img src="/catalog/view/theme/loudlemon/images/homepage/guitars/6.jpg" alt="Guitar preview"/>
                </a>
                <a href="#" class="products-list__item__brand">
                    Fender
                </a>
                <a href="#" class="products-list__item__title">
                    Coronado II 1967 Wildwood
                </a>
                <div class="products-list__item__cost">
                    125 000 руб.
                </div>
            </li>
            <li class="flex-col-4 products-list__item">
                <a href="#" class="products-list__item__img">
                    <img src="/catalog/view/theme/loudlemon/images/homepage/guitars/3.jpg" alt="Guitar preview"/>
                </a>
                <a href="#" class="products-list__item__brand">
                    Fender
                </a>
                <a href="#" class="products-list__item__title">
                    American Standard 1989 Black Relic
                </a>
                <div class="products-list__item__cost">
                    75 000 руб.
                </div>
            </li>
            <li class="flex-col-4 products-list__item">
                <a href="#" class="products-list__item__img">
                    <img src="/catalog/view/theme/loudlemon/images/homepage/guitars/8.jpg" alt="Guitar preview"/>
                </a>
                <a href="#" class="products-list__item__brand">
                    Fender
                </a>
                <a href="#" class="products-list__item__title">
                    American Standard 1989 Blue Relic
                </a>
                <div class="products-list__item__cost">
                    75 000 руб.
                </div>
            </li>
        </ul>
    </div>
</section>  -->

<!-- <section class="view-accessories">
    <div class="view-text">
        <div class="flex-box flex-center box-title">
            <div class="box-title__lines box-title__lines--left wow fadeInLeft" data-wow-delay=".2s" data-wow-duration="1s"></div>
            <h1 class="wow fadeInUp" data-wow-delay=".3s" data-wow-duration="1s">
                Свежие поступления аксессуаров
            </h1>
            <div class="box-title__lines box-title__lines--right wow fadeInRight" data-wow-delay=".2s" data-wow-duration="1s"></div>
        </div>
    </div>
    <div class="fb-container">
        <ul class="flex-box flex-wrap flex-start flex-align-stretch products-list">
            <li class="flex-col-4 products-list__item">
                <a href="#" class="products-list__item__img">
                    <img src="/catalog/view/theme/loudlemon/images/homepage/accessories/1.jpg" alt="Accessories preview"/>
                </a>
                <a href="#" class="products-list__item__brand">
                    Gibson
                </a>
                <a href="#" class="products-list__item__title">
                    Pilsner Glass Set стакан для пива с подставкой
                </a>
                <div class="products-list__item__cost">
                    2 500 руб.
                </div>
            </li>
            <li class="flex-col-4 products-list__item">
                <a href="#" class="products-list__item__img">
                    <img src="/catalog/view/theme/loudlemon/images/homepage/accessories/2.jpg" alt="Accessories preview"/>
                </a>
                <a href="#" class="products-list__item__brand">
                    Original Fuzz
                </a>
                <a href="#" class="products-list__item__title">
                    Indian Guitar Strap in Dandi
                </a>
                <div class="products-list__item__cost">
                    3 850 руб.
                </div>
            </li>
            <li class="flex-col-4 products-list__item">
                <a href="#" class="products-list__item__img">
                    <img src="/catalog/view/theme/loudlemon/images/homepage/accessories/3.png" alt="Accessories preview"/>
                </a>
                <a href="#" class="products-list__item__brand">
                    Gibson
                </a>
                <a href="#" class="products-list__item__title">
                    '57 Classic (Nickel)
                </a>
                <div class="products-list__item__cost">
                    11 680 руб.
                </div>
            </li>
            <li class="flex-col-4 products-list__item">
                <a href="#" class="products-list__item__img">
                    <img src="/catalog/view/theme/loudlemon/images/homepage/accessories/4.png" alt="Accessories preview"/>
                </a>
                <a href="#" class="products-list__item__brand">
                    Gibson
                </a>
                <a href="#" class="products-list__item__title">
                    Distressed Gibson Logo футболка
                </a>
                <div class="products-list__item__cost">
                    1 980 руб.
                </div>
            </li>
        </ul>
    </div>
</section> -->

<nav class="view-categories">
    <div class="flex-box view-categories__row">
        <div class="view-categories__row__column view-categories__row__column--left">
            <?php if (isset($collections[0])) { $collection = $collections[0]; ?>
                <?php list($collection_title, $collection_category) = explode('|', $collection['title']); ?>
                <div class="box-category">
                    <a href="<?php echo $collection['link'] ?>" class="box-category__img">
                        <img src="/image/<?php echo $collection['image'] ?>" alt="<?php echo $collection['title'] ?>"/>
                    </a>
                    <div class="box-category__content">
                        <a href="#" class="box-category__content__title">
                            <?php echo $collection_category ?>
                        </a>
                        <a href="<?php echo $collection['link'] ?>" class="box-category__content__desc">
                            <?php echo $collection_title ?>
                        </a>
                        <a href="<?php echo $collection['link'] ?>" class="button button--dbl-border">
                            <span class="text-label">
                                Перейти
                            </span>
                        </a>
                    </div>
                </div>
            <?php } ?>
        </div>
        <div class="view-categories__row__column view-categories__row__column--right">
            <menu class="flex-box flex-wrap flex-start colletions-list">
                <?php foreach ($collections as $key => $collection) { ?>

                    <?php if ($key == 0) continue; ?>

                    <?php list($collection_title, $collection_category) = explode('|', $collection['title']); ?>

                    <li class="flex-col-2 colletions-list__item">
                        <div class="box-category">
                            <a href="<?php echo $collection['link'] ?>" class="box-category__img">
                                <img src="/image/<?php echo $collection['image'] ?>" alt="<?php echo $collection['title'] ?>"/>
                            </a>
                            <div class="box-category__content">
                                <a href="#" class="box-category__content__title">
                                    <?php echo $collection_category ?>
                                </a>
                                <a href="<?php echo $collection['link'] ?>" class="box-category__content__desc">
                                    <?php echo $collection_title ?>
                                </a>
                                <a href="<?php echo $collection['link'] ?>" class="button button--dbl-border">
                                    <span class="text-label">
                                        Перейти
                                    </span>
                                </a>
                            </div>
                        </div>
                    </li>
                <?php } ?>
                <!--<li class="flex-col-2 categories-list__item">
                    <div class="box-category">
                        <a href="#" class="box-category__img">
                            <img src="/catalog/view/theme/loudlemon/images/homepage/categories/category3.jpg" alt="Category poster"/>
                        </a>
                        <div class="box-category__content wow fadeInLeft" data-wow-delay=".3s" data-wow-duration="1s">
                            <a href="#" class="box-category__content__title">
                                Аксессуары
                            </a>
                            <a href="#" class="box-category__content__desc">
                                Всё для ухода за гитарой
                            </a>
                            <a href="#" class="button button--dbl-border">
                                <span class="text-label">
                                    Перейти
                                </span>
                            </a>
                        </div>
                    </div>
                </li>
                <li class="flex-col-2 categories-list__item">
                    <div class="box-category">
                        <a href="#" class="box-category__img">
                            <img src="/catalog/view/theme/loudlemon/images/homepage/categories/category4.jpg" alt="Category poster"/>
                        </a>
                        <div class="box-category__content wow fadeInLeft" data-wow-delay=".3s" data-wow-duration="1s">
                            <a href="#" class="box-category__content__title">
                                Блог
                            </a>
                            <a href="#" class="box-category__content__desc">
                                Fender и цветастая эстетика 60-х
                            </a>
                            <a href="#" class="button button--dbl-border">
                                <span class="text-label">
                                    Перейти
                                </span>
                            </a>
                        </div>
                    </div>
                </li>
                <li class="flex-col-2 categories-list__item">
                    <div class="box-category">
                        <a href="#" class="box-category__img">
                            <img src="/catalog/view/theme/loudlemon/images/homepage/categories/category5.jpg" alt="Category poster"/>
                        </a>
                        <div class="box-category__content wow fadeInLeft" data-wow-delay=".3s" data-wow-duration="1s">
                            <a href="#" class="box-category__content__title">
                                Акции
                            </a>
                            <a href="#" class="box-category__content__desc">
                                Усилители с усиленной скидкой!
                            </a>
                            <a href="#" class="button button--dbl-border">
                                <span class="text-label">
                                    Перейти
                                </span>
                            </a>
                        </div>
                    </div>
                </li>-->
            </menu>
        </div>
    </div>
</nav>
<div class="js-box-video box-video">
    <video class="js-homepage-video homepage-video" poster="/catalog/view/theme/loudlemon/images/homepage/img_video-poster.png" loop muted>
        <source src="/catalog/view/theme/loudlemon/source/video_homepage.mp4" type="video/mp4">
    </video>
    <div class="box-video__buttons">
        <button class="js-video-volume video-volume is-mute">
            <i class="icon-icon_video-sound"></i>
        </button>
        <a href="#" class="button button--dbl-border">
            <span class="text-label">
                Подробнее
            </span>
        </a>
    </div>
</div>

<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/catalog/view/theme/loudlemon/lib/Instagram.php';
$media_list = $media->data;
?>
<section class="js-news view-news">
    <div class="view-text">
        <div class="flex-box flex-center box-title">
            <div class="box-title__lines box-title__lines--left wow fadeInLeft" data-wow-delay=".2s" data-wow-duration="1s"></div>
            <h1 class="wow fadeInUp" data-wow-delay=".3s" data-wow-duration="1s">
                Свежие новости
            </h1>
            <div class="box-title__lines box-title__lines--right wow fadeInRight" data-wow-delay=".2s" data-wow-duration="1s"></div>
        </div>
    </div>
    <div class="js-news-carousel view-news__carousel">
        <?php foreach($media->data as $media) { ?>
        <div>
            <div class="js-open-new box-new">
                <div class="box-new__img">
                    <img src="<?php echo $media->images->standard_resolution->url ?>" alt="<?php echo $media->caption->text ?>"/>
                </div>
                <div class="box-new__desc">
                    <div class="box-new__desc__text">
                        <?php echo $media->caption->text ?>
                    </div>
                </div>
            </div>
        </div>
        <?php } ?>
    </div>
    <div class="js-news-popup view-news__popup"></div>
    <div class="js-news-popup-content view-news__popup__content sl--scrollable">
        <a href="#" class="js-close-new close-new"></a>
        <div class="js-news-slider view-news__slider">
            <?php foreach($media_list as $media) { ?>
            <div>
                <div class="flex-box flex-center flex-align-stretch box-new">
                    <a href="#" class="flex-col-2 box-new__img">
                        <img src="<?php echo $media->images->standard_resolution->url ?>" alt="New's image"/>
                    </a>
                    <div class="flex-col-2 box-new__desc">
                        <div class="flex-box flex-align-center box-new__desc__info">
                            <a href="#" class="flex-box flex-start flex-align-center box-logo">
                                <span class="box-logo__img">
                                    <img src="/catalog/view/theme/loudlemon/images/homepage/logo.svg" alt="Icon logo"/>
                                </span>
                                <span class="box-logo__label">
                                    <?php echo $media->user->full_name ?>
                                </span>
                            </a>
                            <div class="box-date">
                                <?php
                                $date = new DateTime();
                                $date->setTimestamp($media->caption->created_time);

                                echo $date->format('d M');
                                ?>
                            </div>
                        </div>
                        <div class="js-insta-text box-new__desc__text">
                            <?php echo $media->caption->text ?>
                        </div>
                        <div class="flex-box flex-start flex-align-center box-new__desc__stats">
                            <a href="#" class="flex-box flex-start flex-align-center box-likes">
                                <span class="box-likes__img">
                                    <img src="/catalog/view/theme/loudlemon/images/homepage/icons/insta_like.svg" alt="Icon like"/>
                                </span>
                                <span class="box-likes__count">
                                    <?php echo $media->likes->count ?>
                                </span>
                            </a>
                            <a href="#" class="flex-box flex-start flex-align-center box-comments">
                                    <span class="box-comments__img">
                                        <img src="/catalog/view/theme/loudlemon/images/homepage/icons/insta_comment.svg" alt="Icon like"/>
                                    </span>
                                <span class="box-comments__count">
                                    <?php echo $media->comments->count ?>
                                </span>
                            </a>
                        </div>
                        <div class="box-new__desc__button">
                            <a href="<?php echo $media->link ?>" target="_blank" class="button button--dbl-border button--dbl-border--orange">
                                <span class="text-label">
                                    Смотреть в Instagram
                                </span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <?php } ?>
        </div>
    </div>
</section>
<section class="view-advantages">
    <div class="view-text">
        <div class="flex-box flex-center box-title">
            <div class="box-title__lines box-title__lines--left wow fadeInLeft" data-wow-delay=".2s" data-wow-duration="1s"></div>
            <h1 class="wow fadeInUp" data-wow-delay=".3s" data-wow-duration="1s">
                Наши преимущества
            </h1>
            <div class="box-title__lines box-title__lines--right wow fadeInRight" data-wow-delay=".2s" data-wow-duration="1s"></div>
        </div>
        <h2>
            Каждый день мы обслуживаем людей так, как хотели чтобы обслуживали нас
        </h2>
    </div>
    <div class="fb-container">
        <ul class="flex-box flex-wrap advantages-list">
            <li class="flex-col-3 advantages-list__item wow zoomInUp" data-wow-duration="1.5s">
                <div class="advantages-list__item__img">
                    <img src="/catalog/view/theme/loudlemon/images/homepage/advantages/advantage_1.svg" alt="Advantage icon"/>
                    <div class="advantages-list__item__title">
                        Больше чем просто покупка
                    </div>
                </div>
                <div class="advantages-list__item__desc">
                    Отрегулируем, настроим под вас, модифицируем если нужно, дадим советы, поделимся опытом.
                </div>
            </li>
            <li class="flex-col-3 advantages-list__item wow zoomInUp" data-wow-duration="1.5s">
                <div class="advantages-list__item__img">
                    <img src="/catalog/view/theme/loudlemon/images/homepage/advantages/advantage_2.svg" alt="Advantage icon"/>
                    <div class="advantages-list__item__title">
                        Ламповая атмосфера
                    </div>
                </div>
                <div class="advantages-list__item__desc">
                    В нашем лофте уютнее чем дома
                </div>
            </li>
            <li class="flex-col-3 advantages-list__item wow zoomInUp" data-wow-duration="1.5s">
                <div class="advantages-list__item__img">
                    <img src="/catalog/view/theme/loudlemon/images/homepage/advantages/advantage_3.svg" alt="Advantage icon"/>
                    <div class="advantages-list__item__title">
                        Молниеносная доставка
                    </div>
                </div>
                <div class="advantages-list__item__desc">
                    Доставка по Москве в день заказа. Отправка по России в день оплаты.
                </div>
            </li>
            <li class="flex-col-3 advantages-list__item wow zoomInUp" data-wow-duration="1.5s">
                <div class="advantages-list__item__img">
                    <img src="/catalog/view/theme/loudlemon/images/homepage/advantages/advantage_4.svg" alt="Advantage icon"/>
                    <div class="advantages-list__item__title">
                        Действительно хорошие цены
                    </div>
                </div>
                <div class="advantages-list__item__desc">
                    Удерживаем наши цены на уровне внутреннего рынка США.
                </div>
            </li>
            <li class="flex-col-3 advantages-list__item wow zoomInUp" data-wow-duration="1.5s">
                <div class="advantages-list__item__img">
                    <img src="/catalog/view/theme/loudlemon/images/homepage/advantages/advantage_5.svg" alt="Advantage icon"/>
                    <div class="advantages-list__item__title">
                        Нет проблем с парковкой
                    </div>
                </div>
                <div class="advantages-list__item__desc">
                    Собственная бесплатная парковка прямо у входа. Места для наших клиентов есть всегда.
                </div>
            </li>
            <li class="flex-col-3 advantages-list__item wow zoomInUp" data-wow-duration="1.5s">
                <div class="advantages-list__item__img">
                    <img src="/catalog/view/theme/loudlemon/images/homepage/advantages/advantage_6.svg" alt="Advantage icon"/>
                    <div class="advantages-list__item__title">
                        Оплата картой и сложная оплата
                    </div>
                </div>
                <div class="advantages-list__item__desc">
                    Можете заплатить хоть с 5 карт, платя по частям с каждой.
                </div>
            </li>
        </ul>
    </div>
</section>
<section class="view-reviews">
    <div class="view-text">
        <div class="flex-box flex-center box-title box-title--white">
            <div class="box-title__lines box-title__lines--left wow fadeInLeft" data-wow-delay=".2s" data-wow-duration="1s"></div>
            <h1 class="wow fadeInUp" data-wow-delay=".3s" data-wow-duration="1s">
                Отзывы
            </h1>
            <div class="box-title__lines box-title__lines--right wow fadeInRight" data-wow-delay=".2s" data-wow-duration="1s"></div>
        </div>
    </div>
    <div class="js-reviews-slider view-reviews__slider">
        <?php foreach($reviews as $review) { ?>
        <div>
            <div class="flex-box flex-center flex-align-center box-review">
                <div class="box-review__img">
                    <img src="/image/<?php echo $review['image'] ?>" alt="<?php echo $review['title'] ?>"/>
                </div>
                <div class="box-review__content">
                    <div class="box-review__content__text">
                        <?php echo $review['text'] ?>
                    </div>
                    <div class="flex-box flex-align-center box-review__content__author">
                        <div class="box-review__content__author__line"></div>
                        <div class="box-review__content__author__name">
                            <?php echo $review['title'] ?>
                        </div>
                        <div class="box-review__content__author__line"></div>
                    </div>
                </div>
            </div>
        </div>
        <?php } ?>
        <!--<div>
            <div class="flex-box flex-center flex-align-center box-review">
                <div class="box-review__img">
                    <img src="/catalog/view/theme/loudlemon/images/homepage/reviews/photo2.jpg" alt="Author's photo"/>
                </div>
                <div class="box-review__content">
                    <div class="box-review__content__text">
                        Место, где мою гитару любят и чинят
                    </div>
                    <div class="flex-box flex-align-center box-review__content__author">
                        <div class="box-review__content__author__line"></div>
                        <div class="box-review__content__author__name">
                            Валерий Ковалёв
                        </div>
                        <div class="box-review__content__author__line"></div>
                    </div>
                </div>
            </div>
        </div>
        <div>
            <div class="flex-box flex-center flex-align-center box-review">
                <div class="box-review__img">
                    <img src="/catalog/view/theme/loudlemon/images/homepage/reviews/photo3.jpg" alt="Author's photo"/>
                </div>
                <div class="box-review__content">
                    <div class="box-review__content__text">
                        Захотелось мне заиметь себе арктоп. Ну, это такой тип гитар - большой корпус, эфы, джазовый такой инструментик. Бюджет, естессно, ограничен -нам,простым московским безработным не до custom shop). Случайно нашел сайт данного магазина в сети и решил заехать. И вот тут началось волшебство. Я сначала думал что ввалился в частные апартаменты какого-то гитарного коллекционера). До самого потолка висят, мягко говоря, нетривиальные вещицы. Есть и винтажные инструменты. А главное, встретили милые улыбающиеся люди,которым было не лень лезть по стремянке под потолок доставать мне гитарку,покупать которую я даже не планировал(но, в итоге,купил))). Короче, вот прям, честно - офигенный магаз! По фендерам есть ващще все, плюс много фурнитуры. А какой клевый gibson es-135... Короче говоря рекомендую категорически - там работают люди, любящие гитары.
                    </div>
                    <div class="flex-box flex-align-center box-review__content__author">
                        <div class="box-review__content__author__line"></div>
                        <div class="box-review__content__author__name">
                            Павел Сучков
                        </div>
                        <div class="box-review__content__author__line"></div>
                    </div>
                </div>
            </div>
        </div>
        <div>
            <div class="flex-box flex-center flex-align-center box-review">
                <div class="box-review__img">
                    <img src="/catalog/view/theme/loudlemon/images/homepage/reviews/photo4.jpg" alt="Author's photo"/>
                </div>
                <div class="box-review__content">
                    <div class="box-review__content__text">
                        Купил в этом магазине б/у Fender Jaguar, гитара по состоянию была как новая, собственно как и все другие б/у инструменты в магазине, которые я видел. Покупкой доволен, продавцы очень приветливые и отзывчивые, помогали с выбором гитары. Шоурум стильный и красивый, в нем было приятно провести время, количество уникальных и интересных инструментов впечатляет. Магазин заслуженно получает пять звезд!
                    </div>
                    <div class="flex-box flex-align-center box-review__content__author">
                        <div class="box-review__content__author__line"></div>
                        <div class="box-review__content__author__name">
                            Глеб Захарян
                        </div>
                        <div class="box-review__content__author__line"></div>
                    </div>
                </div>
            </div>
        </div>
        <div>
            <div class="flex-box flex-center flex-align-center box-review">
                <div class="box-review__img">
                    <img src="/catalog/view/theme/loudlemon/images/homepage/reviews/photo5.jpg" alt="Author's photo"/>
                </div>
                <div class="box-review__content">
                    <div class="box-review__content__text">
                        Я после телефонного разговора сразу понял, что акустику буду заказывать именно здесь. Девушка по имени Нина настолько расположила к себе, что сразу все сомнения, связанные с пересылом, упаковкой, невозможностью пощупать гитару на месте развеялись. В итоге вот все идеально: до Иваново гитара приехала за два дня, отстроенная, без единой царапинки, очень качественно упакованная. Прям от души, спасибо вам, ребята!
                    </div>
                    <div class="flex-box flex-align-center box-review__content__author">
                        <div class="box-review__content__author__line"></div>
                        <div class="box-review__content__author__name">
                            Роман Ходаковский
                        </div>
                        <div class="box-review__content__author__line"></div>
                    </div>
                </div>
            </div>
        </div>
        <div>
            <div class="flex-box flex-center flex-align-center box-review">
                <div class="box-review__img">
                    <img src="/catalog/view/theme/loudlemon/images/homepage/reviews/photo6.jpg" alt="Author's photo"/>
                </div>
                <div class="box-review__content">
                    <div class="box-review__content__text">
                        Спасибо ребятам из Loud Lemon за кейс для моей гитары) Только у них оказался в наличии именно тот, который нужен был мне!
                    </div>
                    <div class="flex-box flex-align-center box-review__content__author">
                        <div class="box-review__content__author__line"></div>
                        <div class="box-review__content__author__name">
                            Павел Сочев
                        </div>
                        <div class="box-review__content__author__line"></div>
                    </div>
                </div>
            </div>
        </div>-->
    </div>
</section>
<section class="view-about">
    <div class="view-text">
        <div class="flex-box flex-center box-title">
            <div class="box-title__lines box-title__lines--left wow fadeInLeft" data-wow-delay=".2s" data-wow-duration="1s"></div>
            <h1 class="wow fadeInUp" data-wow-delay=".3s" data-wow-duration="1s">
                Магазин Loud Lemon
            </h1>
            <div class="box-title__lines box-title__lines--right wow fadeInRight" data-wow-delay=".2s" data-wow-duration="1s"></div>
        </div>
        <h2 class="wow zoomIn" data-wow-delay=".5s" data-wow-duration="1s">
            Loud Lemon - это просторный лофт в центре Москвы, где при постоянной влажности, клиентов ждет широкий ассортимент гитар, разных брендов и возрастов. Здесь у нас раритеты и винтажные гитары 60-ых и 70-ых, отстроенные и ухоженные б/у гитары, и абсолютно новенькие инструменты. Единственное чего вы здесь здесь не найдете - это рогатые, многострунные гитары для тяжелых стилей.

            Еще у нас есть бас-гитары, акустические гитары, фолк инструменты, лучшее ламповые усилители, не забываем мы и про расходники: гитарные струны, медиаторы. У нас исключительный выбор оригинальных запчастей и комплектующих для гитар. Мы стараемся удерживать лучшие цены и редкие позиции в наличии. Мы привозим аппарат под заказ,  выкупаем лоты с аукционов, и можем доставить,то что продавец не хочет отправлять в Россию.

            Мы расскажем всю информацию о нашем музыкальном оборудовании, поделимся опытом и поможем определиться с выбором. У вас всегда есть возможность, все послушать, поиграть и сравнить в спокойной обстановке, за чашечкой кофе.

            У нас небольшой коллектив, объединенный любовью к хорошим инструментам, звуку и стилю.

            Так что если будите поблизости - заглядывайте, а мы постараемся «сделать ваш день»!
        </h2>
    </div>
    <a href="/shop/" class="flex-box flex-center view-about__img wow zoomIn" data-wow-delay="1s" data-wow-duration="1s">
        <img src="/catalog/view/theme/loudlemon/images/homepage/img_about.svg" alt="About tagline"/>
    </a>
</section>
<div class="box-brands">
    <div class="js-brands-carousel box-brands__carousel">
        <a href="/index.php?route=product/manufacturer/info&manufacturer_id=11" class="box-brands__carousel__link">
            <img src="/catalog/view/theme/loudlemon/images/homepage/brands/brand1_fender.svg" alt="Brand icon"/>
        </a>
        <a href="/index.php?route=product/manufacturer/info&manufacturer_id=19" class="box-brands__carousel__link">
            <img src="/catalog/view/theme/loudlemon/images/homepage/brands/brand2_gibson.svg" alt="Brand icon"/>
        </a>
        <a href="/index.php?route=product/manufacturer/info&manufacturer_id=12" class="box-brands__carousel__link">
            <img src="/catalog/view/theme/loudlemon/images/homepage/brands/brand3_gretsch.svg" alt="Brand icon"/>
        </a>
        <a href="/index.php?route=product/manufacturer/info&manufacturer_id=63" class="box-brands__carousel__link">
            <img src="/catalog/view/theme/loudlemon/images/homepage/brands/brand4_original_fuzz.svg" alt="Brand icon"/>
        </a>
        <a href="/index.php?route=product/manufacturer/info&manufacturer_id=68" class="box-brands__carousel__link">
            <img src="/catalog/view/theme/loudlemon/images/homepage/brands/brand5_mastery_bridge.svg" alt="Brand icon"/>
        </a>
        <a href="/index.php?route=product/manufacturer/info&manufacturer_id=72" class="box-brands__carousel__link">
            <img src="/catalog/view/theme/loudlemon/images/homepage/brands/brand6_bigsby.svg" alt="Brand icon"/>
        </a>
        <a href="/index.php?route=product/manufacturer/info&manufacturer_id=77" class="box-brands__carousel__link">
            <img src="/catalog/view/theme/loudlemon/images/homepage/brands/brand7_souldier.svg" alt="Brand icon"/>
        </a>
        <a href="/index.php?route=product/manufacturer/info&manufacturer_id=13" class="box-brands__carousel__link">
            <img src="/catalog/view/theme/loudlemon/images/homepage/brands/brand8_danelectro.svg" alt="Brand icon"/>
        </a>
        <a href="/index.php?route=product/manufacturer/info&manufacturer_id=14" class="box-brands__carousel__link">
            <img src="/catalog/view/theme/loudlemon/images/homepage/brands/brand9_rickenbacker.svg" alt="Brand icon"/>
        </a>
        <a href="/index.php?route=product/manufacturer/info&manufacturer_id=71" class="box-brands__carousel__link">
            <img src="/catalog/view/theme/loudlemon/images/homepage/brands/brand10_prs.svg" alt="Brand icon"/>
        </a>
        <a href="/index.php?route=product/manufacturer/info&manufacturer_id=51" class="box-brands__carousel__link">
            <img src="/catalog/view/theme/loudlemon/images/homepage/brands/brand11_hangstrom.svg" alt="Brand icon"/>
        </a>
        <a href="/index.php?route=product/manufacturer/info&manufacturer_id=74" class="box-brands__carousel__link">
            <img src="/catalog/view/theme/loudlemon/images/homepage/brands/brand12_nash.svg" alt="Brand icon"/>
        </a>
        <a href="/index.php?route=product/manufacturer/info&manufacturer_id=70" class="box-brands__carousel__link">
            <img src="/catalog/view/theme/loudlemon/images/homepage/brands/brand13_greco.svg" alt="Brand icon"/>
        </a>
        <a href="/index.php?route=product/manufacturer/info&manufacturer_id=50" class="box-brands__carousel__link">
            <img src="/catalog/view/theme/loudlemon/images/homepage/brands/brand14_g_l.svg" alt="Brand icon"/>
        </a>
        <a href="/index.php?route=product/manufacturer/info&manufacturer_id=15" class="box-brands__carousel__link">
            <img src="/catalog/view/theme/loudlemon/images/homepage/brands/brand15_ernieball-musicman.svg" alt="Brand icon"/>
        </a>
        <a href="/index.php?route=product/manufacturer/info&manufacturer_id=21" class="box-brands__carousel__link">
            <img src="/catalog/view/theme/loudlemon/images/homepage/brands/brand16_duesenberg.svg" alt="Brand icon"/>
        </a>
        <a href="/index.php?route=product/manufacturer/info&manufacturer_id=73" class="box-brands__carousel__link">
            <img src="/catalog/view/theme/loudlemon/images/homepage/brands/brand17_bilt.svg" alt="Brand icon"/>
        </a>
        <a href="/index.php?route=product/manufacturer/info&manufacturer_id=15" class="box-brands__carousel__link">
            <img src="/catalog/view/theme/loudlemon/images/homepage/brands/brand18_ernieball.svg" alt="Brand icon"/>
        </a>
        <a href="/index.php?route=product/manufacturer/info&manufacturer_id=23" class="box-brands__carousel__link">
            <img src="/catalog/view/theme/loudlemon/images/homepage/brands/brand19_daddario.svg" alt="Brand icon"/>
        </a>
    </div>
</div>
</main>
<?php echo $footer;?>
