<header>
	<div class="hamburger-container">
		<!--<div class="animated-button-container__background"></div>-->
		<button class="hamburger hamburger--spin" type="button">
			  <span class="hamburger-box">
				<span class="hamburger-inner"></span>
			  </span>
		</button>
	</div>
	<div class="fixed-buttons">
		<div class="cart-wish-buttons-container">
			<a class="mob-nav-item cart-wish-button login-mobile" href="<?php echo $account; ?>" title="<?php echo $text_account; ?>">
				<i class="svg-login_door"></i> <?php echo $text_account; ?>
			</a>
			<div class="cart-wish-button cart-button"><?php echo $cart; ?></div>
			<a href="<?php echo $wishlist; ?>" id="wishlist-total" title="<?php echo $text_wishlist; ?>" class="cart-wish-button wish-button"><i class="svg-icon_wishlist"></i> <span><?php echo $text_wishlist; ?></span></a>
			<!--<a href="<?php echo $wishlist; ?>" class="cart-wish-button wish-button"><i class="svg-icon_wishlist"></i>(0)</a>-->
		</div>
	</div>
	<div class="toppest delme">
		<div class="container">
			<div class="row toolbar">
				<div class="col-md-4 col-sm-4 col-xs-12 langs">
					<?php echo $language; ?>
				</div>
				<div class="col-md-4 col-sm-4 col-xs-12 search-container">
					<div id="search" class="search">
						<input type="text" name="search" placeholder="<?PHP echo $text_search; ?>">
						<button type="button" class="btn btn-default" name="s">
							<span class="glyphicon glyphicon-search" aria-hidden="true"></span></button>
						<div class="search-line"></div>
					</div>
				</div>
				<div class="col-md-4 col-sm-4 col-xs-12" id="user-links">
					<div class="row" style="margin: 0;">
						<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
							<?php echo $cart; ?></div>
						<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" id='account-drop'>
							<ul class="list-inline">
								<li class="dropdown">
									<a  id="garaj" href="<?php echo $account; ?>" title="<?php echo $text_account; ?>" class="dropdown-toggle" data-toggle="dropdown">

										<?php echo $text_account; ?>
										<span class="caret hidden-xs"></span>
									</a>
									<ul class="dropdown-menu dropdown-menu-right">
										<?php if ($logged) { ?>
										<li><a href="<?php echo $account; ?>"><?php echo $text_account; ?></a></li>
										<li><a href="<?php echo $order; ?>"><?php echo $text_order; ?></a></li>
										<li><a href="<?php echo $logout; ?>"><?php echo $text_logout; ?></a></li>
										<?php } else { ?>
										<li><a href="<?php echo $register; ?>"><?php echo $text_register; ?></a></li>
										<li><a href="<?php echo $login; ?>"><?php echo $text_login; ?></a></li>
										<?php } ?>
									</ul>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="container hidden" id="middle-header">
		<div class="row">
			<div class="col-md-4 col-sm-4 col-xs-12">
				<a href=""><img src="/catalog/view/theme/loudlemon/images/layout/main_logo.svg" alt="Loud-Lemon" id="logo" class="logo-desktop img-responsive"><img src="/catalog/view/theme/loudlemon/images/layout/main_logo_mobile.svg" alt="Loud-Lemon" class="img-responsive logo-mobile"></a>

			</div>
			<div class="col-md-4 col-sm-4 hidden-xs">
				<a href="tel:+74952334442">
					<img src="/catalog/view/theme/loudlemon/images/layout/<?PHP echo $text_mascot; ?>" id="mascot" class="img-responsive">
				</a>
			</div>
			<div class="col-md-4 col-sm-4 col-xs-12">
				<div id="contacts">
	                            <span class="phone hidden-lg hidden-md hidden-sm">
									<?php /* <a href="tel:<?php echo str_replace(array("-", " "), "", $telephone); ?>"><?php echo $telephone; ?></a> */ ?>
									<span class="ya-phone"><?php echo $telephone; ?></span>
								</span>
					<span class="work-hours"><?php  if($curLang == 'ru'){echo 'Часы работы: <span class="brln"></span>';}else {echo 'Working time: <span class="brln"></span>';}?><?php  if($curLang == 'ru'){echo htmlspecialchars_decode($open[0]);} else {echo htmlspecialchars_decode($open[1]);} ?>
	                            </span>
					<span class="adress"><?php  if($curLang == 'ru'){echo htmlspecialchars_decode($address[0]);} else {echo htmlspecialchars_decode($address[1]);} ?></span>
					<span class="mob-addr-open"><?php  if($curLang == 'ru'){echo htmlspecialchars_decode($comment[0]);} else {echo htmlspecialchars_decode($comment[1]);} ?></span>
				</div>
				<div class="SNbar">
					<a href="https://www.instagram.com/loud_lemon/"><img id="cnpic1" src="/catalog/view/theme/loudlemon/images/layout/social_insta.svg" alt="Loud-Lemon В Instagram"></a>
					<a href="https://vk.com/loud_lemon"><img id="cnpic2" src="/catalog/view/theme/loudlemon/images/layout/social_vk.svg" alt="Loud-Lemon на VK"></a>
					<a href=""><img id="cnpic3" src="/catalog/view/theme/loudlemon/images/layout/social_fb.svg" alt="Loud-Lemon на Facebook"></a>
				</div>
			</div>
		</div>
	</div>

	<nav class="navbar navbar-default">
		<div class="container">
			<div class="search-container">
				<div id="search" class="search">
					<input type="text" name="search" id="search-input" placeholder="<?PHP echo $text_search; ?>">
					<button type="button" class="btn btn-default tablet-search-button" name="s">
						<svg xmlns="http://www.w3.org/2000/svg" xml:space="preserve" width="32px" height="32px" version="1.0" style="shape-rendering:geometricPrecision; text-rendering:geometricPrecision; image-rendering:optimizeQuality; fill-rule:evenodd; clip-rule:evenodd" viewBox="0 0 3200 3200" xmlns:xlink="http://www.w3.org/1999/xlink">
									 <g id="Layer_x0020_1">
										 <path class="fil0" d="M137 2885l744 -774c-192,-228 -297,-514 -297,-812 0,-696 567,-1262 1263,-1262 696,0 1262,566 1262,1262 0,696 -566,1263 -1262,1263 -261,0 -510,-79 -723,-229l-750 780c-31,33 -74,51 -119,51 -43,0 -83,-17 -114,-47 -66,-63 -67,-167 -4,-232zm1710 -2519c-514,0 -933,419 -933,933 0,515 419,933 933,933 514,0 933,-418 933,-933 0,-514 -419,-933 -933,-933z"></path>
									 </g>
									</svg>
					</button>
					<div class="search-line"></div>
				</div>
				<div class="search-subtext">
					<?php /* <a href="tel:<?php echo str_replace(array("-", " "), "", $telephone); ?>"><?php echo $telephone; ?></a> */?>
					<span class="ya-phone"><?php echo $telephone; ?></span>
					<span class="address"><?=($curLang == 'ru') ? "Москва, Спартаковский пер., д. 2с1, вход #7" : "Moscow, Spartakovsky 2 str 1, entrance 7";?> <?php  // if($curLang == 'ru'){echo htmlspecialchars_decode($address[0]);} else {echo htmlspecialchars_decode($address[1]);} ?></span>
					<span  class="working-time">(<?=($curLang == 'ru') ? "вт-вс: 12:00-20:00" : "Tu. - Su. 12:00 - 20:00";?>) <?php  //if($curLang == 'ru'){echo htmlspecialchars_decode($open[0]);} else {echo htmlspecialchars_decode($open[1]);} ?></span>
				</div>
			</div>
			<div class="navbar-header">
				<div class="top-logo-container">
					<a href="/"><img src="/catalog/view/theme/loudlemon/images/layout/main_logo_new.svg" alt="Loud Lemon"></a>
				</div>
				<div class="top-tel-container">
					<a href="tel:<?php echo str_replace(array("-", " "), "", $telephone); ?>" data="<?=$telephone?>">
					<svg xmlns="http://www.w3.org/2000/svg" xml:space="preserve" width="32px" height="32px" version="1.0" style="shape-rendering:geometricPrecision; text-rendering:geometricPrecision; image-rendering:optimizeQuality; fill-rule:evenodd; clip-rule:evenodd"
						 viewBox="0 0 3200 3200"
						 xmlns:xlink="http://www.w3.org/1999/xlink">
								 <g id="Layer_x0020_1">
									 <path class="fil0" d="M3023 2223c-213,0 -427,-36 -641,-107 -53,-18 -124,0 -177,35l-391 392c-498,-249 -908,-676 -1174,-1174l391 -392c53,-52 72,-124 36,-177 -54,-196 -90,-408 -90,-622 0,-107 -71,-178 -177,-178l-622 0c-107,0 -178,71 -178,178 0,1671 1351,3022 3023,3022 106,0 177,-71 177,-177l0 -622c0,-107 -71,-178 -177,-178z"/>
								 </g>
								</svg>
					</a>
				</div>

				<form action="/search/">
					<input class="search-mobile-input" type="text">
					<button class="search-mob-button" aria-hidden="true"><svg xmlns="http://www.w3.org/2000/svg" xml:space="preserve" width="32px" height="32px" version="1.0" style="shape-rendering:geometricPrecision; text-rendering:geometricPrecision; image-rendering:optimizeQuality; fill-rule:evenodd; clip-rule:evenodd"
																			  viewBox="0 0 3200 3200"
																			  xmlns:xlink="http://www.w3.org/1999/xlink">
								 <g id="Layer_x0020_1">
									 <path class="fil0" d="M137 2885l744 -774c-192,-228 -297,-514 -297,-812 0,-696 567,-1262 1263,-1262 696,0 1262,566 1262,1262 0,696 -566,1263 -1262,1263 -261,0 -510,-79 -723,-229l-750 780c-31,33 -74,51 -119,51 -43,0 -83,-17 -114,-47 -66,-63 -67,-167 -4,-232zm1710 -2519c-514,0 -933,419 -933,933 0,515 419,933 933,933 514,0 933,-418 933,-933 0,-514 -419,-933 -933,-933z"/>
								 </g>
								</svg></button>
				</form> <?php /*
	                        <div class="mob-cart-container">
				<?php echo $cart; ?>
			</div> <?php */ ?>
			<?php /* ?> <div class="row tablet-nav-row">
				<div class="col-xs-2">
					<div class="animated-button-container animated-button-container__tablet hidden-xs hidden-md hidden-lg">
						<div class="animated-button-container__background"></div>

						<button class="hamburger hamburger--spin" type="button" data-toggle="collapse" data-target=".js-navbar-collapse">
	                                      <span class="hamburger-box">
	                                        <span class="hamburger-inner"></span>
	                                      </span>
						</button>
					</div>
					<span class="menu-text"><?php echo $curLang == 'ru' ? 'МЕНЮ' : 'MENU'; ?></span>
				</div>
				<div class="col-xs-6">
					<div id="search" class="search search-tablet">
						<input type="text" name="search" id="search-input" placeholder="<?PHP echo $text_search; ?>">
						<div class="search-line">
							<button parentName="search-tablet" type="button" class="btn btn-default" name="s">
								<span class="glyphicon glyphicon-search" aria-hidden="true"></span>
							</button>
						</div>
					</div>
				</div>
				<div class="col-xs-2 tablet-cart-container">
					<?php echo $cart; ?>
				</div>
				<div class="col-xs-2 tablet-login-container">
					<a href="<?php echo $account; ?>" title="<?php echo $text_account; ?>" class="login-tablet">
						<?php echo $text_account; ?> <img src="/catalog/view/theme/loudlemon/images/layout/login_door_white.svg" alt="Корзина" class="img-responsive login-tablet-img">
					</a>
				</div>
			</div><?php */ ?>
		</div>


		<div class="navbar-collapse navbar-mob-collapse">
			<div class="row mob-nav">
				<div class="mob-nav-up-menu">
					<?php /* ?>
					<div class="col-xs-6 col-sm-4 mob-menu-left-column">
						<a href="/guitars" class="category-header dddd"><?PHP echo $text_guitars; ?></a>
						<ul class="category-items">
							<?php
		                                            //foreach ($types as $type) {
		                                            //    echo "<li class=\"category-item\"><a href=\"/guitars/?filter=".$type['id']."\">".$type['name']."</a></li>";
							//}
							foreach ($brands as $brand) {
							echo "<li class=\"category-item\"><a href=\"/guitars/?filter=".$brand['id']."\">".$brand['name']."</a></li>";
							} ?>
						</ul>



						<a href="/bass" class="category-header"><?PHP echo $text_вass; ?></a>
						<ul class="category-items">
							<?php
		                                    if($bass_types){
		                                    foreach ($bass_types as $bass_type) {
		                                        echo "<li class=\"category-item\"><a href=\"/bass/?filter=".$bass_type['id']."\">".$bass_type['name']."</a></li>";
							}}
							?>
						</ul>

						<a href="/acoustic" class="category-header"><?PHP echo $curLang == 'ru' ? 'Акустика' : 'Acoustic'; ?></a>
						<ul class="category-items">
							<li class="category-item">
								<a href="/acoustic"><?php echo $curLang == 'ru' ? 'Все' : 'All'; ?></a>
							</li>
						</ul>

						<a href="/effects" class="category-header"><?PHP echo $text_effects; ?></a>
						<ul class="category-items">
							<li class="category-item">
								<a href="/effects"><?php echo $curLang == 'ru' ? 'Все' : 'All'; ?></a>
							</li>
						</ul>

						<a href="/amps" class="category-header"><?PHP echo $text_amps; ?></a>
						<ul class="category-items">
							<?php
		                                    if($amps_watts){
		                                    foreach ($amps_watts as $amps_watt) {
		                                        echo "<li class=\"category-item\"><a href=\"/amps/?filter=".$amps_watt['id']."\">".$amps_watt['name']."</a></li>";
							}}
							?>
						</ul>


						<!--                               <div class="hidden-sm">
                                                          <a class="mob-nav-item" href="/repair"><?PHP echo $text_repair; ?></a>
                                                          <a class="mob-nav-item" href="/delivery"><?PHP echo $text_delivery; ?></a>
                                                          <a class="mob-nav-item" href="/contacts"><?PHP echo $text_contacts; ?></a>
                                                          <a class="mob-nav-item" href="/shop"> <?PHP echo $text_shop; ?> <span class="glyphicon glyphicon-home"></span></a>
                                                      </div>      -->

					</div>
					<div class="col-xs-6 col-sm-4 mob-menu-right-column">
						<a href="/accessories" class="category-header"><?PHP echo $text_accessories; ?></a>
						<ul class="category-items">
							<li class="category-item"><a href="/strings/">
									<?php if($curLang == 'en') {echo "Strings";}else {
		                                            echo "Струны";
		                                         }?>
								</a></li>
							<li class="category-item"><a href="/straps/">
									<?php if($curLang == 'en') {echo "Straps";}else {
		                                            echo "Ремни";
		                                         }?>
								</a></li>
							<li class="category-item"><a href="/cable/">
									<?php if($curLang == 'en') {echo "cable";}else {
		                                            echo "Кабель";
		                                         }?>
								</a></li>
							<li class="category-item"><a href="/parts/">
									<?php if($curLang == 'en') {echo "Parts";}else {
		                                            echo "Запчасти";
		                                         }?>
								</a></li>
							<li class="category-item"><a href="/cases-gig-bags/">
									<?php if($curLang == 'en') {echo "Cases";}else {
		                                            echo "Чехлы";
		                                         }?>
								</a></li>
							<li class="category-item"><a href="/apparel/">
									<?php if($curLang == 'en') {echo "Clothes";}else {
		                                            echo "Одежда";
		                                         }?>
								</a></li>
							<li class="category-item"><a href="/staff/">
									<?php if($curLang == 'en') {echo "Stands";}else {
		                                            echo "Стойки";
		                                         }?>
								</a></li>
							<li class="category-item"><a href="/tuners/">
									<?php if($curLang == 'en') {echo "Tuners";}else {
		                                            echo "Тюнеры";
		                                         }?>
								</a></li>
							<li class="category-item"><a href="/slides-capos/">
									<?php if($curLang == 'en') {echo "Slides/Capo";}else {
		                                            echo "Слайды/Каподастры";
		                                         }?>
								</a></li>
							<li class="category-item"><a href="/picks/">
									<?php if($curLang == 'en') {echo "Picks";}else {
		                                            echo "Медиаторы";
		                                         }?>
								</a></li>

							<li class="category-item"><a href="/pickups/">
									<?php if($curLang == 'en') {echo "Pickups";}else {
		                                            echo "Звукосниматели";
		                                         }?>
								</a></li>

							<li class="category-item"><a href="/care-products/">
									<?php if($curLang == 'en') {echo "Care products";}else {
		                                            echo "Средства по уходу";
		                                         }?>
								</a></li>

							<li class="category-item"><a href="/goods-for-iphone/">
									<?php if($curLang == 'en') {echo "Goods for iPhone";}else {
		                                            echo "Товары для iPhone";
		                                         }?>
								</a></li>

							<li class="category-item"><a href="/guitar-strap-holders/">
									<?php if($curLang == 'en') {echo "Guitar strap holders";}else {
		                                            echo "Держатели для ремней";
		                                         }?>
								</a></li>


							<li class="category-item gifts-li">

								<a href="/gifts/">
									<img src="/catalog/view/theme/loudlemon/images/layout/gifts.svg" class="img-responsive gift-ico"></img>
									<?php if($curLang == 'en') {echo "Gifts";}else {
		                                            echo "Подарки";
		                                         }?>
								</a></li>
						</ul>
					</div>
					<?php */ ?>
				</div>
				<div class="col-xs-12 col-sm-3 mob-sub-menu cart-wish-buttons-container">
					<a class="mob-nav-item cart-wish-button login-mobile" href="<?php echo $account; ?>" title="<?php echo $text_account; ?>">
						<i class="svg-login_door"></i> <?php echo $text_account; ?>
					</a>
					<div class="cart-wish-button cart-button"><?php echo $cart; ?></div>
					<a href="<?php echo $wishlist; ?>" id="wishlist-total" title="<?php echo $text_wishlist; ?>" class="cart-wish-button wish-button"><i class="svg-icon_wishlist"></i> <span><?php echo $text_wishlist; ?></span></a>
					<!--<a href="<?php echo $wishlist; ?>" class="cart-wish-button wish-button"><i class="svg-icon_wishlist"></i>(0)</a>-->
				</div>
				<div class="col-xs-12 col-sm-3 mob-sub-menu mob-main-links">
					<div class="col-xs-4">
						<a class="mob-nav-item-new mob-nav-item-icon" href="/shop/"><span class="glyphicon glyphicon-home"></span><?PHP echo $text_shop; ?></a>
						<a class="mob-nav-item-new" href="/repair/"><?PHP echo $text_repair; ?></a>
						<a class="mob-nav-item-new" href="/warranty/"><?php if($curLang == 'en') {echo "Warranty";}else {
	                                            echo "Гарантии";
	                                         }?></a>


					</div>
					<div class="col-xs-4">
						<a class="mob-nav-item-new" href="/delivery/"><?PHP echo $text_delivery; ?></a>
						<a class="mob-nav-item-new" href="/contacts/"><?PHP echo $text_contacts; ?></a>
						<!--<a class="mob-nav-item login-mobile" href="<?php echo $account; ?>" title="<?php echo $text_account; ?>">-->
						<!--    <?php echo $text_account; ?> <img src="/catalog/view/theme/loudlemon/images/layout/login_door.svg" alt="" class="login-mobile-icon">-->
						<!--</a>-->
					</div>
					<div class="col-xs-4">
						<div class="langs langs-mobile">
							<?php echo $language; ?>
						</div>
					</div>
				</div>
				<div class="col-xs-12 col-sm-3 mob-sub-menu mob-contacts">
					<div class="mobile-contacts">
						<span class="tel ya-phone"><?php echo $telephone; ?></span>
						<?php /* <a class="tel" href="tel:+74952334442">+7 495 233-44-42</a> */?>
						<span>Москва, Спартаковский пер., дом 2с1 вход №7 (ВТ-ВС 12:00-20:00)</span>
					</div>
				</div>
				<div class="col-xs-12 col-sm-3 mob-sub-menu mob-socials">
					<div class="social-bar">
						<a href=""><img id="cnpic3" src="/catalog/view/theme/loudlemon/images/layout/social_fb.svg" alt="Loud-Lemon на Facebook"></a>
						<a href="https://www.instagram.com/loud_lemon/"><img id="cnpic1" src="/catalog/view/theme/loudlemon/images/layout/social_insta.svg" alt="Loud-Lemon В Instagram"></a>
						<a href="https://vk.com/loud_lemon"><img id="cnpic2" src="/catalog/view/theme/loudlemon/images/layout/social_vk.svg" alt="Loud-Lemon на VK"></a>
						<a href="https://www.youtube.com/channel/UC49el0FbI69uG8txZEhHq_w"><img id="cnpic2" src="/catalog/view/theme/loudlemon/images/layout/social_youtube.svg" alt="Loud-Lemon на YouTube"></a>
					</div>
				</div>
			</div>
		</div><!-- /.nav-collapse -->
















		<div class="navbar-tablet-collapse">

			<div class="langs langs-mobile">
				<?php echo $language; ?>
			</div>

			<div class="tablet-main-links">
				<a class="mob-nav-item-new mob-nav-item-icon" href="/shop/"><span class="glyphicon glyphicon-home"></span><?PHP echo $text_shop; ?></a>
				<a class="mob-nav-item-new" href="/repair/"><?PHP echo $text_repair; ?></a>
				<a class="mob-nav-item-new" href="/warranty/"><?php if($curLang == 'en') {echo "Warranty";}else {
	                                    echo "Гарантии";
	                                 }?></a>
				<a class="mob-nav-item-new" href="/delivery/"><?PHP echo $text_delivery; ?></a>
				<a class="mob-nav-item-new" href="/contacts/"><?PHP echo $text_contacts; ?></a>
			</div>

			<div class="cart-wish-buttons-container">
				<a class="mob-nav-item cart-wish-button login-mobile" href="<?php echo $account; ?>" title="<?php echo $text_account; ?>">
					<i class="svg-login_door"></i> <?php echo $text_account; ?>
				</a>
				<div class="cart-wish-button cart-button"><?php echo $cart; ?></div>
				<a href="<?php echo $wishlist; ?>" id="wishlist-total" title="<?php echo $text_wishlist; ?>" class="cart-wish-button wish-button"><i class="svg-icon_wishlist"></i> <span><?php echo $text_wishlist; ?></span></a>
				<!--<a href="<?php echo $wishlist; ?>" class="cart-wish-button wish-button"><i class="svg-icon_wishlist"></i>(0)</a>-->
			</div>

			<div class="mobile-contacts">
				<span class="tel ya-phone"><?php echo $telephone; ?></span>
				<?php /* <a class="tel" href="tel:+74952334442">+7 495 233-44-42</a> */ ?>
				<span>Москва, Спартаковский пер., дом 2с1 вход №7 (ВТ-ВС 12:00-20:00)</span>
			</div>

			<div class="social-bar">
				<a href=""><img id="cnpic3" src="/catalog/view/theme/loudlemon/images/layout/social_fb.svg" alt="Loud-Lemon на Facebook"></a>
				<a href="https://www.instagram.com/loud_lemon/"><img id="cnpic1" src="/catalog/view/theme/loudlemon/images/layout/social_insta.svg" alt="Loud-Lemon В Instagram"></a>
				<a href="https://vk.com/loud_lemon"><img id="cnpic2" src="/catalog/view/theme/loudlemon/images/layout/social_vk.svg" alt="Loud-Lemon на VK"></a>
				<a href="https://www.youtube.com/channel/UC49el0FbI69uG8txZEhHq_w"><img id="cnpic2" src="/catalog/view/theme/loudlemon/images/layout/social_youtube.svg" alt="Loud-Lemon на YouTube"></a>
			</div>


		</div>
		<!--/.navbar-tablet-collapse-->



		<div class="about-etc-menu">
			<ul class="nav navbar-nav <?PHP if($_SERVER['REQUEST_URI'] == '/shop/' || $_SERVER['REQUEST_URI'] == '/repair/'  || $_SERVER['REQUEST_URI'] == '/delivery/' || $_SERVER['REQUEST_URI'] == '/warranty/') echo 'active' ?>">
				<li class="dropdown mini-dropdown">
					<a href="" class="dropdown-toggle" data-toggle="dropdown"><?PHP echo $curLang == 'en' ? 'About' : 'О нас'; ?> <span class="glyphicon glyphicon-triangle-bottom"></span></a>

					<ul class="dropdown-menu mini-dropdown-menu">
						<ul class="mini-nav <?PHP if($_SERVER['REQUEST_URI'] == '/shop/') echo 'active' ?>">
							<li>
								<a href="/shop/"><?PHP echo $text_shop; ?></a>
							</li>
						</ul>
						<ul class="mini-nav <?PHP if($_SERVER['REQUEST_URI'] == '/repair/') echo 'active' ?>">
							<li>
								<a href="/repair/"><?PHP echo $text_repair; ?></a>
							</li>
						</ul>
						<ul class="mini-nav <?PHP if($_SERVER['REQUEST_URI'] == '/delivery/') echo 'active' ?>">
							<li>
								<a href="/delivery/"><?PHP echo $text_delivery; ?></a>
							</li>
						</ul>
						<ul class="mini-nav <?PHP if($_SERVER['REQUEST_URI'] == '/warranty/') echo 'active' ?>">
							<li>
								<a href="/warranty/"><?PHP echo Гарантии; ?></a>
							</li>
						</ul>
					</ul>
				</li>
			</ul>

			<ul class="nav navbar-nav <?PHP if($_SERVER['REQUEST_URI'] == '/contacts/') echo 'active' ?>">
				<li>
					<a href="/contacts/"><?PHP echo $text_contacts; ?></a>
				</li>
			</ul>
		</div>




		<!--/.overflow-menu-->
		</div>

		<div class="header-wrapper"></div>
	</nav>
	<div class="overflow-menu">
		<div class="container">
			<?php
							$menu = $modules->getModules('menu');
			if( count($menu) ) {
			foreach ($menu as $module) {
			echo $module;
			}
			}
			?>
		</div>
		<!--/.container-->

	</div>
	<script>
        if ($(window).width()<768) {
            $('.overflow-menu').appendTo('.mob-nav-up-menu');
        }
	</script>
</header>
