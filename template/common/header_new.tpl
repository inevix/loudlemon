<body class="is-load">
<div class="preloader preloader-default"></div>
<div class="js-content view-wrapper padding-top">
    <div class="js-share flex-box flex-align-center view-share">
        <a href="/guitars/brand/gibson/" class="view-share__content">
		<span class="view-share__content__desc">
			Акция! Все гитары Gibson со скидкой в 10%!
		</span>
            <span class="view-share__content__button">
			<span class="text-label">Перейти</span>
		</span>
        </a>
        <a href="#" class="js-share-close view-share-close"></a>
    </div>
    <aside class="js-header-info flex-box flex-align-center view-header-info">
        <div class="flex-box flex-align-center view-header-info__row">
            <div class="flex-box flex-start flex-align-center view-header-info__row__column view-header-info__row__column--left">
                <a href="tel:<?php echo str_replace(array("-", " "), "", $telephone); ?>" class="view-header-info__phone">
                <?php echo $telephone; ?>
                </a>
                <div class="view-header-info__address">
                    <i class="icon icon-menu-secondary_icon-metro"></i> Красносельская, Спартаковский переулок, дом 2 стр. 1, 12:00-20:00, понедельник выходной
                </div>
            </div>
            <div class="flex-box flex-end flex-align-center view-header-info__row__column view-header-info__row__column--right">
                <menu class="flex-box flex-align-center flex-end info-list">
                    <li class="info-list__item">
                        <a href="/brands/" class="info-list__item__link">
                            Бренды
                        </a>
                    </li>
                    <li class="info-list__item">
                        <a href="#" class="info-list__item__link">
                            Акции
                        </a>
                    </li>
                    <li class="info-list__item">
                        <a href="/shop/" class="info-list__item__link">
                            <i class="icon icon-menu-secondary_icon-shop"></i> Магазин
                        </a>
                    </li>
                    <li class="info-list__item">
                        <a href="/repair/" class="info-list__item__link">
                            Ремонт
                        </a>
                    </li>
                    <li class="info-list__item">
                        <a href="/delivery/" class="info-list__item__link">
                            Доставка и оплата
                        </a>
                    </li>
                    <li class="info-list__item">
                        <a href="/warranty/" class="info-list__item__link">
                            Гарантии
                        </a>
                    </li>
                    <li class="info-list__item">
                        <a href="#" class="info-list__item__link">
                            Блог
                        </a>
                    </li>
                    <li class="info-list__item">
                        <a href="/contacts/" class="info-list__item__link">
                            <i class="icon icon-menu-secondary_icon-contacts"></i> Контакты
                        </a>
                    </li>
                </menu>
            </div>
        </div>
    </aside>
    <header class="js-header view-header padding-top">
        <div class="flex-box view-header__content">
            <a href="/" class="box-logo">
                <img src="/catalog/view/theme/loudlemon/images/homepage/logo.svg" alt="logo"/>
            </a>
            <nav class="header-nav">
                <ul class="flex-box flex-center flex-align-center header-nav-menu">
                    <li class="header-nav-menu__item has-dropdown">
                        <a href="#" class="js-has-dropdown header-nav-menu__item__link">
                            Гитары
                        </a>
                        <nav class="dropdown-nav">
                            <ul class="flex-box dropdown-nav-list">
                                <li class="dropdown-nav-list__item">
                                    <a href="/guitars/" class="all-products-category">
                                    <span class="all-products-category__title">
                                        Все
                                    </span>
                                        <span class="all-products-category__img">
                                        <img src="/catalog/view/theme/loudlemon/images/homepage/header-categories/categories_guitars.svg" alt="Icon guitars"/>
                                    </span>
                                    </a>
                                </li>
                                <li class="dropdown-nav-list__item">
                                    <div class="dropdown-nav-list__item__title">
                                        Бренд
                                    </div>
                                    <div class="flex-box flex-wrap fb-row">
                                        <a href="/guitars/brand/bilt/" class="flex-col-2 dropdown-nav-list__item__link">Bilt</a>
                                        <a href="/guitars/brand/danelectro/" class="flex-col-2 dropdown-nav-list__item__link">Danelectro</a>
                                        <a href="/guitars/brand/duesenberg/" class="flex-col-2 dropdown-nav-list__item__link">Duesenberg</a>
                                        <a href="/guitars/brand/epiphone/" class="flex-col-2 dropdown-nav-list__item__link">Epiphone</a>
                                        <a href="/guitars/brand/fender/" class="flex-col-2 dropdown-nav-list__item__link">Fender</a>
                                        <a href="/guitars/brand/fender-squier/" class="flex-col-2 dropdown-nav-list__item__link">Fender Squier</a>
                                        <a href="/guitars/brand/g-amp-l/" class="flex-col-2 dropdown-nav-list__item__link">G&amp;L</a>
                                        <a href="/guitars/brand/gibson/" class="flex-col-2 dropdown-nav-list__item__link">Gibson</a>
                                        <a href="/guitars/brand/greco/" class="flex-col-2 dropdown-nav-list__item__link">Greco</a>
                                        <a href="/guitars/brand/gretsch/" class="flex-col-2 dropdown-nav-list__item__link">Gretsch</a>
                                        <a href="/guitars/brand/hagstr-m/" class="flex-col-2 dropdown-nav-list__item__link">Hagström</a>
                                        <a href="/guitars/brand/nash/" class="flex-col-2 dropdown-nav-list__item__link">Nash</a>
                                        <a href="/guitars/brand/orville/" class="flex-col-2 dropdown-nav-list__item__link">Orville</a>
                                        <a href="/guitars/brand/prs/" class="flex-col-2 dropdown-nav-list__item__link">PRS</a>
                                        <a href="/guitars/brand/rickenbacker/" class="flex-col-2 dropdown-nav-list__item__link">Rickenbacker</a>
                                        <a href="/guitars/brand/yamaha/" class="flex-col-2 dropdown-nav-list__item__link">Yamaha</a>
                                    </div>
                                </li>
                                <li class="dropdown-nav-list__item type">
                                    <div class="dropdown-nav-list__item__title">
                                        Тип / Форма
                                    </div>
                                    <div class="flex-box flex-wrap fb-row">
                                        <a href="/guitars/forma-korpusa/archtop-amp-hollowbody/" class="flex-col-2 dropdown-nav-list__item__link">Archtop &amp; Hollowbody</a>
                                        <a href="/guitars/forma-korpusa/explorer/" class="flex-col-2 dropdown-nav-list__item__link">Explorer</a>
                                        <a href="/guitars/forma-korpusa/firebird-amp-rd/" class="flex-col-2 dropdown-nav-list__item__link">Firebird</a>
                                        <a href="/guitars/forma-korpusa/v/" class="flex-col-2 dropdown-nav-list__item__link">Flying V</a>
                                        <a href="/guitars/forma-korpusa/jazzmaster-jaguar/" class="flex-col-2 dropdown-nav-list__item__link">Jazzmaster &amp; Jaguar</a>
                                        <a href="/guitars/forma-korpusa/les-paul-amp-double-cut/" class="flex-col-2 dropdown-nav-list__item__link">Les Paul &amp; DC</a>
                                        <a href="/guitars/forma-korpusa/offset/" class="flex-col-2 dropdown-nav-list__item__link">Offset</a>
                                        <a href="/guitars/forma-korpusa/sg/" class="flex-col-2 dropdown-nav-list__item__link">SG</a>
                                        <a href="/guitars/forma-korpusa/stratocaster/" class="flex-col-2 dropdown-nav-list__item__link">Stratocaster</a>
                                        <a href="/guitars/forma-korpusa/telecaster/" class="flex-col-2 dropdown-nav-list__item__link">Telecaster</a>
                                        <a href="/guitars/forma-korpusa/rickenbaker/" class="flex-col-2 dropdown-nav-list__item__link">Rickenbacker</a>
                                        <a href="/guitars/forma-korpusa/bariton/" class="flex-col-2 dropdown-nav-list__item__link">Баритон</a>
                                        <a href="/guitars/forma-korpusa/levorukaja-lh-/" class="flex-col-2 dropdown-nav-list__item__link">Леворукая</a>
                                        <a href="/guitars/forma-korpusa/12-strunnaja/" class="flex-col-2 dropdown-nav-list__item__link">12-струнная</a>
                                    </div>
                                </li>
                                <li class="dropdown-nav-list__item state">
                                    <div class="dropdown-nav-list__item__title">
                                        Состояние
                                    </div>
                                    <div>
                                        <a href="/guitars/sostojanie/novyj/" class="flex-col-2 dropdown-nav-list__item__link">Новый</a>
                                        <a href="/guitars/sostojanie/b-u/" class="flex-col-2 dropdown-nav-list__item__link">Б/у</a>
                                        <a href="/guitars/sostojanie/vintazhnyj/" class="flex-col-2 dropdown-nav-list__item__link">Винтажный</a>
                                    </div>
                                </li>
                            </ul>
                        </nav>
                    </li>
                    <li class="header-nav-menu__item has-dropdown">
                        <a href="#" class="header-nav-menu__item__link">
                            Бас&#8209;гитары
                        </a>
                        <nav class="dropdown-nav">
                            <ul class="flex-box dropdown-nav-list">
                                <li class="dropdown-nav-list__item">
                                    <a href="/bass/" class="all-products-category">
                                    <span class="all-products-category__title">
                                        Все
                                    </span>
                                        <span class="all-products-category__img">
                                        <img src="/catalog/view/theme/loudlemon/images/homepage/header-categories/categories_bass.svg" alt="Icon bass guitars"/>
                                    </span>
                                    </a>
                                </li>
                                <li class="dropdown-nav-list__item">
                                    <div class="dropdown-nav-list__item__title">
                                        Бренд
                                    </div>
                                    <div>
                                        <a href="/bass/brand/fender/" class="dropdown-nav-list__item__link">Fender</a>
                                        <a href="/bass/brand/fender-squier/" class="dropdown-nav-list__item__link">Fender Squier</a>
                                        <a href="/bass/brand/gibson/" class="dropdown-nav-list__item__link">Gibson</a>
                                        <a href="/bass/brand/gretsch/" class="dropdown-nav-list__item__link">Gretsch</a>
                                        <a href="/bass/brand/music-man/" class="dropdown-nav-list__item__link">Music Man</a>
                                    </div>
                                </li>
                                <li class="dropdown-nav-list__item">
                                    <div class="dropdown-nav-list__item__title">
                                        Форма
                                    </div>
                                    <div>
                                        <a href="/bass/forma-korpusa/archtop-amp-hollowbody/" class="dropdown-nav-list__item__link">Archtop &amp; Hollowbody</a>
                                        <a href="/bass/forma-korpusa/jazz-bass/" class="dropdown-nav-list__item__link">Jazz Bass</a>
                                        <a href="/bass/forma-korpusa/les-paul-amp-double-cut/" class="dropdown-nav-list__item__link">Les Paul</a>
                                        <a href="/bass/forma-korpusa/offset/" class="dropdown-nav-list__item__link">Offset</a>
                                        <a href="/bass/forma-korpusa/precision/" class="dropdown-nav-list__item__link">Precision</a>
                                        <a href="/bass/forma-korpusa/sg/" class="dropdown-nav-list__item__link">SG</a>
                                    </div>
                                </li>
                                <li class="dropdown-nav-list__item state">
                                    <div class="dropdown-nav-list__item__title">
                                        Состояние
                                    </div>
                                    <div>
                                        <a href="/bass/sostojanie/novyj/" class="dropdown-nav-list__item__link">
                                            Новый
                                        </a>
                                        <a href="/bass/sostojanie/b-u/" class="dropdown-nav-list__item__link">
                                            Б/у
                                        </a>
                                    </div>
                                </li>
                            </ul>
                        </nav>
                    </li>
                    <li class="header-nav-menu__item has-dropdown">
                        <a href="#" class="header-nav-menu__item__link">
                            Акустика
                        </a>
                        <nav class="dropdown-nav">
                            <ul class="flex-box dropdown-nav-list">
                                <li class="dropdown-nav-list__item">
                                    <a href="/acoustic/" class="all-products-category">
                                    <span class="all-products-category__title">
                                        Все
                                    </span>
                                        <span class="all-products-category__img">
                                        <img src="/catalog/view/theme/loudlemon/images/homepage/header-categories/categories_acoustics.svg" alt="Icon acoustic guitars"/>
                                    </span>
                                    </a>
                                </li>
                                <li class="dropdown-nav-list__item state">
                                    <div class="dropdown-nav-list__item__title">
                                        Категории
                                    </div>
                                    <div>
                                        <a href="/acoustic/kategorija/bandzho/" class="dropdown-nav-list__item__link">Банджо</a>
                                        <a href="/acoustic/kategorija/gitary/" class="dropdown-nav-list__item__link">Гитары</a>
                                        <a href="/acoustic/kategorija/mandoliny/" class="dropdown-nav-list__item__link">Мандолины</a>
                                        <a href="/acoustic/kategorija/ukulele/" class="dropdown-nav-list__item__link">Укулеле</a>
                                    </div>
                                </li>
                                <li class="dropdown-nav-list__item">
                                    <div class="dropdown-nav-list__item__title">
                                        Бренд
                                    </div>
                                    <div>
                                        <a href="/acoustic/brand/fender/" class="dropdown-nav-list__item__link">Fender</a>
                                        <a href="/acoustic/brand/fender-squier/" class="dropdown-nav-list__item__link">Fender Squier</a>
                                        <a href="/acoustic/brand/gretsch/" class="dropdown-nav-list__item__link">Gretsch</a>
                                        <a href="/acoustic/brand/Guild/" class="dropdown-nav-list__item__link">Guild</a>
                                        <a href="/acoustic/brand/j-amp-d/" class="dropdown-nav-list__item__link">J&amp;D</a>
                                        <a href="/acoustic/brand/martin/" class="dropdown-nav-list__item__link">Martin</a>
                                        <a href="/acoustic/brand/takamine/" class="dropdown-nav-list__item__link">Takamine</a>
                                    </div>
                                </li>
                                <li class="dropdown-nav-list__item type">
                                    <div class="dropdown-nav-list__item__title">
                                        Тип
                                    </div>
                                    <div>
                                        <a href="/acoustic/forma-korpusa/12-strunnaja/" class="dropdown-nav-list__item__link">12-струнные</a>
                                        <a href="/acoustic/forma-korpusa/3-4/" class="dropdown-nav-list__item__link">3/4</a>
                                        <a href="/acoustic/forma-korpusa/dzhambo/" class="dropdown-nav-list__item__link">Джамбо</a>
                                        <a href="/acoustic/forma-korpusa/drednout/" class="dropdown-nav-list__item__link">Дредноут</a>
                                        <a href="/acoustic/forma-korpusa/klassika/" class="dropdown-nav-list__item__link">Классика</a>
                                        <a href="/acoustic/forma-korpusa/kontsert/" class="dropdown-nav-list__item__link">Концерт</a>
                                        <a href="/acoustic/forma-korpusa/levorukaja-lh-/" class="dropdown-nav-list__item__link">Леворукая</a>
                                        <a href="/acoustic/forma-korpusa/parlor/" class="dropdown-nav-list__item__link">Парлор</a>
                                        <a href="/acoustic/forma-korpusa/trevel/" class="dropdown-nav-list__item__link">Травел</a>
                                        <a href="/acoustic/forma-korpusa/elektroakustika/" class="dropdown-nav-list__item__link">Электроакустика</a>
                                    </div>
                                </li>
                                <li class="dropdown-nav-list__item state">
                                    <div class="dropdown-nav-list__item__title">
                                        Состояние
                                    </div>
                                    <div>
                                        <a href="/acoustic/sostojanie/novyj/" class="dropdown-nav-list__item__link">Новый</a>
                                        <a href="/acoustic/sostojanie/b-u/" class="dropdown-nav-list__item__link">Б/у</a>
                                    </div>
                                </li>
                            </ul>
                        </nav>
                    </li>
                    <li class="header-nav-menu__item has-dropdown">
                        <a href="#" class="header-nav-menu__item__link">
                            Эффекты
                        </a>
                        <nav class="dropdown-nav">
                            <ul class="flex-box dropdown-nav-list">
                                <li class="dropdown-nav-list__item">
                                    <a href="/effects/" class="all-products-category">
                                    <span class="all-products-category__title">
                                        Все
                                    </span>
                                        <span class="all-products-category__img">
                                        <img src="/catalog/view/theme/loudlemon/images/homepage/header-categories/categories_effects.svg" alt="Icon effects"/>
                                    </span>
                                    </a>
                                </li>
                                <li class="dropdown-nav-list__item">
                                    <div class="dropdown-nav-list__item__title">
                                        Бренд
                                    </div>
                                    <div>
                                        <a href="/effects/brand/boss/" class="dropdown-nav-list__item__link">Boss</a>
                                        <a href="/effects/brand/cioks/" class="dropdown-nav-list__item__link">Cioks</a>
                                        <a href="/effects/brand/dunlop/" class="dropdown-nav-list__item__link">Dunlop</a>
                                        <a href="/effects/brand/earthquaker-devices/" class="dropdown-nav-list__item__link">EarthQuaker Devices</a>
                                        <a href="/effects/brand/ebs/" class="dropdown-nav-list__item__link">EBS</a>
                                        <a href="/effects/brand/electro-harmonix/" class="dropdown-nav-list__item__link">Electro-Harmonix</a>
                                        <a href="/effects/brand/fulltone/" class="dropdown-nav-list__item__link">Fulltone</a>
                                        <a href="/effects/brand/keeley-electronics/" class="dropdown-nav-list__item__link">Keeley Electronics</a>
                                        <a href="/effects/brand/mxr/" class="dropdown-nav-list__item__link">MXR</a>
                                        <a href="/effects/brand/tc-electronic/" class="dropdown-nav-list__item__link">TC Electronic</a>
                                        <a href="/effects/brand/way-huge/" class="dropdown-nav-list__item__link">Way Huge</a>
                                        <a href="/effects/brand/xotic/" class="dropdown-nav-list__item__link">Xotic</a>
                                    </div>
                                </li>
                                <li class="dropdown-nav-list__item type">
                                    <div class="dropdown-nav-list__item__title">
                                        Тип
                                    </div>
                                    <div class="flex-box flex-wrap fb-row">
                                        <a href="/effects/tip-effekta/a-b-switch/" class="flex-col-2 dropdown-nav-list__item__link">A/B Switch</a>
                                        <a href="/effects/tip-effekta/booster/" class="flex-col-2 dropdown-nav-list__item__link">Booster</a>
                                        <a href="/effects/tip-effekta/chorus/" class="flex-col-2 dropdown-nav-list__item__link">Chorus</a>
                                        <a href="/effects/tip-effekta/compressor/" class="flex-col-2 dropdown-nav-list__item__link">Compressor</a>
                                        <a href="/effects/tip-effekta/delay/" class="flex-col-2 dropdown-nav-list__item__link">Delay</a>
                                        <a href="/effects/tip-effekta/distortion/" class="flex-col-2 dropdown-nav-list__item__link">Distortion</a>
                                        <a href="/effects/tip-effekta/envelope-filter/" class="flex-col-2 dropdown-nav-list__item__link">Envelope Filter</a>
                                        <a href="/effects/tip-effekta/equalizer/" class="flex-col-2 dropdown-nav-list__item__link">Equalizer</a>
                                        <a href="/effects/tip-effekta/flanger/" class="flex-col-2 dropdown-nav-list__item__link">Flanger</a>
                                        <a href="/effects/tip-effekta/fuzz/" class="flex-col-2 dropdown-nav-list__item__link">Fuzz</a>
                                        <a href="/effects/tip-effekta/looper/" class="flex-col-2 dropdown-nav-list__item__link">Looper</a>
                                        <a href="/effects/tip-effekta/noise-gate/" class="flex-col-2 dropdown-nav-list__item__link">Noise Gate</a>
                                        <a href="/effects/tip-effekta/octaver/" class="flex-col-2 dropdown-nav-list__item__link">Octaver</a>
                                        <a href="/effects/tip-effekta/overdrive/" class="flex-col-2 dropdown-nav-list__item__link">Overdrive</a>
                                        <a href="/effects/tip-effekta/phaser/" class="flex-col-2 dropdown-nav-list__item__link">Phaser</a>
                                        <a href="/effects/tip-effekta/pitch-shifter/" class="flex-col-2 dropdown-nav-list__item__link">Pitch Shifter</a>
                                        <a href="/effects/tip-effekta/power-supply/" class="flex-col-2 dropdown-nav-list__item__link">Power Supply</a>
                                        <a href="/effects/tip-effekta/preamp/" class="flex-col-2 dropdown-nav-list__item__link">Preamp</a>
                                        <a href="/effects/tip-effekta/reverb/" class="flex-col-2 dropdown-nav-list__item__link">Reverb</a>
                                        <a href="/effects/tip-effekta/synthesizer/" class="flex-col-2 dropdown-nav-list__item__link">Synthesizer</a>
                                        <a href="/effects/tip-effekta/tremolo/" class="flex-col-2 dropdown-nav-list__item__link">Tremolo</a>
                                        <a href="/effects/tip-effekta/tuner/" class="flex-col-2 dropdown-nav-list__item__link">Tuner</a>
                                        <a href="/effects/tip-effekta/wah-wah/" class="flex-col-2 dropdown-nav-list__item__link">Wah-Wah</a>
                                    </div>
                                </li>
                            </ul>
                        </nav>
                    </li>
                    <li class="header-nav-menu__item has-dropdown">
                        <a href="#" class="header-nav-menu__item__link">
                            Усиление
                        </a>
                        <nav class="dropdown-nav">
                            <ul class="flex-box dropdown-nav-list">
                                <li class="dropdown-nav-list__item">
                                    <a href="/amps/" class="all-products-category">
                                    <span class="all-products-category__title">
                                        Все
                                    </span>
                                        <span class="all-products-category__img">
                                        <img src="/catalog/view/theme/loudlemon/images/homepage/header-categories/categories_amps.svg" alt="Icon amps"/>
                                    </span>
                                    </a>
                                </li>
                                <li class="dropdown-nav-list__item">
                                    <div class="dropdown-nav-list__item__title">
                                        Бренд
                                    </div>
                                    <div>
                                        <a href="/amps/brand/fender/" class="dropdown-nav-list__item__link">Fender</a>
                                        <a href="/amps/brand/marshall/" class="dropdown-nav-list__item__link">Marshall</a>
                                        <a href="/amps/brand/mesa-boogie/" class="dropdown-nav-list__item__link">Mesa Boogie</a>
                                        <a href="/amps/brand/vox/" class="dropdown-nav-list__item__link">Vox</a>
                                    </div>
                                </li>
                                <li class="dropdown-nav-list__item">
                                    <div class="dropdown-nav-list__item__title">
                                        Ватты
                                    </div>
                                    <div>
                                        <a href="/amps/vatty/20-vt-i-menshe/" class="dropdown-nav-list__item__link">20 Вт и меньше</a>
                                        <a href="/amps/vatty/20-vt-40-vt/" class="dropdown-nav-list__item__link">20 Вт - 40 Вт</a>
                                        <a href="/amps/vatty/40-vt-i-bolshe/" class="dropdown-nav-list__item__link">40 Вт и больше</a>
                                    </div>
                                </li>
                                <li class="dropdown-nav-list__item">
                                    <div class="dropdown-nav-list__item__title">
                                        Динамик
                                    </div>
                                    <div>
                                        <a href="/amps/dinamik/6-djujmov/" class="dropdown-nav-list__item__link">6 дюймов</a>
                                        <a href="/amps/dinamik/8-djujmov/" class="dropdown-nav-list__item__link">8 дюймов</a>
                                        <a href="/amps/dinamik/10-djujmov/" class="dropdown-nav-list__item__link">10 дюймов</a>
                                        <a href="/amps/dinamik/12-djujmov/" class="dropdown-nav-list__item__link">12 дюймов</a>
                                    </div>
                                </li>
                                <li class="dropdown-nav-list__item state">
                                    <div class="dropdown-nav-list__item__title">
                                        Состояние
                                    </div>
                                    <div>
                                        <a href="/amps/sostojanie/novyj/" class="dropdown-nav-list__item__link">Новый</a>
                                        <a href="/amps/sostojanie/b-u/" class="dropdown-nav-list__item__link">Б/у</a>
                                    </div>
                                </li>
                            </ul>
                        </nav>
                    </li>
                    <li class="header-nav-menu__item has-dropdown">
                        <a href="#" class="header-nav-menu__item__link">
                            Аксессуары
                        </a>
                        <nav class="dropdown-nav">
                            <ul class="flex-box dropdown-nav-list">
                                <li class="dropdown-nav-list__item">
                                    <a href="/accessories/" class="all-products-category">
                                    <span class="all-products-category__title">
                                        Все
                                    </span>
                                        <span class="all-products-category__img">
                                        <img src="/catalog/view/theme/loudlemon/images/homepage/header-categories/categories_accessories.svg" alt="Icon accessories"/>
                                    </span>
                                    </a>
                                </li>
                                <li class="dropdown-nav-list__item type">
                                    <div class="dropdown-nav-list__item__title">
                                        Аксессуары
                                    </div>
                                    <div class="flex-box flex-wrap fb-row">
                                        <a href="https://loud-lemon.com/strings/" class="flex-col-3 dropdown-nav-list__item__link">Cтруны</a>
                                        <a href="https://loud-lemon.com/straps/" class="flex-col-3 dropdown-nav-list__item__link">Ремни</a>
                                        <a href="https://loud-lemon.com/cable/" class="flex-col-3 dropdown-nav-list__item__link">Кабель</a>
                                        <a href="https://loud-lemon.com/parts/" class="flex-col-3 dropdown-nav-list__item__link">Запчасти</a>
                                        <a href="https://loud-lemon.com/cases-gig-bags/" class="flex-col-3 dropdown-nav-list__item__link">Чехлы</a>
                                        <a href="https://loud-lemon.com/apparel/" class="flex-col-3 dropdown-nav-list__item__link">Одежда</a>
                                        <a href="https://loud-lemon.com/staff/" class="flex-col-3 dropdown-nav-list__item__link">Стойки</a>
                                        <a href="https://loud-lemon.com/tuners/" class="flex-col-3 dropdown-nav-list__item__link">Тюнеры</a>
                                        <a href="https://loud-lemon.com/slides-capos/" class="flex-col-3 dropdown-nav-list__item__link">Слайды/Каподастры</a>
                                        <a href="https://loud-lemon.com/picks/" class="flex-col-3 dropdown-nav-list__item__link">Медиаторы</a>
                                        <a href="https://loud-lemon.com/pickups/" class="flex-col-3 dropdown-nav-list__item__link">Звукосниматели</a>
                                        <a href="https://loud-lemon.com/care-products/" class="flex-col-3 dropdown-nav-list__item__link">Средства по уходу</a>
                                        <a href="https://loud-lemon.com/goods-for-iphone/" class="flex-col-3 dropdown-nav-list__item__link">Товары для iPhone</a>
                                        <a href="https://loud-lemon.com/guitar-strap-holders/" class="flex-col-3 dropdown-nav-list__item__link">Держатели для ремня</a>
                                        <a href="https://loud-lemon.com/gifts/" class="flex-col-3 dropdown-nav-list__item__link">Подарки</a>
                                    </div>
                                </li>
                            </ul>
                        </nav>
                    </li>
                </ul>
            </nav>
            <nav class="user-nav">
                <menu class="flex-box flex-end flex-align-center user-nav-menu">
                    <li class="user-nav-menu__item">
                        <a href="#" class="js-toggle-search user-nav-menu__item__link open-search">
                            <i class="icon icon-menu-primary_icon-find"></i>
                            <span class="user-nav-menu__item__link__title">
								Поиск
							</span>
                        </a>
                    </li>
                    <li class="user-nav-menu__item">
                        <?php if ($logged) { ?>
                        <a href="<?php echo $account; ?>" class="user-nav-menu__item__link">
                            <i class="icon icon-menu-primary_icon-login_enter"></i>
                            <span class="user-nav-menu__item__link__title">
								Профиль
							</span>
                        </a>
                        <?php } else { ?>
                        <a href="/my-account/" class="user-nav-menu__item__link">
                            <i class="icon icon-menu-primary_icon-login"></i>
                            <span class="user-nav-menu__item__link__title">
									Вход
								</span>
                        </a>
                        <?php } ?>
                    </li>
                    <li class="user-nav-menu__item">
                        <a href="/cart/" class="user-nav-menu__item__link">
                            <i class="icon icon-menu-primary_icon-cart">
                                <!--<span class="count-products"></span>-->
                            </i>
                            <span class="user-nav-menu__item__link__title">
								Корзина
							</span>
                        </a>
                    </li>
                    <li class="user-nav-menu__item">
                        <a href="/wishlist/" class="user-nav-menu__item__link">
                            <i class="icon icon-menu-primary_icon-whishlist"></i>
                            <span class="user-nav-menu__item__link__title">
								Вишлист
							</span>
                        </a>
                    </li>
                </menu>
            </nav>
        </div>
    </header>
    <header class="js-mobile-header view-mobile-header padding-top">
        <div class="js-mobile-header-content flex-box flex-align-center view-mobile-header__content">
            <a href="#" class="js-box-logo box-logo">
                <img src="/catalog/view/theme/loudlemon/images/homepage/logo.svg" alt="Icon logo"/>
            </a>
            <nav class="flex-box flex-align-center view-mobile-header__content__nav">
                <a href="#" class="js-button-wishlist view-mobile-header__content__nav__link button-wishlist">
                    <i class="icon icon-menu-primary_icon-whishlist"></i>
                </a>
                <a href="#" class="js-button-cart view-mobile-header__content__nav__link button-cart">
                    <i class="icon icon-menu-primary_icon-cart">
                        <span class="count-products">1</span>
                    </i>
                </a>
                <a href="tel:+74952334442" class="view-mobile-header__content__nav__link">
                    <i class="icon icon-menu-primary_icon-phone"></i>
                </a>
                <a href="#" class="js-toggle-search view-mobile-header__content__nav__link button-search">
                    <i class="icon icon-menu-primary_icon-find"></i>
                </a>
            </nav>
            <a href="#" class="js-button-nav button-nav">
				<span class="button-nav__bar">
					<span class="line"></span>
					<span class="line line--center"></span>
					<span class="line"></span>
				</span>
            </a>
            <nav class="js-mobile-header-nav-dropdown dropdown-nav padding-top sl--scrollable">
                <div class="dropdown-nav-content">
                    <menu class="dropdown-nav-list">
                        <li class="js-parent-item dropdown-nav-list__item">
                            <a href="#" data-submenu-open="submenu-guitars" class="js-open-submenu dropdown-nav-list__item__link">
                                Гитары
                            </a>
                        </li>
                        <li class="js-parent-item dropdown-nav-list__item">
                            <a href="#" data-submenu-open="submenu-bassguitars" class="js-open-submenu dropdown-nav-list__item__link">
                                Бас-гитары
                            </a>
                        </li>
                        <li class="js-parent-item dropdown-nav-list__item">
                            <a href="#" data-submenu-open="submenu-acoustic" class="js-open-submenu dropdown-nav-list__item__link">
                                Акустика
                            </a>
                        </li>
                        <li class="js-parent-item dropdown-nav-list__item">
                            <a href="#" data-submenu-open="submenu-effects" class="js-open-submenu dropdown-nav-list__item__link">
                                Эффекты
                            </a>
                        </li>
                        <li class="js-parent-item dropdown-nav-list__item">
                            <a href="#" data-submenu-open="submenu-amps" class="js-open-submenu dropdown-nav-list__item__link">
                                Усиление
                            </a>
                        </li>
                        <li class="js-parent-item dropdown-nav-list__item">
                            <a href="#" data-submenu-open="submenu-accessories" class="js-open-submenu dropdown-nav-list__item__link">
                                Аксессуары
                            </a>
                        </li>
                    </menu>
                    <div class="box-contacts">
                        <div class="flex-box flex-align-center box-phone">
                            <div class="box-phone__line"></div>
                            <a href="tel:+74952334442" class="box-phone__link">
                                +7 495 233-44-42
                            </a>
                            <div class="box-phone__line"></div>
                        </div>
                        <div class="box-address">
                            <p>
                                <i class="icon icon-menu-secondary_icon-metro"></i> Красносельская
                            </p>
                            <p>
                                Спартаковский переулок, дом 2 стр. 1
                            </p>
                            <p>
                                12:00-20:00, понедельник выходной
                            </p>
                        </div>
                    </div>
                    <div class="flex-box flex-center flex-align-center box-stock">
                        <div class="box-stock__img">
                            <img src="/catalog/view/theme/loudlemon/images/homepage/advantages/advantage_3.svg" alt="Stock icon"/>
                        </div>
                        <div class="box-stock__content">
                            <div class="box-stock__content__title">
                                Доставка в день заказа
                            </div>
                            <a href="#" class="box-stock__content__button">
                                Подробнее
                            </a>
                        </div>
                    </div>
                    <a href="#" class="box-map">
					<span class="box-map__img">
						<img src="/catalog/view/theme/loudlemon/images/homepage/img_footer-map.png" alt="Map background"/>
					</span>
                        <span class="flex-box flex-center flex-align-center box-map__title">
						<span class="box-map__title__img">
							<img src="/catalog/view/theme/loudlemon/images/homepage/icons/footer_map-icon.svg" alt="Icon map guitar"/>
						</span>
						<span class="box-map__title__text">
							Мы находимся здесь
						</span>
					</span>
                    </a>
                    <menu class="flex-box flex-center flex-align-center social-list">
                        <li class="social-list__item">
                            <a href="#" class="social-list__item__link">
                                <img src="/catalog/view/theme/loudlemon/images/homepage/icons/social/social_fb.svg" alt="Icon Facebook"/>
                            </a>
                        </li>
                        <li class="social-list__item">
                            <a href="#" class="social-list__item__link">
                                <img src="/catalog/view/theme/loudlemon/images/homepage/icons/social/social_vk.svg" alt="Icon VK"/>
                            </a>
                        </li>
                        <li class="social-list__item">
                            <a href="#" class="social-list__item__link">
                                <img src="/catalog/view/theme/loudlemon/images/homepage/icons/social/social_youtube.svg" alt="Icon Youtube"/>
                            </a>
                        </li>
                        <li class="social-list__item">
                            <a href="#" class="social-list__item__link">
                                <img src="/catalog/view/theme/loudlemon/images/homepage/icons/social/social_insta.svg" alt="Icon Instagram"/>
                            </a>
                        </li>
                    </menu>
                </div>
            </nav>
        </div>
    </header>
