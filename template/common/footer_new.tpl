<footer class="view-footer">
    <div class="flex-box flex-align-center view-footer__content">
        <div class="flex-box flex-center box-auth">
            <a href="/my-account/" class="box-auth__link">
                <span class="box-auth__link__span">
                    <i class="icon icon-menu-primary_icon-login"></i>
                    <span class="box-auth__link__span-title">
                        Вход / регистрация
                    </span>
                </span>
            </a>
        </div>

        <a href="#" class="box-logo">
            <img src="/catalog/view/theme/loudlemon/images/homepage/icons/footer_mainlogo.svg" alt="Icon logo"/>
        </a>
        <div class="box-sourcemap">
            <menu class="flex-box flex-start source-map-list">
                <li class="source-map-list__item">
                    <a href="/brands/" class="source-map-list__item__link">
                        Бренды
                    </a>
                    <a href="#" class="source-map-list__item__link">
                        Акции
                    </a>
                    <a href="#" class="source-map-list__item__link">
                        Магазин
                    </a>
                    <a href="/repair/" class="source-map-list__item__link">
                        Ремонт
                    </a>
                </li>
                <li class="source-map-list__item">
                    <a href="/delivery/" class="source-map-list__item__link">
                        Доставка и оплата
                    </a>
                    <a href="/warranty/" class="source-map-list__item__link">
                        Гарантии
                    </a>
                    <a href="#" class="source-map-list__item__link">
                        Блог
                    </a>
                    <a href="/contacts/" class="source-map-list__item__link">
                        Контакты
                    </a>
                </li>
            </menu>
        </div>
        <div class="box-contacts">
            <div class="flex-box flex-align-center box-phone">
                <div class="box-phone__line"></div>
                <a href="tel:+74952334442" class="box-phone__link">
                    +7 495 233-44-42
                </a>
                <div class="box-phone__line"></div>
            </div>
            <div class="box-address">
                <p>
                    <i class="icon icon-menu-secondary_icon-metro"></i> Красносельская
                </p>
                <p>
                    Спартаковский переулок, дом 2 стр. 1
                </p>
                <p>
                    12:00-20:00, понедельник выходной
                </p>
            </div>
        </div>
        <div class="box-social">
            <menu class="flex-box flex-align-center social-list">
                <li class="social-list__item">
                    <a href="#" class="social-list__item__link">
                        <img src="/catalog/view/theme/loudlemon/images/homepage/icons/social/social_fb.svg" alt="Icon Facebook"/>
                    </a>
                </li>
                <li class="social-list__item">
                    <a href="https://vk.com/loud_lemon" class="social-list__item__link">
                        <img src="/catalog/view/theme/loudlemon/images/homepage/icons/social/social_vk.svg" alt="Icon VK"/>
                    </a>
                </li>
                <li class="social-list__item">
                    <a href="https://www.youtube.com/channel/UC49el0FbI69uG8txZEhHq_w" class="social-list__item__link">
                        <img src="/catalog/view/theme/loudlemon/images/homepage/icons/social/social_youtube.svg" alt="Icon Youtube"/>
                    </a>
                </li>
                <li class="social-list__item">
                    <a href="https://www.instagram.com/loud_lemon/" class="social-list__item__link">
                        <img src="/catalog/view/theme/loudlemon/images/homepage/icons/social/social_insta.svg" alt="Icon Instagram"/>
                    </a>
                </li>
            </menu>
        </div>
        <a href="javascript:void(0)" class="box-map">
            <span class="box-map__img">
                <img src="/catalog/view/theme/loudlemon/images/homepage/img_footer-map.png" alt="Map background"/>
            </span>
            <span class="flex-box flex-center flex-align-center box-map__title">
                <span class="box-map__title__img">
                    <img src="/catalog/view/theme/loudlemon/images/homepage/icons/footer_map-icon.svg" alt="Icon map guitar"/>
                </span>
                <span class="box-map__title__text">
                    Мы находимся здесь
                </span>
            </span>
        </a>
    </div>
    <div class="flex-box flex-align-center view-footer__copyright">
        <div class="box-copyright">
            <a href="#" class="box-copyright__link">LoudLemon</a> &copy; 2014-<?php echo date('Y') ?>
        </div>
        <div class="box-pay-services">
            <img src="/catalog/view/theme/loudlemon/images/homepage/icons/footer_finance-logos.svg" alt="Icons payu services"/>
        </div>
        <div class="flex-box flex-end flex-align-center box-powered">
            <div class="box-powered__title">
                Created by
            </div>
            <a href="https://dreamcloud.digital" class="box-powered__img" target="_blank">
                <img src="/catalog/view/theme/loudlemon/images/homepage/icons/footer_dc-logo.svg" alt="DreamCloud logo"/>
            </a>
        </div>
    </div>
</footer>
<a href="#" class="js-link-to-top link-to-top is-hide">
    <span class="link-to-top__title">Наверх</span>
</a>
<div class="js-search-panel flex-box flex-align-center flex-center box-search sl--scrollable">
    <a href="#" class="js-close-search box-search__close">
        <span class="box-search__close__cross"></span>
        <span class="box-search__close__title">
            Закрыть окно поиска
        </span>
    </a>
    <form class="box-search__form" action="/search/">
        <input type="hidden" name="description" value="true">
        <div class="box-search__form__field">
            <label for="search_request"></label>
            <input id="search_request" name="search" type="text" class="js-search-input control-holder"/>
        </div>
        <div class="flex-box flex-center box-search__form__field">
            <button class="button button--dbl-border button--dbl-border--orange" type="submit" name="s">
                <span class="text-label">
                    Искать <i class="icon icon-menu-primary_icon-find"></i>
                </span>
            </button>
        </div>
    </form>
</div>
<div class="js-fixed-subs fixed-subs padding-top">
    <ul class="js-submenu submenu-guitars submenu-nav-list padding-top sl--scrollable">
        <li class="flex-box flex-align-center submenu-nav-list__item--nav">
            <a href="#" class="js-close-submenu submenu-nav-list__item--nav__link--back">
                Гитары
            </a>
            <a href="/guitars/" class="submenu-nav-list__item--nav__link--all">
                Все
            </a>
        </li>
        <li class="submenu-nav-list__item">
            <div class="submenu-nav-list__item__title">
                Бренд
            </div>
            <div class="flex-box flex-start flex-wrap submenu-nav-list__item__row">
                <a href="/guitars/brand/bilt/" class="flex-col-2 submenu-nav-list__item__link">Bilt</a>
                <a href="/guitars/brand/danelectro/" class="flex-col-2 submenu-nav-list__item__link">Danelectro</a>
                <a href="/guitars/brand/duesenberg/" class="flex-col-2 submenu-nav-list__item__link">Duesenberg</a>
                <a href="/guitars/brand/epiphone/" class="flex-col-2 submenu-nav-list__item__link">Epiphone</a>
                <a href="/guitars/brand/fender/" class="flex-col-2 submenu-nav-list__item__link">Fender</a>
                <a href="/guitars/brand/fender-squier/" class="flex-col-2 submenu-nav-list__item__link">Fender Squier</a>
                <a href="/guitars/brand/g-amp-l/" class="flex-col-2 submenu-nav-list__item__link">G&amp;L</a>
                <a href="/guitars/brand/gibson/" class="flex-col-2 submenu-nav-list__item__link">Gibson</a>
                <a href="/guitars/brand/greco/" class="flex-col-2 submenu-nav-list__item__link">Greco</a>
                <a href="/guitars/brand/gretsch/" class="flex-col-2 submenu-nav-list__item__link">Gretsch</a>
                <a href="/guitars/brand/hagstr-m/" class="flex-col-2 submenu-nav-list__item__link">Hagström</a>
                <a href="/guitars/brand/nash/" class="flex-col-2 submenu-nav-list__item__link">Nash</a>
                <a href="/guitars/brand/orville/" class="flex-col-2 submenu-nav-list__item__link">Orville</a>
                <a href="/guitars/brand/prs/" class="flex-col-2 submenu-nav-list__item__link">PRS</a>
                <a href="/guitars/brand/rickenbacker/" class="flex-col-2 submenu-nav-list__item__link">Rickenbacker</a>
                <a href="/guitars/brand/yamaha/" class="flex-col-2 submenu-nav-list__item__link">Yamaha</a>
            </div>
        </li>
        <li class="submenu-nav-list__item">
            <div class="submenu-nav-list__item__title">
                Тип / Форма
            </div>
            <div class="flex-box flex-start flex-wrap submenu-nav-list__item__row">
                <a href="/guitars/forma-korpusa/archtop-amp-hollowbody/" class="flex-col-2 submenu-nav-list__item__link">Archtop &amp; Hollowbody</a>
                <a href="/guitars/forma-korpusa/explorer/" class="flex-col-2 submenu-nav-list__item__link">Explorer</a>
                <a href="/guitars/forma-korpusa/firebird-amp-rd/" class="flex-col-2 submenu-nav-list__item__link">Firebird</a>
                <a href="/guitars/forma-korpusa/v/" class="flex-col-2 submenu-nav-list__item__link">Flying V</a>
                <a href="/guitars/forma-korpusa/jazzmaster-jaguar/" class="flex-col-2 submenu-nav-list__item__link">Jazzmaster &amp; Jaguar</a>
                <a href="/guitars/forma-korpusa/les-paul-amp-double-cut/" class="flex-col-2 submenu-nav-list__item__link">Les Paul &amp; DC</a>
                <a href="/guitars/forma-korpusa/offset/" class="flex-col-2 submenu-nav-list__item__link">Offset</a>
                <a href="/guitars/forma-korpusa/sg/" class="flex-col-2 submenu-nav-list__item__link">SG</a>
                <a href="/guitars/forma-korpusa/stratocaster/" class="flex-col-2 submenu-nav-list__item__link">Stratocaster</a>
                <a href="/guitars/forma-korpusa/telecaster/" class="flex-col-2 submenu-nav-list__item__link">Telecaster</a>
                <a href="/guitars/forma-korpusa/rickenbaker/" class="flex-col-2 submenu-nav-list__item__link">Rickenbacker</a>
                <a href="/guitars/forma-korpusa/bariton/" class="flex-col-2 submenu-nav-list__item__link">Баритон</a>
                <a href="/guitars/forma-korpusa/levorukaja-lh-/" class="flex-col-2 submenu-nav-list__item__link">Леворукая</a>
                <a href="/guitars/forma-korpusa/12-strunnaja/" class="flex-col-2 submenu-nav-list__item__link">12-струнная</a>
            </div>
        </li>
        <li class="submenu-nav-list__item">
            <div class="submenu-nav-list__item__title">
                Состояние
            </div>
            <div class="submenu-nav-list__item__row">
                <a href="/guitars/sostojanie/novyj/" class="submenu-nav-list__item__link">Новый</a>
                <a href="/guitars/sostojanie/b-u/" class="submenu-nav-list__item__link">Б/у</a>
                <a href="/guitars/sostojanie/vintazhnyj/" class="submenu-nav-list__item__link">Винтажный</a>
            </div>
        </li>
    </ul>
    <ul class="js-submenu submenu-bassguitars submenu-nav-list padding-top sl--scrollable">
        <li class="flex-box flex-align-center submenu-nav-list__item--nav">
            <a href="#" class="js-close-submenu submenu-nav-list__item--nav__link--back">
                Бас-гитары
            </a>
            <a href="/bass/" class="submenu-nav-list__item--nav__link--all">
                Все
            </a>
        </li>
        <li class="submenu-nav-list__item">
            <div class="submenu-nav-list__item__title">
                Бренд
            </div>
            <div class="flex-box flex-start flex-wrap submenu-nav-list__item__row">
                <a href="/bass/brand/fender/" class="flex-col-2 submenu-nav-list__item__link">Fender</a>
                <a href="/bass/brand/fender-squier/" class="flex-col-2 submenu-nav-list__item__link">Fender Squier</a>
                <a href="/bass/brand/gibson/" class="flex-col-2 submenu-nav-list__item__link">Gibson</a>
                <a href="/bass/brand/gretsch/" class="flex-col-2 submenu-nav-list__item__link">Gretsch</a>
                <a href="/bass/brand/music-man/" class="flex-col-2 submenu-nav-list__item__link">Music Man</a>
            </div>
        </li>
        <li class="submenu-nav-list__item">
            <div class="submenu-nav-list__item__title">
                Форма
            </div>
            <div class="flex-box flex-start flex-wrap submenu-nav-list__item__row">
                <a href="/bass/forma-korpusa/archtop-amp-hollowbody/" class="flex-col-2 submenu-nav-list__item__link">Archtop &amp; Hollowbody</a>
                <a href="/bass/forma-korpusa/jazz-bass/" class="flex-col-2 submenu-nav-list__item__link">Jazz Bass</a>
                <a href="/bass/forma-korpusa/les-paul-amp-double-cut/" class="flex-col-2 submenu-nav-list__item__link">Les Paul</a>
                <a href="/bass/forma-korpusa/offset/" class="flex-col-2 submenu-nav-list__item__link">Offset</a>
                <a href="/bass/forma-korpusa/precision/" class="flex-col-2 submenu-nav-list__item__link">Precision</a>
                <a href="/bass/forma-korpusa/sg/" class="flex-col-2 submenu-nav-list__item__link">SG</a>
            </div>
        </li>
        <li class="submenu-nav-list__item">
            <div class="submenu-nav-list__item__title">
                Состояние
            </div>
            <div class="submenu-nav-list__item__row">
                <a href="/bass/sostojanie/novyj/" class="submenu-nav-list__item__link">
                    Новый
                </a>
                <a href="/bass/sostojanie/b-u/" class="submenu-nav-list__item__link">
                    Б/у
                </a>
            </div>
        </li>
    </ul>
    <ul class="js-submenu submenu-acoustic submenu-nav-list padding-top sl--scrollable">
        <li class="flex-box flex-align-center submenu-nav-list__item--nav">
            <a href="#" class="js-close-submenu submenu-nav-list__item--nav__link--back">
                Акустика
            </a>
            <a href="/acoustic/" class="submenu-nav-list__item--nav__link--all">
                Все
            </a>
        </li>
        <li class="submenu-nav-list__item">
            <div class="submenu-nav-list__item__title">
                Категории
            </div>
            <div class="flex-box flex-start flex-wrap submenu-nav-list__item__row">
                <a href="/acoustic/kategorija/bandzho/" class="flex-col-2 submenu-nav-list__item__link">Банджо</a>
                <a href="/acoustic/kategorija/gitary/" class="flex-col-2 submenu-nav-list__item__link">Гитары</a>
                <a href="/acoustic/kategorija/mandoliny/" class="flex-col-2 submenu-nav-list__item__link">Мандолины</a>
                <a href="/acoustic/kategorija/ukulele/" class="flex-col-2 submenu-nav-list__item__link">Укулеле</a>
            </div>
        </li>
        <li class="submenu-nav-list__item">
            <div class="submenu-nav-list__item__title">
                Бренд
            </div>
            <div class="flex-box flex-start flex-wrap submenu-nav-list__item__row">
                <a href="/acoustic/brand/fender/" class="flex-col-2 submenu-nav-list__item__link">Fender</a>
                <a href="/acoustic/brand/fender-squier/" class="flex-col-2 submenu-nav-list__item__link">Fender Squier</a>
                <a href="/acoustic/brand/gretsch/" class="flex-col-2 submenu-nav-list__item__link">Gretsch</a>
                <a href="/acoustic/brand/Guild/" class="flex-col-2 submenu-nav-list__item__link">Guild</a>
                <a href="/acoustic/brand/j-amp-d/" class="flex-col-2 submenu-nav-list__item__link">J&amp;D</a>
                <a href="/acoustic/brand/martin/" class="flex-col-2 submenu-nav-list__item__link">Martin</a>
                <a href="/acoustic/brand/takamine/" class="flex-col-2 submenu-nav-list__item__link">Takamine</a>
            </div>
        </li>
        <li class="submenu-nav-list__item">
            <div class="submenu-nav-list__item__title">
                Тип
            </div>
            <div class="submenu-nav-list__item__row">
                <a href="/acoustic/forma-korpusa/12-strunnaja/" class="submenu-nav-list__item__link">12-струнные</a>
                <a href="/acoustic/forma-korpusa/3-4/" class="submenu-nav-list__item__link">3/4</a>
                <a href="/acoustic/forma-korpusa/dzhambo/" class="submenu-nav-list__item__link">Джамбо</a>
                <a href="/acoustic/forma-korpusa/drednout/" class="submenu-nav-list__item__link">Дредноут</a>
                <a href="/acoustic/forma-korpusa/klassika/" class="submenu-nav-list__item__link">Классика</a>
                <a href="/acoustic/forma-korpusa/kontsert/" class="submenu-nav-list__item__link">Концерт</a>
                <a href="/acoustic/forma-korpusa/levorukaja-lh-/" class="submenu-nav-list__item__link">Леворукая</a>
                <a href="/acoustic/forma-korpusa/parlor/" class="submenu-nav-list__item__link">Парлор</a>
                <a href="/acoustic/forma-korpusa/trevel/" class="submenu-nav-list__item__link">Травел</a>
                <a href="/acoustic/forma-korpusa/elektroakustika/" class="submenu-nav-list__item__link">Электроакустика</a>
            </div>
        </li>
        <li class="submenu-nav-list__item">
            <div class="submenu-nav-list__item__title">
                Состояние
            </div>
            <div class="submenu-nav-list__item__row">
                <a href="/acoustic/sostojanie/novyj/" class="submenu-nav-list__item__link">Новый</a>
                <a href="/acoustic/sostojanie/b-u/" class="submenu-nav-list__item__link">Б/у</a>
            </div>
        </li>
    </ul>
    <ul class="js-submenu submenu-effects submenu-nav-list padding-top sl--scrollable">
        <li class="flex-box flex-align-center submenu-nav-list__item--nav">
            <a href="#" class="js-close-submenu submenu-nav-list__item--nav__link--back">
                Эффекты
            </a>
            <a href="/effects/" class="submenu-nav-list__item--nav__link--all">
                Все
            </a>
        </li>
        <li class="submenu-nav-list__item">
            <div class="submenu-nav-list__item__title">
                Бренд
            </div>
            <div class="flex-box flex-start flex-wrap submenu-nav-list__item__row">
                <a href="/effects/brand/boss/" class="flex-col-2 submenu-nav-list__item__link">Boss</a>
                <a href="/effects/brand/cioks/" class="flex-col-2 submenu-nav-list__item__link">Cioks</a>
                <a href="/effects/brand/dunlop/" class="flex-col-2 submenu-nav-list__item__link">Dunlop</a>
                <a href="/effects/brand/earthquaker-devices/" class="flex-col-2 submenu-nav-list__item__link">EarthQuaker Devices</a>
                <a href="/effects/brand/ebs/" class="flex-col-2 submenu-nav-list__item__link">EBS</a>
                <a href="/effects/brand/electro-harmonix/" class="flex-col-2 submenu-nav-list__item__link">Electro-Harmonix</a>
                <a href="/effects/brand/fulltone/" class="flex-col-2 submenu-nav-list__item__link">Fulltone</a>
                <a href="/effects/brand/keeley-electronics/" class="flex-col-2 submenu-nav-list__item__link">Keeley Electronics</a>
                <a href="/effects/brand/mxr/" class="flex-col-2 submenu-nav-list__item__link">MXR</a>
                <a href="/effects/brand/tc-electronic/" class="flex-col-2 submenu-nav-list__item__link">TC Electronic</a>
                <a href="/effects/brand/way-huge/" class="flex-col-2 submenu-nav-list__item__link">Way Huge</a>
                <a href="/effects/brand/xotic/" class="flex-col-2 submenu-nav-list__item__link">Xotic</a>
            </div>
        </li>
        <li class="submenu-nav-list__item">
            <div class="submenu-nav-list__item__title">
                Тип
            </div>
            <div class="flex-box flex-start flex-wrap submenu-nav-list__item__row">
                <a href="/effects/tip-effekta/a-b-switch/" class="flex-col-2 submenu-nav-list__item__link">A/B Switch</a>
                <a href="/effects/tip-effekta/booster/" class="flex-col-2 submenu-nav-list__item__link">Booster</a>
                <a href="/effects/tip-effekta/chorus/" class="flex-col-2 submenu-nav-list__item__link">Chorus</a>
                <a href="/effects/tip-effekta/compressor/" class="flex-col-2 submenu-nav-list__item__link">Compressor</a>
                <a href="/effects/tip-effekta/delay/" class="flex-col-2 submenu-nav-list__item__link">Delay</a>
                <a href="/effects/tip-effekta/distortion/" class="flex-col-2 submenu-nav-list__item__link">Distortion</a>
                <a href="/effects/tip-effekta/envelope-filter/" class="flex-col-2 submenu-nav-list__item__link">Envelope Filter</a>
                <a href="/effects/tip-effekta/equalizer/" class="flex-col-2 submenu-nav-list__item__link">Equalizer</a>
                <a href="/effects/tip-effekta/flanger/" class="flex-col-2 submenu-nav-list__item__link">Flanger</a>
                <a href="/effects/tip-effekta/fuzz/" class="flex-col-2 submenu-nav-list__item__link">Fuzz</a>
                <a href="/effects/tip-effekta/looper/" class="flex-col-2 submenu-nav-list__item__link">Looper</a>
                <a href="/effects/tip-effekta/noise-gate/" class="flex-col-2 submenu-nav-list__item__link">Noise Gate</a>
                <a href="/effects/tip-effekta/octaver/" class="flex-col-2 submenu-nav-list__item__link">Octaver</a>
                <a href="/effects/tip-effekta/overdrive/" class="flex-col-2 submenu-nav-list__item__link">Overdrive</a>
                <a href="/effects/tip-effekta/phaser/" class="flex-col-2 submenu-nav-list__item__link">Phaser</a>
                <a href="/effects/tip-effekta/pitch-shifter/" class="flex-col-2 submenu-nav-list__item__link">Pitch Shifter</a>
                <a href="/effects/tip-effekta/power-supply/" class="flex-col-2 submenu-nav-list__item__link">Power Supply</a>
                <a href="/effects/tip-effekta/preamp/" class="flex-col-2 submenu-nav-list__item__link">Preamp</a>
                <a href="/effects/tip-effekta/reverb/" class="flex-col-2 submenu-nav-list__item__link">Reverb</a>
                <a href="/effects/tip-effekta/synthesizer/" class="flex-col-2 submenu-nav-list__item__link">Synthesizer</a>
                <a href="/effects/tip-effekta/tremolo/" class="flex-col-2 submenu-nav-list__item__link">Tremolo</a>
                <a href="/effects/tip-effekta/tuner/" class="flex-col-2 submenu-nav-list__item__link">Tuner</a>
                <a href="/effects/tip-effekta/wah-wah/" class="flex-col-2 submenu-nav-list__item__link">Wah-Wah</a>
            </div>
        </li>
    </ul>
    <ul class="js-submenu submenu-amps submenu-nav-list padding-top sl--scrollable">
        <li class="flex-box flex-align-center submenu-nav-list__item--nav">
            <a href="#" class="js-close-submenu submenu-nav-list__item--nav__link--back">
                Усиление
            </a>
            <a href="/amps/" class="submenu-nav-list__item--nav__link--all">
                Все
            </a>
        </li>
        <li class="submenu-nav-list__item">
            <div class="submenu-nav-list__item__title">
                Бренд
            </div>
            <div class="flex-box flex-start flex-wrap submenu-nav-list__item__row">
                <a href="/amps/brand/fender/" class="submenu-nav-list__item__link">Fender</a>
                <a href="/amps/brand/marshall/" class="submenu-nav-list__item__link">Marshall</a>
                <a href="/amps/brand/mesa-boogie/" class="submenu-nav-list__item__link">Mesa Boogie</a>
                <a href="/amps/brand/vox/" class="submenu-nav-list__item__link">Vox</a>
            </div>
        </li>
        <li class="submenu-nav-list__item">
            <div class="submenu-nav-list__item__title">
                Ватты
            </div>
            <div class="flex-box flex-start flex-wrap submenu-nav-list__item__row">
                <a href="/amps/vatty/20-vt-i-menshe/" class="submenu-nav-list__item__link">20 Вт и меньше</a>
                <a href="/amps/vatty/20-vt-40-vt/" class="submenu-nav-list__item__link">20 Вт - 40 Вт</a>
                <a href="/amps/vatty/40-vt-i-bolshe/" class="submenu-nav-list__item__link">40 Вт и больше</a>
            </div>
        </li>
        <li class="submenu-nav-list__item">
            <div class="submenu-nav-list__item__title">
                Динамик
            </div>
            <div class="submenu-nav-list__item__row">
                <a href="/amps/dinamik/6-djujmov/" class="submenu-nav-list__item__link">6 дюймов</a>
                <a href="/amps/dinamik/8-djujmov/" class="submenu-nav-list__item__link">8 дюймов</a>
                <a href="/amps/dinamik/10-djujmov/" class="submenu-nav-list__item__link">10 дюймов</a>
                <a href="/amps/dinamik/12-djujmov/" class="submenu-nav-list__item__link">12 дюймов</a>
            </div>
        </li>
        <li class="submenu-nav-list__item">
            <div class="submenu-nav-list__item__title">
                Состояние
            </div>
            <div class="submenu-nav-list__item__row">
                <a href="/amps/sostojanie/novyj/" class="submenu-nav-list__item__link">Новый</a>
                <a href="/amps/sostojanie/b-u/" class="submenu-nav-list__item__link">Б/у</a>
            </div>
        </li>
    </ul>
    <ul class="js-submenu submenu-accessories submenu-nav-list padding-top sl--scrollable">
        <li class="flex-box flex-align-center submenu-nav-list__item--nav">
            <a href="#" class="js-close-submenu submenu-nav-list__item--nav__link--back">
                Аксессуары
            </a>
            <a href="/accessories/" class="submenu-nav-list__item--nav__link--all">
                Все
            </a>
        </li>
        <li class="submenu-nav-list__item">
            <div class="submenu-nav-list__item__title">
                Аксессуары
            </div>
            <div class="flex-box flex-start flex-wrap submenu-nav-list__item__row">
                <a href="https://loud-lemon.com/strings/" class="flex-col-2 submenu-nav-list__item__link">Cтруны</a>
                <a href="https://loud-lemon.com/straps/" class="flex-col-2 submenu-nav-list__item__link">Ремни</a>
                <a href="https://loud-lemon.com/cable/" class="flex-col-2 submenu-nav-list__item__link">Кабель</a>
                <a href="https://loud-lemon.com/parts/" class="flex-col-2 submenu-nav-list__item__link">Запчасти</a>
                <a href="https://loud-lemon.com/cases-gig-bags/" class="flex-col-2 submenu-nav-list__item__link">Чехлы</a>
                <a href="https://loud-lemon.com/apparel/" class="flex-col-2 submenu-nav-list__item__link">Одежда</a>
                <a href="https://loud-lemon.com/staff/" class="flex-col-2 submenu-nav-list__item__link">Стойки</a>
                <a href="https://loud-lemon.com/tuners/" class="flex-col-2 submenu-nav-list__item__link">Тюнеры</a>
                <a href="https://loud-lemon.com/slides-capos/" class="flex-col-2 submenu-nav-list__item__link">Слайды/Каподастры</a>
                <a href="https://loud-lemon.com/picks/" class="flex-col-2 submenu-nav-list__item__link">Медиаторы</a>
                <a href="https://loud-lemon.com/pickups/" class="flex-col-2 submenu-nav-list__item__link">Звукосниматели</a>
                <a href="https://loud-lemon.com/care-products/" class="flex-col-2 submenu-nav-list__item__link">Средства по уходу</a>
                <a href="https://loud-lemon.com/goods-for-iphone/" class="flex-col-2 submenu-nav-list__item__link">Товары для iPhone</a>
                <a href="https://loud-lemon.com/guitar-strap-holders/" class="flex-col-2 submenu-nav-list__item__link">Держатели для ремня</a>
                <a href="https://loud-lemon.com/gifts/" class="flex-col-2 submenu-nav-list__item__link">Подарки</a>
            </div>
        </li>
    </ul>
</div>