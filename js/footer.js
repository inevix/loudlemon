//// FIRST PART ///////////////


 $(window).on('load', function () {
          $('#pmin').mask("### ### ###", {reverse: true});
          $('#pmax').mask("### ### ###", {reverse: true});
          
          //hidePreloader(); //see header.tpl for this function

          //setTimeout(function(){
          //  if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
          //    if ($('.product-row').offset()) {
          //      $(document).scrollTop($('.product-row').offset().top);
          //    }
          //  }
          //}, 350)

      });
      $(document).ready(function(){

        navigator.sayswho= (function(){
            var ua= navigator.userAgent, tem,
            M= ua.match(/(opera|chrome|safari|firefox|msie|trident(?=\/))\/?\s*(\d+)/i) || [];
            if(/trident/i.test(M[1])){
                tem=  /\brv[ :]+(\d+)/g.exec(ua) || [];
                return 'IE '+(tem[1] || '');
            }
            if(M[1]=== 'Chrome'){
                tem= ua.match(/\b(OPR|Edge)\/(\d+)/);
                if(tem!= null) return tem.slice(1).join(' ').replace('OPR', 'Opera');
            }
            M= M[2]? [M[1], M[2]]: [navigator.appName, navigator.appVersion, '-?'];
            if((tem= ua.match(/version\/(\d+)/i))!= null) M.splice(1, 1, tem[1]);
            return M;
        })();

        var ua = navigator.sayswho;
        if (ua[0] == 'Safari' && ua[1] < 10) {
          $('#product_char .price-title').each(function(){
            $(this).addClass('safari');
          });
          $('.price-title').each(function(){
            $(this).addClass('safari');
          });
          $('.price-old').each(function(){
            $(this).addClass('safari');
          });
          $('.price-new').each(function(){
            $(this).addClass('safari');
          });
        }

        if (ua[0] == 'Safari') {
          $('.mob-nav>.col-xs-6.col-sm-4').addClass('safariHeight');
        }


        function stickyFilter() {
          if(window.innerWidth > 767) {
            if( ua[0] == 'Safari' ) {
              $('.filter-col').addClass('sticky-filter-safari');
            }
            $('.filter-col').addClass('sticky-filter');
            document.getElementsByClassName("filter-col").className += " sticky-filter";
            $('.filter-col').Stickyfill();
          } else {
            Stickyfill.kill();
            $('.filter-col').removeClass('sticky-filter');
            $('.filter-col').removeClass('sticky-filter-safari');
          }
        }
        // $('.filter-col').Stickyfill();

        // $( '.filter-col' ).fixedsticky();


        stickyFilter();
        $(window).on('resize orientationchange', function() {
          stickyFilter()
        });

        $(document).on('click', '.filter-title', function(){
          Stickyfill.rebuild();
        });

        if (ua[0] == 'Firefox') {
          $('.filter-col.sticky-filter').css('margin-top', 0)
        }

        if (ua[0] != 'Safari') {
          var up = $("#scroll"),
              bottomCSS = up.css("bottom"),
              footerOffset = $("footer").offset();

          var upPosition = function() {
            var fot = $(document).height() - $('footer').height();
            var windowHeight = $(window).height(),
            scrTop = $(this).scrollTop();

            var upOffset = scrTop + $(window).height() - fot;
            if (upOffset > 0) {
              if (ua[0] == 'Edge' || ua[0] == 'IE' || ua[0] == 'I' || ua[0] == 'E') {
                up.css({ bottom: ($('footer').height() + 2) + 'px', position: 'absolute' });
              } else {
                up.css({ bottom: ($('footer').height() + 1) + 'px', position: 'absolute' });
              }
            } else {
              up.css({ bottom: bottomCSS, position: 'fixed' });
            }
          }
          $(document).ready(function(){upPosition()});
          $(document).scroll(function () {
            upPosition();
          });
          $(window).resize(function(){
            footerOffset = $("footer").offset();
            upPosition();
          });
        } else {
          if ($(document).height() > ($(window).height() * 2)) {
            $("#scroll").addClass('stickySafari');
          }
          //$('footer').css('margin-top', '0');
          function prevMargin( elem ) {
            // console.log(elem.prev().is('script'));
            if (elem.prev().is('script')) {
              prevMargin(elem.prev())
            } else {
              if ($('header').next().height() < 500) {
                elem.prev().css('margin-bottom', '0px');
              } else {
                elem.prev().css('margin-bottom', '-70px');
              }
            }
          }
          $('#scroll').css('margin-left', ($(window).width() - 100));
          $(window).resize(function(){
            $('#scroll').css('margin-left', ($(window).width() - 100));
          });
          //prevMargin($('#scroll'));

        }
        if ($(document).height() > ($(window).height() * 1.8)) {
          if ($(document).scrollTop() < $(window).height()) { $('#scroll').css('display', 'none'); } else { $('#scroll').fadeIn(); }
          $(document).scroll(function () {
            $(window).height() < $(this).scrollTop() ? $('#scroll').fadeIn() : $('#scroll').fadeOut();
          });
        } else {
          $('#scroll').css('display', 'none');
        }

        $("#scroll img").click(function () {$('body,html').animate({scrollTop: 0}, 400); return false;});
        if( ! /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
          $("#scroll img").hover(function () {$(this).attr('src', '/catalog/view/theme/loudlemon/images/layout/up/up_button_hover.svg')}, function () {$(this).attr('src', '/catalog/view/theme/loudlemon/images/layout/up/up_button.svg')});
        }



var ress2 = function() {
  var footerMT = $(window).outerHeight() - $('body').outerHeight();
 // if ($(window).width() <= 485) {
 //   $('#footer-payments1').addClass('hidden-xs');
 //   $('#footer-payments2').removeClass('hidden-xs');
 //   $('.hidden-sxs').addClass('hidden-xs');
 //   $('.nothidden-sxs').removeClass('hidden-xs');
 // } else {
 //    $('#footer-payments2').addClass('hidden-xs');
 //    $('#footer-payments1').removeClass('hidden-xs');
 //    $('.hidden-sxs').removeClass('hidden-xs');
 //    $('.nothidden-sxs').addClass('hidden-xs');
 // }https://developer.mozilla.org/docs/Mozilla/Performance/ScrollLinkedEffects
 if ($(window).width() > 485 && $(window).width() < 768) {
  	$('#footer-menu>div.col-xs-6').removeClass('col-xs-6').addClass('col-xs-4');
 } else {
    $('#footer-menu>div.col-xs-4').removeClass('col-xs-4').addClass('col-xs-6');
 }
 if (footerMT > 1) {
  $('footer').css('margin-top', parseInt($('footer').css('margin-top')) + footerMT + 'px');
 }
}
// ress2();
if (ua[0] != 'Safari') {
  setTimeout(ress2, 500);
  $(window).resize(function(){
    ress2();
  });
}



// $("#slider-price").slider({
//   min: 0,
//   max: 200000,
//   values: [<?PHP echo $pmin; ?>, <?PHP echo $pmax; ?>],
//   range: true,
//   stop: function(event, ui) {
//     jQuery("input#pmin").val(jQuery("#slider-price").slider("values",0));
//     jQuery("input#pmax").val(jQuery("#slider-price").slider("values",1));
//     },
//     slide: function(event, ui){
//     jQuery("input#pmin").val(jQuery("#slider-price").slider("values",0));
//     jQuery("input#pmax").val(jQuery("#slider-price").slider("values",1));
//     }
// });

$("input#pmin").change(function(){
  var value1=jQuery("input#pmin").val();
  var value2=jQuery("input#pmax").val();

    if(parseInt(value1) > parseInt(value2)){
    value1 = value2;
    jQuery("input#pmin").val(value1);
  }
  jQuery("#slider-price").slider("values",0,value1);
});


$("input#pmax").change(function(){
  var value1=jQuery("input#pmin").val();
  var value2=jQuery("input#pmax").val();

  if (value2 > 200000) { value2 = 200000; jQuery("input#pmax").val(200000)}

  if(parseInt(value1) > parseInt(value2)){
    value2 = value1;
    jQuery("input#pmax").val(value2);
  }
  jQuery("#slider-price").slider("values",1,value2);
});

});




////////////// SECOND PART //////////////



 $('#bad-button').on('click', function() {
            if($( window ).width() < 991){
              $(location).attr("href", '/cart/');
            }
});


///////////////// THIRD PART ///////////////////

              $( document ).ready(function() {
                  $('#slider3').height( $('#product_main_img').height()-68 );
                  $('.thumbelina-but').width( $('img.thumb_click').width() +2 );
                });
            $( window ).resize(function() {
                $('#slider3').height( 780 );
                $('#slider3').height( $('#product_main_img').height()-68 );
                $('.thumbelina-but').width( $('img.thumb_click').width() + 2 );
            });
            
            
///////////////////// 4TH PART //////////////////

   var $div = $("li.mega-dropdown");
        var $div2 = $("li.mini-dropdown");
        var observer = new MutationObserver(function(mutations) {
            mutations.forEach(function(mutation) {
                if (mutation.attributeName === "class") {
                    var attributeValue = $(mutation.target).prop(mutation.attributeName);
                    $(mutation.target).parent().parent().parent().parent().toggleClass("lemon-nav");
                    $(mutation.target).parent().toggleClass("opened-menu");
                    $(mutation.target).closest('.overflow-menu').toggleClass('open');
                }
            });
        });
/*
        observer.observe($div[0],  {
            attributes: true
        });
        observer.observe($div[1],  {
            attributes: true
        });
        observer.observe($div[2],  {
            attributes: true
        });
        observer.observe($div[3],  {
            attributes: true
        });
        observer.observe($div[4],  {
            attributes: true
        });
        observer.observe($div[5],  {
            attributes: true
        });
        observer.observe($div2[0],  {
            attributes: true
        });
 */      
        
//////////////////////// 5TH PART ////////////////////////
    $(document).ready(function() {
        // Optimalisation: Store the references outside the event handler:
        var $window = $(window);

        function checkWidth() {
            var windowsize = $window.width();
            if (windowsize < 991) {
                $("#bad-button.dropdown-toggle").removeAttr('data-toggle');
                $("#bad-button.dropdown-toggle").click(function(){
                	console.log('Go cart!');
                	window.location.href = "/cart/";
                });
                
            }else{
                 $("#bad-button.dropdown-toggle").attr('data-toggle', 'dropdown');
            }
        }
        // Execute on load
        checkWidth();
        // Bind event listener
        $(window).resize(checkWidth);
    });

 ////////////////// 5.5TH PART /////////////////////

        $('#cnpic1').hover(
      function(){ $(this).attr("src", "/catalog/view/theme/loudlemon/images/layout/social_insta_hover.svg") },
      function(){ $(this).attr("src", "/catalog/view/theme/loudlemon/images/layout/social_insta.svg") }
);
        $('#cnpic2').hover(
      function(){ $(this).attr("src", "/catalog/view/theme/loudlemon/images/layout/social_vk_hover.svg") },
      function(){ $(this).attr("src", "/catalog/view/theme/loudlemon/images/layout/social_vk.svg") }
);
        $('#cnpic3').hover(
      function(){ $(this).attr("src", "/catalog/view/theme/loudlemon/images/layout/social_fb_hover.svg") },
      function(){ $(this).attr("src", "/catalog/view/theme/loudlemon/images/layout/social_fb.svg") }
);

                $('#nav-pic-guitars').hover(
      function(){ $(this).attr("src", "/catalog/view/theme/loudlemon/images/layout/navbar/guitars_hover.svg") },
      function(){ $(this).attr("src", "/catalog/view/theme/loudlemon/images/layout/navbar/guitars.svg") }
);

                $('#nav-pic-acoustic').hover(
      function(){ $(this).attr("src", "/catalog/view/theme/loudlemon/images/layout/navbar/acoustic_hover.svg") },
      function(){ $(this).attr("src", "/catalog/view/theme/loudlemon/images/layout/navbar/acoustic.svg") });

                $('#nav-pic-bass').hover(
      function(){ $(this).attr("src", "/catalog/view/theme/loudlemon/images/layout/navbar/bass_hover.svg") },
      function(){ $(this).attr("src", "/catalog/view/theme/loudlemon/images/layout/navbar/bass.svg") }
);
                $('#nav-pic-effects').hover(
      function(){ $(this).attr("src", "/catalog/view/theme/loudlemon/images/layout/navbar/effects_hover.svg") },
      function(){ $(this).attr("src", "/catalog/view/theme/loudlemon/images/layout/navbar/effects.svg") }
);
                $('#nav-pic-amps').hover(
      function(){ $(this).attr("src", "/catalog/view/theme/loudlemon/images/layout/navbar/amps_hover.svg") },
      function(){ $(this).attr("src", "/catalog/view/theme/loudlemon/images/layout/navbar/amps.svg") }
);
                $('#nav-pic-accessories').hover(
      function(){ $(this).attr("src", "/catalog/view/theme/loudlemon/images/layout/navbar/strings_hover.svg") },
      function(){ $(this).attr("src", "/catalog/view/theme/loudlemon/images/layout/navbar/strings.svg") }
);

                /*
                $('.sort-container .sort-link').on('click', function(e){
                  e.preventDefault();
                  var href = $(this).attr('href');
                  $('.sort-link').each(function(){
                    $(this).removeClass('sort-active').removeClass('sort-ASC').removeClass('sort-DESC');
                  });
                  $(this).addClass('sort-active');
                  if (href.indexOf('ASC') > -1) {
                    $(this).addClass('sort-ASC');
                    href = href.replace('ASC', 'DESC');
                  } else {
                    $(this).addClass('sort-DESC');
                    href = href.replace('DESC', 'ASC');
                  }


                  var sortTimeout = setTimeout(function(){
                    refreshList();
                  }, 1000);

                  $('.sort-container .sort-link').on('click', function(e){
                    e.preventDefault();
                    clearTimeout(sortTimeout);
                  });

                  $(this).attr('href', href);

                  // var href = $(this).attr('href');
                  // console.log(href.indexOf('ASC'));
                  // console.log(href.indexOf('DESC'));
                  // console.log(href.indexOf('pd.name'));
                  // console.log(href.indexOf('p.price'));




                });
                */

                $(document).ready(function(){
                  // $(window).on('resize orientationchange', function(){
                    if ($(this).width() < 992) {
                      $('.tablet-nav-row').on('click', '#cart', function(e){
                        e.preventDefault();
                        window.location.href = $('#cart_link').attr('href');
                      });
                    }
                    
                    $('.search-mob-button').attr('search', 'false');
                    
                    $('.search-mob-button').on('click', function(e){
                    
                      //console.log('run');
                      
                      e.preventDefault();
                      
                      //console.log('oo' + $('.navbar-header .hamburger').hasClass('is-active'));
                      
                      //if($('.navbar-header .hamburger').hasClass('is-active')) {
                      //	$('.navbar-header .hamburger').trigger('click');
                      //	console.log('trigered');
                      //}
                      
                      
                      if ($('.search-mob-button').attr('search') == 'true') {
                      	// console.log('go');
                        window.location.href = '/search/?description=true&search='+$('.search-mobile-input').val();
                      }
                      else {
                      		$('.search-mobile-input').focus();
                      		$('html').addClass('search-opened');
                      		$('.search-mob-button').attr('search', 'true');
                      }
                    });
                    
                    $('.search-mobile-input').keypress(function(e){
                        if(e.which == 13)
                        {
                          window.location.href = '/search/?description=true&search='+$('.search-mobile-input').val();
                        }
                    });
                    
                    $('.search-mobile-input').focusout(function(){
                        setTimeout(function() {
                        	$('.search-mob-button').attr('search', 'false');
                        	$('html').removeClass('search-opened');
                        }, 500);
                    });
                      

                    
                  // });
                 });

                  $('.search-tablet>input').focus(function(){
                    $('.search-tablet').css('background-color', '#856F52');
                  });
                  $('.search-tablet>input').focusout(function(){
                    $('.search-tablet').css('background-color', '#A88D69');
                  });

                  $('.tablet-cart-container img').attr('src', '/catalog/view/theme/loudlemon/images/layout/shop_cart_white.svg')
                  $(window).on('resize orientationchange', function(){

                  });


//BURGER ANIMATION
$(document).ready(function(){

	// $('.menu-text').click(function(){
	//   $('.hamburger').each(function(){
	//     $(this).trigger('click');
	//     $(this).toggleClass('is-active');
	//     // $('.mob-cart-container').toggleClass('hidden');
	//   });
	//   $('.animated-button-container').toggleClass('open');
	//   $('.mob-cart-container').toggleClass('hidden');
	//   if (window.innerWidth < 768) {
	//     $('body, html').toggleClass('body-nonscrollable');
	//     $('header').toggleClass('header-scrollable');
	//     $('.header-wrapper').toggleClass('hidden');
	//     if( /iPhone|iPad|iPod/i.test(navigator.userAgent) ) {
	//       $('.mob-sub-menu').toggleClass('mob-sub-menu-safari');
	//     }
	//   }
	//   // $(this).parent().find('.animated-button-container').toggleClass('open');
	//   // $(this).parent().find('#nav-icon3').toggleClass('open');
	//   // $('.nav-icon').trigger('click');
	
	// });

	$('header .hamburger, .header-wrapper').on('click', function(){
	  //console.log('burger!');
	  
	  $('html').toggleClass("menu-opened");
		
	  $('header .hamburger').each(function(){
	    $(this).toggleClass('is-active');
	  });
	  
	  $('.mob-cart-container').toggleClass('hidden');
	  $('.animated-button-container').toggleClass('open');
	  $('.navbar-collapse').toggleClass('open');
	  

	    $('body').toggleClass('body-nonscrollable');
	    // $('header').toggleClass('header-scrollable');
	    // $('.header-wrapper').toggleClass('hidden');
	    // if( /iPhone|iPad|iPod/i.test(navigator.userAgent) ) {
	    //   $('.mob-sub-menu').toggleClass('mob-sub-menu-safari');
	    // }

	  
	});
	
	
	// if((!$('#carousel-main').length && !$('#scene').length && !$('header + img').length) && !$('#not-found').length && !$('#contact-page')) {
	// 	$('.overflow-menu').addClass('open-permanently');
	// }
	
	if($('header').next().hasClass('container') || $( "#empty-cat" ).length){
		$('.overflow-menu').addClass('open-permanently');
	}
	
	if($('#not-found').length) {
		$('body').css('background', '#fed144');
	}
	
	// $('footer').appendTo('body');

});



///////////// 6TH PART ///////////////////////
   $(document).on('click', '.pagination li a', function() {
        // console.log($('#content').scrollTop());
        // $(document).scrollTop();
        // $('html, body').animate({
        //     scrollTop: $("#content").offset().top
        // }, 500);
      });

      function parallax() {
          $('.fixedbackground').css('background-position-y', '-'+($(document).scrollTop() / 2.5) + 'px');
          // var someNumber = ($(document).scrollTop() - ($(window).height() - $('.fixedbackground').height())/2);
          // // if (someNumber > 0) {
          // //     $('.fixedbackground img').css('opacity', (1 - (someNumber / ($(window).height()/2))));
          // // }
          $('.front-image').css({
                                  'opacity': 1 - ($(document).scrollTop() / ($('header').height() * 3.15)),
                                  'padding-bottom': ($(document).scrollTop() / 4) + 'px'
                                });
      }
      if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {

          $('.fixedbackground').css('background-attachment', 'scroll');
          $('.fixedbackground').css('background', 'none');
          $('#scene').next().css({'position': 'relative', 'z-index': '1'});
          $('.fixedbackground').prepend('<img class="img-background layer" data-depth="0.20" src="'+$('.fixedbackground').attr('bannerUrl')+'">');
          // $('.front-image').removeClass('img-responsive');
          function positionFrontImage() {
            $('.front-image').css({width: ($('#scene').width() / 1.5)});
            $('.front-image').css({
                                    'margin-left': (($('#scene').width() - $('.front-image').width())/2),
                                    'margin-top': (($('#scene').height() - $('.front-image').height())/1.8)
                                  });
          }
          if( /iPhone|iPad|iPod/i.test(navigator.userAgent) ) {
            a = 10;
          } else {
            a = 2;
          }
            positionFrontImage();
            for (i=1;i<a;i++) {
              setTimeout(function(){positionFrontImage();}, (i*200));
            }


          $(window).on('resize orientationchange', function() {
            positionFrontImage();
          });
          var $scene = $('#scene').parallax();
      } else {
          $('.fixedbackground').css('background-attachment', 'fixed');
          parallax();
          $(window).scroll(function(){
              parallax();
          });
      }
      
      
      $('.mobile-filter-button, .apply-button > .lemon-button').click(function() {
        	$('.filter-col').toggleClass('hidden-xs');
        	$('#content, .pagin-container, .apply-button').toggleClass('hidden-xs');
        	$('.mobile-filter-button').toggleClass('active');
        	// $('html, body').scrollTop($("#content").offset().top-175);
        	
        });
        
        $('.mobile-sort-button, #title-block .fader, .sort-container > *').click(function() {
        	
        	if (window.matchMedia('(max-width: 767px)').matches) {
        			
	        	$('.sort-container').toggleClass('hidden-xs mobile-active');
	        	// $('body').toggleClass('body-fader modal-open');
	        	$('.sort-container').siblings('.fader').toggleClass('hidden');
	        	
	        	// $('body').toggleClass('modal-open');
	        	// $('modal-backdrop').toggleClass('fade in');
	        	$('.mobile-sort-button').toggleClass('active');
	    	} 
        
        });
      
      
      
