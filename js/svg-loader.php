<?php

 $dir = __DIR__."/../images/layout/include-svg/*.svg";

?>
	(function ($) {

		var images = {
			<?php
				foreach(glob($dir) as $file)  
				{  
				    ?>'<?=basename($file, ".svg")?>' : '<?= str_replace("\r", "", str_replace("\n", "", str_replace("'", "\"", file_get_contents($file)))); ?>', 
				    <?php
				}  
			?>
		};

		$(function(){
			for (var k in images) {
				$('.svg-'+k).append(images[k]);
			}
		});
		
	})(jQuery);
