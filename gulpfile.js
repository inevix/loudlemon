"use strict";

// VARIABLES
var
    // Properties for auto-prefixes list
    autoprefixerList = [
        'Chrome >= 45',
        'Firefox ESR',
        'Edge >= 12',
        'Explorer >= 10',
        'iOS >= 9',
        'Safari >= 9',
        'Android >= 4.4',
        'Opera >= 30'
    ],
    // Properties of Local Server
    configServer = {
        proxy: 'https://new.loud-lemon.com/',
        notify: false,
        https: true,
        reloadDelay: 3000
    },
    path = {
        build: {
            js: 'js/',
            css: 'stylesheet/',
            img: 'images/homepage/',
            fonts: 'fonts/'
        },
        src: {
            js: 'src/js/homepage.js',
            jsPlugins: 'src/js/_libs/*.js',
            style: 'src/scss/homepage.scss',
            stylePlugins: 'src/scss/_libs/*.scss',
            img: 'src/img/**/*',
            pngSpriteIcons: 'src/img/icons_sprite_png/*.png',
            fonts: 'src/fonts/**/*'
        },
        watch: {
            tpl: 'template/**/*.tpl',
            js: ['src/js/**/*.js', '!src/js/_libs/'],
            jsPlugins: 'src/js/_libs/*.js',
            css: ['src/scss/**/*.scss', '!src/scss/_libs/'],
            cssPlugins: 'src/scss/_libs/*.scss',
            img: 'src/img/**/*',
            pngSpriteIcons: 'src/img/icons_sprite_png/*.png',
            fonts: 'srs/fonts/**/*'
        },
        clean: 'js/homepage.js, js/homepage-plugins.js, stylesheet/homepage.css, stylesheet/homepage-plugins.css'
    },
    // Include GULP
    gulp = require('gulp'),
    // Local Server for work and auto-reload pages
    browserSync = require('browser-sync'),
    // Module for watch errors
    plumber = require('gulp-plumber'),
    // Module for import the contents of one file to another
    rigger = require('gulp-rigger'),
    // Module for merging files into one
    concat = require('gulp-concat'),
    // Module for generating a map of source files
    sourcemaps = require('gulp-sourcemaps'),
    // Module for compiling SASS (SCSS) to CSS
    sass = require('gulp-sass'),
    // Module for automatic installation of auto-prefixes
    autoprefixer = require('gulp-autoprefixer'),
    // Module for minimizing CSS
    cleanCSS = require('gulp-clean-css'),
    // Module for minimizing JavaScript
    uglify = require('gulp-uglify'),
    // Module for caching
    cache = require('gulp-cache'),
    // Module for compressing PNG, JPEG, GIF and SVG images
    imagemin = require('gulp-imagemin'),
    // Module for compressing JPEG
    jpegrecompress = require('imagemin-jpeg-recompress'),
    // Module for compressing PNG
    pngquant = require('imagemin-pngquant'),
    // Module for deleting files and directories
    del = require('del'),
    // BABEL for transform ES6
    babel = require('gulp-babel'),
    // Module for auto-merge png icons
    spritesmith = require('gulp.spritesmith'),
    // Module for rename files
    rename = require('gulp-rename');

// START LOCAL SERVER
gulp.task('browser-sync', function () {
    browserSync.init(configServer);
});

// MERGE STYLES
gulp.task('css:build', function () {
    return gulp.src(path.src.style) // get the style.scss
        .pipe(plumber()) // error tracking
        //.pipe(sourcemaps.init()) // initialization source-map
        .pipe(sass()) // from SCSS to CSS
        .pipe(autoprefixer({ // add prefixes
            browsers: autoprefixerList
        }))
        .pipe(cleanCSS()) // minimizing CSS
        //.pipe(sourcemaps.write('./')) // write source-map
        .pipe(gulp.dest(path.build.css)) // move compiled files
        .pipe(browserSync.reload({stream: true})); // reload server
});

// MERGE STYLES PLUG-INs
gulp.task('cssPlugins:build', function () {
    return gulp.src(path.src.stylePlugins) // get the style.scss
        .pipe(plumber()) // error tracking
        //.pipe(sourcemaps.init()) // initialization source-map
        .pipe(concat('homepage-plugins.scss'))
        .pipe(sass()) // from SCSS to CSS
        .pipe(autoprefixer({ // add prefixes
            browsers: autoprefixerList
        }))
        .pipe(cleanCSS()) // minimizing CSS
        //.pipe(sourcemaps.write('./')) // write source-map
        .pipe(gulp.dest(path.build.css)) // move compiled files
        .pipe(browserSync.reload({stream: true})); // reload server
});

// MERGE CUSTOM JAVASCRIPT
gulp.task('js:build', function () {
    return gulp.src(path.src.js) // get the custom.js
        .pipe(plumber()) // error tracking
        //.pipe(sourcemaps.init()) // initialization source-map
        .pipe(babel()) // include BABEL for ES5, ES6
        .pipe(uglify()) // minimizing JS
        //.pipe(sourcemaps.write('./')) // write source-map
        .pipe(gulp.dest(path.build.js)) // move compiled files
        .pipe(browserSync.reload({stream: true})); // reload server
});

// MERGE JAVASCRIPT PLUG-INs
gulp.task('jsPlugins:build', function () {
    return gulp.src(path.src.jsPlugins) // get the custom.js
        .pipe(plumber()) // error tracking
        //.pipe(sourcemaps.init()) // initialization source-map
        .pipe(concat('homepage-plugins.js'))
        .pipe(babel()) // include BABEL for ES5, ES6
        .pipe(uglify()) // minimizing JS
        //.pipe(sourcemaps.write('./')) // write source-map
        .pipe(gulp.dest(path.build.js)) // move compiled files
        .pipe(browserSync.reload({stream: true})); // reload server
});

// COMPRESSING IMAGES
gulp.task('image:build', function () {
    return gulp.src(path.src.img)
        .pipe(cache(imagemin({
            interlaced: true,
            progressive: true,
            svgoPlugins: [{removeViewBox: false}],
            use: [
                pngquant(),
                jpegrecompress({
                    progressive: true,
                    max: 90,
                    min: 80
                })
            ]
        })))
        .pipe(gulp.dest(path.build.img)) // move compiled files
        .pipe(browserSync.reload({stream: true})); // reload server
});

gulp.task('sprite:build', function () {
    var sprite = gulp.src(path.src.pngSpriteIcons).pipe(spritesmith({
        imgName: '../img/sprite.png',
        cssName: '_sprite.scss',
        cssFormat: 'scss',
        algoritm: 'binary-tree',
        padding: 5
    }));
    sprite.img.pipe(rename('sprite.png')).pipe(gulp.dest(path.build.img));
    sprite.css.pipe(gulp.dest('src/scss/_sprite/'))
    .pipe(browserSync.reload({stream: true})); // reload server
});

// MOVE FONTS
gulp.task('fonts:build', function () {
    return gulp.src(path.src.fonts)
        .pipe(gulp.dest(path.build.fonts)) // move compiled files
        .pipe(browserSync.reload({stream: true})); // reload server
});

// DELETE BUILD DIRS
gulp.task('clean:build', function () {
    del.sync(path.clean);
});

// CLEAR CACHE
gulp.task('cache:clear', function () {
    cache.clearAll();
});

// BUILD PROJECT
gulp.task('build', [
    //'clean:build',
    'css:build',
    'cssPlugins:build',
    'js:build',
    'jsPlugins:build',
    'fonts:build',
    'image:build',
    'sprite:build'
]);

// RUN TASKS WHEN CHANGING FILES
gulp.task('watch', function () {
    gulp.watch(path.watch.tpl, browserSync.reload);
    gulp.watch(path.watch.css, ['css:build']);
    gulp.watch(path.watch.cssPlugins, ['cssPlugins:build']);
    gulp.watch(path.watch.js, ['js:build']);
    gulp.watch(path.watch.jsPlugins, ['jsPlugins:build']);
    gulp.watch(path.watch.img, ['image:build']);
    gulp.watch(path.watch.pngSpriteIcons, ['sprite:build']);
    gulp.watch(path.watch.fonts, ['fonts:build']);
});

// DEFAULT TASK
gulp.task('default', [
    //'clean:build',
    'build',
    'browser-sync',
    'watch'
]);