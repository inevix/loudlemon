/*
ON LOAD PAGE FUNCTION
*/

let windowWidth = window.innerWidth;

jQuery( window ).on( 'load', function() {

    if ( windowWidth <= 1024 ) {
        let jsMainVideo = $('.js-box-video');

        if ( jsMainVideo.length ) {
            jsMainVideo.remove();
        }

    }

    let firstSlideOfMainSlider = $('.js-main-slider .slick-slide.slick-current');
    firstSlideOfMainSlider.removeClass('slick-active');

    $('body').removeClass('is-load');

    setTimeout( function () {
        firstSlideOfMainSlider.addClass('slick-active');
    }, 0)

    if ( windowWidth >= 1024 ) {

        new WOW().init();

    }

    linkToTopScroll( $(this) );

} );

/*
INITIALIZATION FUNCTIONS
*/

jQuery( document ).ready( function ( $ ) {

    /**
     * Init jQuery functions
     */

    initSliders();
    toggleNewsPopup();
    closeShare();
    toggleSearchPanel();
    linkToTopClick();

    if ( windowWidth >= 1025) {
        homepageVideo();
    } else {
        toggleMobileNav();
        toggleSubMobileMenu();
    }

} );

/*
ON SCROLL PAGE FUNCTIONS
*/

jQuery( window ).on( 'scroll', function() {

    /**
     * Init jQuery functions
     */

    linkToTopScroll( $(this) );

    if ( windowWidth >= 1025) {
        homepageVideoPlayOnScroll( $(this) );
        scrollHeader( $(this) );
    } else {
        scrollHeaderMobile( $(this) );
    }

} );

jQuery( window ).on( 'resize', function() {
    windowWidth = window.innerWidth;
} );

/**
 * jQuery functions
 */

function closeShare() {
    let share = $('.js-share'),
        header = $('.js-header'),
        headerInfo = $('.js-header-info'),
        mobileHeader = $('.js-mobile-header'),
        btnClose = $('.js-share-close'),
        content = $('.js-content'),
        searchPanel = $('.js-search-panel'),
        mobileHeaderDropdownNav = $('.js-mobile-header-nav-dropdown'),
        dropdownSubmenu = $('.js-submenu'),
        fixedSubs = $('.fixed-subs');

    btnClose.on( 'click', function( e ) {
        share.addClass('is-hide');
        share.addClass('is-user-closed');
        content.removeClass('padding-top');
        content.addClass('not-share');
        searchPanel.addClass('not-share');

        if ( windowWidth >= 1025 ) {
            header.removeClass('padding-top');
            header.addClass('not-share');
            headerInfo.removeClass('padding-top');
            headerInfo.addClass('not-share');
        } else {
            mobileHeader.removeClass('padding-top');
            mobileHeaderDropdownNav.removeClass('padding-top');
            dropdownSubmenu.removeClass('padding-top');
            mobileHeader.addClass('is-fixed');
            fixedSubs.removeClass('padding-top');
        }

        e.preventDefault();
    } );
}

function toggleSearchPanel() {
    let openPanel = $('.js-toggle-search'),
        closePanel = $('.js-close-search'),
        searchPanel = $('.js-search-panel'),
        searchInput = $('.js-search-input'),
        header = $('.js-header'),
        buttonNav = $('.js-button-nav'),
        buttonWishlist = $('.js-button-wishlist'),
        buttonCart = $('.js-button-cart'),
        boxLogo = $('.js-box-logo'),
        mobileHeaderContent = $('.js-mobile-header-content'),
        mobileHeaderDropdownNav = $('.js-mobile-header-nav-dropdown'),
        dropdownSubmenu = $('.js-submenu'),
        htmlDoc = $('html'),
        fixedSubs = $('.fixed-subs');

    let openSearchPanel = function() {

        if ( searchPanel.length ) {

            openPanel.on( 'click', function( e ) {
                mobileHeaderDropdownNav.addClass('sl--scrollable');

                if ( htmlDoc.hasClass('popup-open') && !mobileHeaderDropdownNav.hasClass('is-active') ) {
                    scrollLock.show();
                    htmlDoc.removeClass('popup-open');
                } else if ( htmlDoc.hasClass('popup-open') && mobileHeaderDropdownNav.hasClass('is-active') ) {

                } else {
                    scrollLock.hide();
                    htmlDoc.addClass('popup-open');
                }

                if ( $(this).hasClass('is-active') ) {
                    openPanel.removeClass('is-active');
                    header.removeClass('view-header--white');

                    searchPanel.removeClass('is-active');
                    searchInput.val('');

                    e.preventDefault();
                } else {
                    $(this).addClass('is-active');

                    header.addClass('view-header--white');

                    searchPanel.addClass('is-active');
                    searchInput.focus();

                    $(this).closest(mobileHeaderContent).find(buttonNav).removeClass('is-active');

                    if ( windowWidth < 1025 ) {
                        mobileHeaderDropdownNav.removeClass('is-active');
                        dropdownSubmenu.removeClass('is-active');
                        fixedSubs.removeClass('is-active');
                    }

                    if ( windowWidth < 600 ) {
                        $(this).closest(mobileHeaderContent).find(buttonWishlist).removeClass('is-active');
                        $(this).closest(mobileHeaderContent).find(buttonCart).removeClass('is-active');
                        $(this).closest(mobileHeaderContent).find(boxLogo).removeClass('is-hide');
                    }

                    e.preventDefault();
                }

            } );

        }
    }

    let closeSearchPanel = function() {

        if ( searchPanel.length ) {

            closePanel.on( 'click', function( e ) {
                mobileHeaderDropdownNav.addClass('sl--scrollable');
                scrollLock.show();
                openPanel.removeClass('is-active');
                header.removeClass('view-header--white');
                fixedSubs.removeClass('view-header--white');

                searchPanel.removeClass('is-active');
                searchInput.val('');

                e.preventDefault();
            } );

        }
    }

    openSearchPanel();
    closeSearchPanel();

}

function homepageVideo() {
    let video = $('.js-homepage-video'),
        volumeButton = $('.js-video-volume');

    video.on( 'click', function( e ) {

        if ( this.paused === true ) {
            this.play();
            this.classList.remove('handle-pause');

            e.preventDefault();
        } else {
            this.pause();
            this.classList.add('handle-pause');

            e.preventDefault();
        }

    } );

    volumeButton.on( 'click', function( e ) {

        if ( video.get(0).muted === false ) {
            video.get(0).muted = true;
            $(this).addClass('is-mute');

            e.preventDefault();
        } else {
            video.get(0).muted = false;
            $(this).removeClass('is-mute');

            e.preventDefault();
        }

    } );

}

function homepageVideoPlayOnScroll( window ) {
    let videoBox = $('.js-homepage-video');

    if ( videoBox.length ) {
        let video = videoBox.get(0),
            videoOffsetTop = videoBox.offset().top,
            videoHeight = videoBox.height(),
            windowScrollTop = window.scrollTop();

        if ( !videoBox.hasClass('handle-pause') && windowWidth >= 1025 ) {

            if ( windowScrollTop > ( videoOffsetTop - videoHeight / 2 ) && windowScrollTop < ( videoOffsetTop + videoHeight / 2 ) ) {
                video.play();
            } else {
                video.pause();
            }

        }

    }

}

function toggleNewsPopup() {
    let btnOpenNews = $('.js-open-new'),
        btnCloseNews = $('.js-close-new'),
        popupNews = $('.js-news-popup'),
        popupNewsContent = $('.js-news-popup-content');

    let openNewsPopup = function() {

        btnOpenNews.on( 'click', function( e ) {
            scrollLock.hide();
            $('html').addClass('popup-open');
            popupNews.addClass('is-active');

            setTimeout( function() {
                popupNewsContent.addClass('is-active');
            }, 500 );

            e.preventDefault();
        } );

    }

    let closeNewsPopup = function() {

        btnCloseNews.on( 'click', function( e ) {
            scrollLock.show();
            $('html').removeClass('popup-open');
            popupNewsContent.removeClass('is-active');

            setTimeout( function() {
                popupNews.removeClass('is-active');
            }, 500 );

            e.preventDefault();
        } );

        popupNews.on( 'click', function( e ) {
            scrollLock.show();
            $('html').removeClass('popup-open');
            popupNewsContent.removeClass('is-active');

            setTimeout( function() {
                popupNews.removeClass('is-active');
            }, 500 );

            e.preventDefault();
        } );

        popupNews.on( 'mouseover', function() {
            btnCloseNews.addClass('is-active');
        } );

        popupNews.on( 'mouseout', function() {
            btnCloseNews.removeClass('is-active');
        } );

    }

    openNewsPopup();
    closeNewsPopup();

}

function scrollHeader( window ) {
    let header = $('.js-header'),
        share = $('.js-share'),
        headerInfo = $('.js-header-info'),
        searchPanel = $('.js-search-panel'),
        heightBoxes = header.height() + share.height() + headerInfo.height(),
        paddingTop = 300 + share.height() + headerInfo.height();

    if ( header.length ) {

        if ( window.scrollTop() > heightBoxes && window.scrollTop() < paddingTop ) {
            header.removeClass('is-scroll');
            searchPanel.removeClass('is-scroll');
            header.addClass('not-visible');
        } else if ( window.scrollTop() >= paddingTop ) {
            header.removeClass('not-visible');
            header.addClass('is-scroll');
            searchPanel.addClass('is-scroll');
        } else {
            header.removeClass('not-visible');
            header.removeClass('is-scroll');
            searchPanel.removeClass('is-scroll');
        }

        if ( !share.hasClass('is-user-closed') ) {

            if ( window.scrollTop() > paddingTop ) {
                share.addClass('is-hide');
                headerInfo.addClass('is-hide');
                header.removeClass('padding-top');
            } else {
                share.removeClass('is-hide');
                headerInfo.removeClass('is-hide');
                header.addClass('padding-top');
            }

        } else {

            if ( window.scrollTop() > paddingTop ) {
                share.addClass('is-hide');
                headerInfo.addClass('is-hide');
                header.removeClass('not-share');
            } else {
                headerInfo.removeClass('is-hide');
                header.addClass('not-share');
            }

        }



    }

}

function scrollHeaderMobile( window ) {
    let mobileHeader = $('.js-mobile-header'),
        boxShare = $('.js-share'),
        boxSearch = $('.js-search-panel'),
        mobileHeaderDropdownNav = $('.js-mobile-header-nav-dropdown'),
        dropdownSubmenu = $('.js-submenu'),
        fixedSubs = $('.fixed-subs'),
        content = $('.js-content');

    if ( mobileHeader.length ) {

        if ( window.scrollTop() > 71 && !boxShare.hasClass('is-user-closed') ) {
            mobileHeader.removeClass('padding-top');
            mobileHeaderDropdownNav.removeClass('padding-top');
            dropdownSubmenu.removeClass('padding-top');
            fixedSubs.removeClass('padding-top');
            mobileHeader.addClass('is-fixed');
            boxSearch.addClass('is-scroll');
        } else if ( window.scrollTop() < 71 && boxShare.hasClass('is-user-closed') ) {
            mobileHeader.removeClass('padding-top');
            mobileHeaderDropdownNav.removeClass('padding-top');
            dropdownSubmenu.removeClass('padding-top');
            fixedSubs.removeClass('padding-top');
            mobileHeader.addClass('is-fixed');
            boxSearch.addClass('is-scroll');
        }  else if ( window.scrollTop() > 0 && boxShare.hasClass('is-user-closed') ) {
            mobileHeader.removeClass('padding-top');
            mobileHeaderDropdownNav.removeClass('padding-top');
            dropdownSubmenu.removeClass('padding-top');
            fixedSubs.removeClass('padding-top');
            mobileHeader.addClass('is-fixed');
            boxSearch.addClass('is-scroll');
        } else {
            if ( content.hasClass('padding-top') ) {
                mobileHeader.addClass('padding-top');
                mobileHeaderDropdownNav.addClass('padding-top');
                dropdownSubmenu.addClass('padding-top');
                fixedSubs.addClass('padding-top');
            }
            mobileHeader.removeClass('is-fixed');
            boxSearch.removeClass('is-scroll');
        }

    }

}

function toggleMobileNav() {
    let buttonNav = $('.js-button-nav'),
        buttonWishlist = $('.js-button-wishlist'),
        buttonCart = $('.js-button-cart'),
        boxLogo = $('.js-box-logo'),
        mobileHeaderContent = $('.js-mobile-header-content'),
        openPanel = $('.js-toggle-search'),
        searchPanel = $('.js-search-panel'),
        mobileHeaderDropdownNav = $('.js-mobile-header-nav-dropdown'),
        dropdownSubmenu = $('.js-submenu'),
        htmlDoc = $('html'),
        fixedSubs = $('.fixed-subs');

    buttonNav.on( 'click', function( e ) {
        $(this).toggleClass('is-active');
        mobileHeaderDropdownNav.toggleClass('is-active');
        mobileHeaderDropdownNav.addClass('sl--scrollable');
        dropdownSubmenu.removeClass('is-active');
        fixedSubs.removeClass('is-active');

        if ( htmlDoc.hasClass('popup-open') && !searchPanel.hasClass('is-active') ) {
            scrollLock.show();
            htmlDoc.removeClass('popup-open');
        } else if ( htmlDoc.hasClass('popup-open') && searchPanel.hasClass('is-active') ) {

        } else {
            scrollLock.hide();
            htmlDoc.addClass('popup-open');
        }

        $(this).closest(mobileHeaderContent).find(openPanel).removeClass('is-active');
        searchPanel.removeClass('is-active');

        if ( windowWidth < 600 ) {
            $(this).closest(mobileHeaderContent).find(buttonWishlist).toggleClass('is-active');
            $(this).closest(mobileHeaderContent).find(buttonCart).toggleClass('is-active');
            $(this).closest(mobileHeaderContent).find(boxLogo).toggleClass('is-hide');

        }

        e.preventDefault();
    } );
}

function toggleSubMobileMenu() {
    let openSubmenu = $('.js-open-submenu'),
        closeSubmenu = $('.js-close-submenu'),
        submenu = $('.js-submenu'),
        mobileHeaderDropdownNav = $('.js-mobile-header-nav-dropdown'),
        fixedSubs = $('.fixed-subs');

    if ( submenu.length ) {

        openSubmenu.on( 'click', function( e ) {
            let submenuOpen = $(this).attr('data-submenu-open'),
                submenuCurrent = $('.'+submenuOpen);

            mobileHeaderDropdownNav.removeClass('sl--scrollable');
            submenu.removeClass('is-active');
            fixedSubs.addClass('is-active');
            submenuCurrent.addClass('is-active');
            scrollLock.show();

            e.preventDefault();
        } );

        closeSubmenu.on( 'click', function( e ) {
            console.log('scroll unlock')
            submenu.removeClass('is-active');
            fixedSubs.removeClass('is-active');
            mobileHeaderDropdownNav.addClass('sl--scrollable');
            scrollLock.show();

            e.preventDefault();
        } );

    }
}

function linkToTopClick() {
    let linkToTop = $('.js-link-to-top');

    if ( linkToTop.length ) {

        linkToTop.on( 'click', function( e ) {
            $('html, body').animate( {
                scrollTop: 0
            }, 1500);

            e.preventDefault();
        } );

    }
}

function linkToTopScroll( body ) {
    let linkToTop = $('.js-link-to-top');

    if ( body.scrollTop() > window.innerHeight ) {
        linkToTop.removeClass('is-hide');
    } else {
        linkToTop.addClass('is-hide');
    }
}

function initSliders() {
    let jsMainSlider = $('.js-main-slider');

    if ( jsMainSlider.length ) {

        jsMainSlider.slick( {
            arrows: true,
            dots: true,
            dotsClass: 'flex-box flex-center flex-align-center slick-dots',
            autoplay: true,
            autoplaySpeed: 10000,
            responsive: [
                {
                    breakpoint: 1025,
                    settings: {
                        dots: false
                    }
                },
                {
                    breakpoint: 768,
                    settings: {
                        arrows: false
                    }
                }
            ]
        } );

    }

    let jsBrandsCarousel = $('.js-brands-carousel');

    if ( jsBrandsCarousel.length ) {

        jsBrandsCarousel.slick( {
            arrows: true,
            dots: false,
            slidesToScroll: 1,
            slidesToShow: 6,
            swipeToSlide: true,
            responsive: [
                {
                    breakpoint: 1441,
                    settings: {
                        slidesToShow: 5
                    }
                },
                {
                    breakpoint: 1280,
                    settings: {
                        slidesToShow: 4
                    }
                },
                {
                    breakpoint: 1025,
                    settings: {
                        slidesToShow: 3
                    }
                },
                {
                    breakpoint: 768,
                    settings: {
                        slidesToShow: 2
                    }
                },
                {
                    breakpoint: 550,
                    settings: {
                        slidesToShow: 1
                    }
                }
            ]
        } );

    }

    let jsNewsCarousel = $('.js-news-carousel'),
        jsNewsSlider = $('.js-news-slider');

    if ( jsNewsCarousel.length ) {

        jsNewsCarousel.slick( {
            arrows: true,
            dots: false,
            slidesToScroll: 1,
            slidesToShow: 6,
            swipeToSlide: true,
            asNavFor: '.js-news-slider',
            focusOnSelect: true,
            adaptiveHeight: false,
            responsive: [
                {
                    breakpoint: 1441,
                    settings: {
                        slidesToShow: 5
                    }
                },
                {
                    breakpoint: 1171,
                    settings: {
                        slidesToShow: 4
                    }
                },
                {
                    breakpoint: 1025,
                    settings: {
                        slidesToShow: 3
                    }
                },
                {
                    breakpoint: 651,
                    settings: {
                        slidesToShow: 2
                    }
                },
                {
                    breakpoint: 401,
                    settings: {
                        slidesToShow: 1
                    }
                }
            ]
        } );
        jsNewsSlider.slick( {
            arrows: true,
            dots: false,
            slidesToScroll: 1,
            slidesToShow: 1,
            asNavFor: '.js-news-carousel',
            fade: true,
            responsive: [
                {
                    breakpoint: 1025,
                    settings: {
                        adaptiveHeight: true
                    }
                }
            ]
        } );

    }

    let jsReviewsSlider = $('.js-reviews-slider');

    if ( jsReviewsSlider.length ) {

        jsReviewsSlider.slick( {
            arrows: true,
            dots: false,
            responsive: [
                {
                    breakpoint: 1441,
                    settings: {
                        adaptiveHeight: true
                    }
                }
            ]
        } );

    }

}